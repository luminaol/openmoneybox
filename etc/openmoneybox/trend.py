#!/usr/bin/env python
"""
/***************************************************************
 * Name:      trend.py
 * Purpose:   OpenMoneyBox - Generate trend chart image
 * Author:    Igor Cali' (igor.cali0@gmail.com)
 * Created:   2017-02-25
 * Copyright: Igor Cali' (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

Derived from sample at http://matplotlib.org/1.5.1/examples/pylab_examples/date_demo_convert.html
"""

from datetime import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from numpy import arange
import numpy as np
import gettext
import sys

def print_syntax():
	print ('Syntax: python trend.py locale_dir input_file output_file\n')
	print ('    locale_dir: path to system locale folder')
	print ('    input_file: csv input file with chart data')
	sys.exit('    output_file: output chart image')

args = len(sys.argv)

if args < 4:
	print_syntax();

locale_dir = sys.argv[1:][0]
input_file = sys.argv[2:][0]
output_file = sys.argv[3:][0]

if not locale_dir:
	print_syntax();

if not input_file:
	print_syntax();

if not output_file:
	print_syntax();

paint_trend = False
if args > 4:
	paint_trend = True;
	trendcolor = sys.argv[4:][0]
	trendvalue = sys.argv[5:][0]

# default for locale_dir should be 'openmoneybox', '/usr/share/locale'
gettext.bindtextdomain(locale_dir)
gettext.textdomain('openmoneybox')
_ = gettext.gettext

col_names = ["date", "value"]
dtypes = ["int", "float"]
data = np.genfromtxt(input_file, delimiter=',', names=col_names, dtype=dtypes)

date1 = datetime.fromtimestamp(data['date'][0])
date2 = datetime.fromtimestamp(data['date'][-1])
# Necessary to make sure last day is shown in the chart
second = timedelta(seconds=1);
date2 = date2 + second

delta = timedelta(days=1)
dates = drange(date1, date2, delta)

#fig, ax = plt.subplots()
#ax.plot_date(dates, data['value'], ".k-")

# Size in inches
fig = plt.figure(figsize=(6, 2	), dpi=100)
ax = fig.add_subplot(111)
ax.plot_date(dates, data['value'], ".k-")

if paint_trend:
	empty_day_first = 0;
	empty_day_last = 0;
	i = 0
	found = False
	for value in data['value']:
		i = i + 1
		if value == 0:
			if not found:
				empty_day_first = i
				found = True
		else:
			found = False
	empty_day_last = i

	if empty_day_first > 1:
		trenddate1 = datetime.fromtimestamp(data['date'][empty_day_first - 2])
	else:
		trenddate1 = datetime.fromtimestamp(data['date'][0])
	trenddate2 = date2
	if empty_day_first > 1:
		trenddelta = timedelta(days = empty_day_last - empty_day_first + 1)
	else:
		trenddelta = timedelta(days = empty_day_last - empty_day_first)
	trendrange = drange(trenddate1, trenddate2, trenddelta)
	trendvalues = [data['value'][empty_day_first - 2], trendvalue]
	if found:	
		ax.plot_date(trendrange, trendvalues, "x" + trendcolor + "-")

# this is superfluous, since the autoscaler should get it right, but
# use date2num and num2date to convert between dates and floats if
# you want; both date2num and num2date convert an instance or sequence
ax.set_xlim(dates[0], dates[-1])

# The hour locator takes the hour or sequence of hours you want to
# tick, not the base multiple

ax.xaxis.set_major_locator(DayLocator())
#ax.xaxis.set_minor_locator(HourLocator(arange(0, 25, 24)))
ax.xaxis.set_major_formatter(DateFormatter('%d-%m-%Y'))
steps = drange(date1, date2, timedelta(days=7))
plt.xticks(steps);

ax.fmt_xdata = DateFormatter('%Y-%m-%d %H:%M:%S')
fig.autofmt_xdate()

# Set y scale
MinValue = data['value'][0]

if MinValue == 0:
	for i in data['value']:
		if i > 0:
			MinValue = i
			break

for m in data['value']:
	if m != 0:
		if m < MinValue:
			MinValue = m
MaxValue = max(data['value'])

p = 0
res = MinValue;
while res >= 1:
	res = res/10;
	if res >= 1:
		p += 1
p = pow(10, p);
num = int(MinValue / p)
if num > 1:
	num -= 1
MinValue = num * p

p = 0
real_max_value = res = MaxValue;
while res >= 1:
	res = res / 10;
	if res >= 1:
		p += 1
p = pow(10, p);
num = int(MaxValue / p + 1)
MaxValue = num * p;

if MinValue > 0:
	if (p > 1000) & (MaxValue / MinValue > 2):
		test_max_value = MaxValue - (MinValue / 4 * 3);
		if test_max_value >= real_max_value:
			MaxValue = test_max_value;
		else:
			test_max_value = MaxValue - (MinValue / 2);
			if test_max_value >= real_max_value:
				MaxValue = test_max_value

	plt.ylim([MinValue, MaxValue])

hfont = {'fontname':'Ubuntu'}
plt.title(_('Trend Chart'), **hfont)

plt.xlabel(_('Day'), **hfont)
plt.ylabel(_('Value'), **hfont)
	
#plt.show()
plt.savefig(output_file, bbox_inches='tight')
