﻿/***************************************************************
 * Name:      openmoneybox
 * Description: Budget management application
 * OpenMoneyBox is an application designed to manage small personal money budgets in the easiest way.
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Last update   2019-04-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/
_____________________________________________________________________________________________________________________________________________________________________

Install on linux:
	> make
	> sudo make install

	Tested on:
		- Ubuntu Bionic (18.04.2) x64 [v3.3.1.x]
		- Ubuntu Xenial (16.04.4) x64 [v3.2.x.x]
		- Ubuntu Trusty (14.04.3) x64 [v3.1.2.x]
		- Debian Stretch
_____________________________________________________________________________________________________________________________________________________________________

Install on windows:
	Requirements: install Mingw with MSYS

	1) build wxwidgets:
		(disable MSYS binary beforehand)
		debug build:
			> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=debug UNICODE=1
		release build:
			> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=release UNICODE=1 DEBUG_FLAG=0
		(set wxDEBUG_LEVEL to 0 in /include/wx/debug.h)

	2) build openmoneybox:
		> mingw32-make -f makefile.win
		> mingw32-make -f makefile.win install
			Tested on Win10 x64
_____________________________________________________________________________________________________________________________________________________________________

