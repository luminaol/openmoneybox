/***************************************************************
 * Name:      platformsetup.cpp
 * Purpose:   Specific definition for OS platform
 * Author:    Igor Cal� (igor.cali0@gmail.com)
 * Created:   2017-02-18
 * Copyright: Igor Cal� (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef PLATFORMSETUP_CPP_INCLUDED
#define PLATFORMSETUP_CPP_INCLUDED

#include <wx/string.h>

#ifdef __WXMSW__
  #include <wx/filefn.h>
  #include "platformsetup.h"
  wxString DataDir=wxGetCwd()+L"\\";
  wxString ShareDir=DataDir;
#else
	wxString DataDir=L"/etc/openmoneybox/";		// Program data directory
	wxString ShareDir=L"/usr/share/";			// OS shared durectory
#endif // __WXMSW__

#ifdef __WXGTK__
	#if(wxCHECK_VERSION(3, 0, 0))
		wxString Console = "xterm -hold -e ";	// Console prompt
	#else
		wxString Console = L"xterm -hold -e ";	// Console prompt
	#endif	// wxCHECK_VERSION
#endif // __WXGTK__

#endif  // PLATFORMSETUP_CPP_INCLUDED
