/***************************************************************
 * Name:      omb31core.cpp
 * Purpose:   Core Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-04-28
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef BIL31CORE_CPP_INCLUDED
#define BIL31CORE_CPP_INCLUDED

#include <wx/string.h>
#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

//#include <wx/file.h>
#include <wx/filename.h>
#include <wx/textfile.h>

#include "platformsetup.h"
#include "constants.h"
#include "omb31core.h"
#ifndef __OMBCONVERT_BIN__
	#include "ui/password.h"
#endif // __OMBCONVERT_BIN__

#ifdef __OPENMONEYBOX_EXE__
	#include "openmoneybox/ui/main_wx.h"
#endif

#ifndef __WXMSW__
	extern wxLanguage Lan;
#endif // __WXMSW__

#ifdef __OPENMONEYBOX_EXE__
	extern "C" ombMainFrame *frame;
#endif // __OPENMONEYBOX_EXE__

#ifdef __WXGTK__
	extern wxString Console;
#endif // __WXGTK__

#ifdef _OMB_USE_CIPHER
	#include "wxsqlite3.h"
#endif // _OMB_USE_CIPHER

// db structure
extern wxString cs_information;
extern wxString cs_funds;
//extern wxString cs_credits;
//extern wxString cs_debts;
//extern wxString cs_loans;
//extern wxString cs_borrows;
extern wxString cs_shoplist;
//extern wxString cs_transactions;
extern wxString cs_categories;
extern wxString cs_funds_master;
//extern wxString cs_credits_master;
//extern wxString cs_debts_master;
//extern wxString cs_loans_master;
//extern wxString cs_borrows_master;
extern wxString cs_categories_master;

	// Credits table create statement
	wxString cs_credits_v31 = L"CREATE TABLE Credits(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Debts table create statement
	wxString cs_debts_v31 = L"CREATE TABLE Debts(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Loans table create statement
	wxString cs_loans_v31 = L"CREATE TABLE Loans(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER);";

	// Borrows table create statement
	wxString cs_borrows_v31 = L"CREATE TABLE Borrows(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER);";

	// Transactions list table create statement
	wxString cs_transactions_v31 = L"create TABLE Transactions(" \
																						"id INTEGER PRIMARY KEY," \
																						"isdate INTEGER," \
																						"date INTEGER," \
																						"time INTEGER," \
																						"type INTEGER," \
																						"value TEXT," \
																						"reason TEXT," \
																						"cat_index INTEGER);";

	// Credits table create statement (master)
	wxString cs_credits_master_v31 = L"CREATE TABLE Credits%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Debts table create statement (master)
	wxString cs_debts_master_v31 = L"CREATE TABLE Debts%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Loans table create statement (master)
	wxString cs_loans_master_v31 = L"CREATE TABLE Loans%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER);";

	// Borrows table create statement (master)
	wxString cs_borrows_master_v31 = L"CREATE TABLE Borrows%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER);";

#ifdef _OMB_USE_CIPHER
	extern bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
	extern wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql);
#endif // _OMB_USE_CIPHER

#ifndef __OMBCONVERT_BIN__
	TData::TData(wxGauge *ProgressBar){
#else
	TData::TData(void){
#endif // __OMBCONVERT_BIN__
	// Data creation
	Funds=new TVal[MAX_FUNDS];
	Credits=new TVal[MAX_CREDITS];
	Debts=new TVal[MAX_DEBTS];
	Lent=new TObj[MAX_LENT];
	Borrowed=new TObj[MAX_BORROWED];
	ShopItems=new TShopItem[MAX_SHOPS];
	Lines=new TLine[MAX_LINES];
	Categories = new TCategory[MAX_CATEGORIES];

	//__________________
	//NotEnoughMemory=false;
	MattersBuffer=new wxArrayString();
	MattersBuffer->Clear();

	/*
	CategoryDB = new wxArrayString();
	CategoryDB->Clear();
	*/

	#ifndef __OMBCONVERT_BIN__
		Progress=ProgressBar;
	#endif // __OMBCONVERT_BIN__

	#ifndef _OMB_USE_CIPHER
		database = new wxSQLite3Database();
	#endif // _OMB_USE_CIPHER

	Initialize(true);
	//Opening=false;
}

void TData::Initialize(bool Creating){
	NFun = NCre = NDeb = NLen = NBor = NSho = NLin = 0;
	// Funds initialization
	Funds[0].Name=wxEmptyString;
	Funds[0].Value=0;
  // Credits initialization
  Credits[0].Name=wxEmptyString;
  Credits[0].Value=0;
  // Debts initialization
  Debts[0].Name=wxEmptyString;
  Debts[0].Value=0;
  // Lent objects initialization
  Lent[0].Name=wxEmptyString;
  Lent[0].Object=wxEmptyString;
  Lent[0].Alarm=wxInvalidDateTime;
  // Borrowed objects initialization
  Borrowed[0].Name=wxEmptyString;
  Borrowed[0].Object=wxEmptyString;
  Borrowed[0].Alarm=wxInvalidDateTime;
  // Shopping list initialization
  ShopItems[0].Name=wxEmptyString;
  ShopItems[0].Alarm=wxInvalidDateTime;
  // Report initialization
  Lines[0].IsDate=false;
  Lines[0].Date=wxInvalidDateTime;
  Lines[0].Time=wxInvalidDateTime;
  Lines[0].Type=toNULL;
  Lines[0].Value=wxEmptyString;
  Lines[0].Reason=wxEmptyString;
 	Lines[0].CategoryIndex = -1;

  // Totals zeroing
	Tot_Funds=Tot_Credits=Tot_Debts=0;
  // File data initialization
  FileData.FileName=wxEmptyString;
  FileData.FileView=wxEmptyString;
  //FileData.Access=wxEmptyString;
	FileData.DefFund=wxEmptyString;
  FileData.Modified=false;
	FileData.ReadOnly=false;
  if(Creating){
		Day=wxDateTime::Today();
		FileData.Year = Day.GetYear();
		// Following code is a workaround for GetMonth() bug in Linux
		// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
		wxString temp;
		#ifdef __WXMSW__
			temp = Day.Format("%m", wxDateTime::Local).c_str();
		#else
			temp = Day.Format(L"%m", wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month, 10);
		// ---------------------------------------------------------------------
		FileData.DateStamp = wxInvalidDateTime;}}

bool TData::AddValue(TTypeVal T, int id, wxString N, double V){
	if(! Parsing){
		if(N.IsEmpty() || V == 0) return false;
		N = DoubleQuote(N);
	}

	bool E=false;
	int x;
	wxString curstr;

	#if( wxCHECK_VERSION(3,0,0) )
		curstr = ::wxString::FromCDouble(V, 2);
	#else
		curstr = FormDigits(V);
	#endif

	switch(T){
		case tvFou:
			for(x = 0; x < NFun; x++)if(Funds[x].Name == N){
				Error(11,N);
				return false;}

			if(Parsing){
				Funds[NFun].Id = id;
				Funds[NFun].Name = N;
				Funds[NFun].Value = V;}
			else{
				#ifdef _OMB_USE_CIPHER
					if(! TableExists(L"Funds", wxEmptyString, database)) ExecuteUpdate(database, cs_funds.c_str()/*, false*/);
					wxString Sql = L"insert into Funds values (NULL, '" +
																			N +
																			L"', " +
																			curstr +
																			L");";
					ExecuteUpdate(database, Sql.c_str()/*, false*/);
				#else
					if(! database->TableExists(L"Funds")) database->ExecuteUpdate(cs_funds);
					database->ExecuteUpdate(L"insert into Funds values (NULL, '" +
																		N +
																		L"', " +
																		curstr +
																		L");");
				#endif // _OMB_USE_CIPHER
			}

			/*
			if(!SortValues(tvFou)){
				Error(37,wxEmptyString);
				wxTheApp->Exit();
				return false;}
			*/
			break;
		case tvCre:
			for(x = 0; x < NCre; x++) if(Credits[x].Name == N){
				Credits[x].Value += V;

				/*
				curstr = ::wxString::FromCDouble(Credits[x].Value, 2);
				database->ExecuteUpdate("update Credits set value = " +
																		curstr +
																		" where id = " +
																		wxString::Format("%d", Credits[x].Id) +
																		";");
				*/
				ChangeFundValue(tvCre, Credits[x].Id, Credits[x].Value);

				E = true;
				break;}
			if(!E){
				if(Parsing){
					Credits[NCre].Id = id;
					Credits[NCre].Name = N;
					Credits[NCre].Value = V;}
				else{
					#ifdef _OMB_USE_CIPHER
						if(! TableExists(L"Credits", wxEmptyString, database)) ExecuteUpdate(database, cs_credits_v31.c_str()/*, false*/);
						wxString Sql = L"insert into Credits values (NULL, '" +
																				N +
																				L"', " +
																				curstr +
																				L");";
						ExecuteUpdate(database, Sql.c_str()/*, false*/);
					#else
						if(! database->TableExists(L"Credits")) database->ExecuteUpdate(cs_credits_v31);
						database->ExecuteUpdate(L"insert into Credits values (NULL, '" +
																			N +
																			L"', " +
																			curstr +
																			L");");
					#endif // _OMB_USE_CIPHER
				}
			}
			//Tot_Credits += V;
			/*
			if(!SortValues(tvCre)){
				Error(37,wxEmptyString);
				wxTheApp->Exit();
				return false;}
			*/
			break;
		case tvDeb:
			for(x = 0; x < NDeb; x++) if(Debts[x].Name == N){
				Debts[x].Value += V;
				/*
				curstr = ::wxString::FromCDouble(Debts[x].Value, 2);
				database->ExecuteUpdate(L"update Debts set value = " +
																		curstr +
																		L" where id = " +
																		wxString::Format(L"%d", Debts[x].Id) +
																		L";");
				*/
				ChangeFundValue(tvDeb, Debts[x].Id, Debts[x].Value);
				E = true;
				break;}
			if(!E){
				if(Parsing){
					Debts[NDeb].Id = id;
					Debts[NDeb].Name = N;
					Debts[NDeb].Value = V;}
				else{
					#ifdef _OMB_USE_CIPHER
						if(! TableExists(L"Debts", wxEmptyString, database)) ExecuteUpdate(database, cs_debts_v31.c_str()/*, false*/);
						wxString Sql = L"insert into Debts values (NULL, '" +
																				N +
																				L"', " +
																				curstr +
																				L");";
						ExecuteUpdate(database, Sql.c_str()/*, false*/);
					#else
						if(! database->TableExists(L"Debts")) database->ExecuteUpdate(cs_debts_v31);
						database->ExecuteUpdate(L"insert into Debts values (NULL, '" +
																			N +
																			L"', " +
																			curstr +
																			L");");
					#endif // _OMB_USE_CIPHER
				}
			}
			//Tot_Debts += V;
			/*
			if(!SortValues(tvDeb)){
				Error(37,wxEmptyString);
				wxTheApp->Exit();
				return false;}
			*/
			break;
		default:
			return false;}

	if(! Parsing){
		ParseDatabase();
		FileData.Modified=true;}
	return true;}

bool TData::DelValue(TTypeVal T,int I){
	if(I < 0) return false;

	#ifdef _OMB_USE_CIPHER
		wxString Sql;
	#endif // _OMB_USE_CIPHER

	switch(T){
		case tvFou:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Funds where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Funds where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
		break;
		case tvCre:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Credits where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Credits where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		case tvDeb:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Debts where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Debts where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		default:
			return false;}
	ParseDatabase();
	FileData.Modified=true;
	return true;}

bool TData::AddObject(TObjType T, int id, wxString N, wxString O, wxDateTime D){
	if(N.IsEmpty() || O.IsEmpty())return false;
	if (D == invalidDate) return false;

	if(! Parsing){
		N = DoubleQuote(N);
		O = DoubleQuote(O);}

	#ifdef _OMB_USE_CIPHER
		wxString Sql;
	#endif // _OMB_USE_CIPHER

	switch(T){
		case toPre:
			if(Parsing){
				Lent[NLen].Id = id;
				Lent[NLen].Name = N;
				Lent[NLen].Object = O;
				Lent[NLen].Alarm = D;}
			else{
				#ifdef _OMB_USE_CIPHER
					if(! TableExists(L"Loans", wxEmptyString, database)) ExecuteUpdate(database, cs_loans_v31.c_str()/*, false*/);
					Sql = L"insert into Loans values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +
																			L");";
					ExecuteUpdate(database, Sql.c_str()/*, false*/);
				#else
					if(! database->TableExists(L"Loans")) database->ExecuteUpdate(cs_loans_v31);
					database->ExecuteUpdate(L"insert into Loans values (NULL, '" +
																		N +
																		L"', '" +
																		O +
																		L"', " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L");");
				#endif // _OMB_USE_CIPHER
			}
			break;
		case toInP:
			if(Parsing){
				Borrowed[NBor].Id = id;
				Borrowed[NBor].Name = N;
				Borrowed[NBor].Object = O;
				Borrowed[NBor].Alarm = D;
			}
			else{
				#ifdef _OMB_USE_CIPHER
					if(! TableExists(L"Borrows", wxEmptyString, database)) ExecuteUpdate(database, cs_borrows_v31.c_str()/*, false*/);
					Sql = L"insert into Borrows values (NULL, '" +
																			N +
																			L"', '" +
																			O +
																			L"', " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +
																			L");";
					ExecuteUpdate(database, Sql.c_str()/*, false*/);
				#else
					if(! database->TableExists(L"Borrows")) database->ExecuteUpdate(cs_borrows_v31);
					database->ExecuteUpdate(L"insert into Borrows values (NULL, '" +
																		N +
																		L"', '" +
																		O +
																		L"', " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L");");
				#endif // _OMB_USE_CIPHER
			}
		}
	if(! Parsing){
		FileData.Modified = true;
		ParseDatabase();}
  return true;}

bool TData::DelObject(TObjType T,int I){
	if(I < 0) return false;

	#ifdef _OMB_USE_CIPHER
		wxString Sql;
	#endif // _OMB_USE_CIPHER

	switch(T){
		case toPre:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Loans where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Loans where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		case toInP:
			#ifdef _OMB_USE_CIPHER
				Sql = L"delete from Borrows where id = " +
																		wxString::Format(L"%d", I) +
																		L";";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
			#else
				database->ExecuteUpdate(L"delete from Borrows where id = " +
																	wxString::Format(L"%d", I) +
																	L";");
			#endif // _OMB_USE_CIPHER
			break;
		default:
			return false;}
  FileData.Modified=true;
  ParseDatabase();
  return true;}

bool TData::AddShopItem(int id, wxString N, wxDateTime A){
	if(N.IsEmpty())return false;
	if(A == invalidDate) return false;
	if(Parsing){
		ShopItems[NSho].Id = id;
		ShopItems[NSho].Name = N;
		ShopItems[NSho].Alarm = A;
	}
	else{
		N = DoubleQuote(N);

		#ifdef _OMB_USE_CIPHER
			if(! TableExists(L"Shoplist", wxEmptyString, database)) ExecuteUpdate(database, cs_shoplist.c_str()/*, false*/);
			wxString Sql = L"insert into Shoplist values (NULL, '" +
																	N +
																	L"', " +
																	wxString::Format(L"%d", (int) A.GetTicks()) +
																	L");";
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
		#else
			if(! database->TableExists(L"Shoplist")) database->ExecuteUpdate(cs_shoplist);
			database->ExecuteUpdate(L"insert into Shoplist values (NULL, '" +
																N +
																L"', " +
																wxString::Format(L"%d", (int) A.GetTicks()) +
																L");");
		#endif // _OMB_USE_CIPHER
	}

	if(! Parsing){
  	FileData.Modified=true;
  	ParseDatabase();}
  return true;}

bool TData::DelShopItem(int I){
	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"delete from Shoplist where id = " +
																wxString::Format(L"%d", I) +
																L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"delete from Shoplist where id = " +
															wxString::Format(L"%d", I) +
															L";");
  #endif // _OMB_USE_CIPHER
  FileData.Modified=true;
  ParseDatabase();
  return true;}

bool TData::AddDate(int id, wxDateTime D, double T){
	if(!D.IsValid())return false;
	if(! Parsing){
		#ifdef __OPENMONEYBOX_EXE__
			int M, A;
			A = Day.GetYear();
			M = Day.GetMonth() + 1;
			if((A != FileData.Year) || (M != FileData.Month)){
				//#ifdef __OPENMONEYBOX_EXE__
					if(AutoConvert()){
						wxCommandEvent evt(wxEVT_NULL,0);
						frame->TextConvClick(evt);}
				//#endif // __OPENMONEYBOX_EXE__
				wxString Path=::wxPathOnly(FileData.FileName);
				#ifdef __WXMSW__
					wxString month=::wxString::Format("%02d",FileData.Month);
				#else
					#if(wxCHECK_VERSION(3,0,0))
						wxString month=::wxString::Format(L"%02d",int(FileData.Month));
					#else
						wxString month=::wxString::Format(L"%02d",FileData.Month);
					#endif
				#endif // __WXMSW__
				wxString File = GetDocPrefix();
				File+=L"_";
				#ifdef __WXMSW__
					File+=::wxString::Format("%d",FileData.Year);
				#else
					File+=::wxString::Format(L"%d",FileData.Year);
				#endif // __WXMSW__
				File += L"-" + month + L".omb";
				FileData.Modified = true;

				//#ifdef __OPENMONEYBOX_EXE__
					wxString master = Get_MasterDB();
					wxString trailname = ::wxString::Format(L"_%d_%02d", FileData.Year, int(FileData.Month));

					bool has_cat = Prepare_MasterDB(master, trailname);

					#ifdef _OMB_USE_CIPHER
						sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);

						sqlite3 *pBk;
						sqlite3_open((Path + L"/" + File).c_str(), &pBk);
						sqlite3_backup *pBackup;
						pBackup = sqlite3_backup_init(pBk, "main", database, "main");
						if( pBackup ){
							(void)sqlite3_backup_step(pBackup, -1);
							(void)sqlite3_backup_finish(pBackup);
						}
					#else
						database->ReleaseSavepoint(L"rollback");
						database->Backup(Path + L"/" + File);
					#endif // _OMB_USE_CIPHER

					Archive_inMaster(master, trailname, ! has_cat);

					#ifdef _OMB_USE_CIPHER
						ExecuteUpdate(database, "DELETE FROM Transactions");
						ExecuteUpdate(database, "VACUUM");
						ExecuteUpdate(database, "SAVEPOINT rollback;");
					#else
						database->ExecuteUpdate("DELETE FROM Transactions");
						database->Vacuum();
						database->Savepoint(L"rollback");
					#endif // _OMB_USE_CIPHER

				//#endif // __OPENMONEYBOX_EXE__

				//CategoryDB->Sort(false);

				NLin = 0;
				FileData.Month = int (D.GetMonth()) + 1;
				FileData.Year = D.GetYear();}
			#endif // __OPENMONEYBOX_EXE__
	}
	else	// Robustness code to avoid creation of duplicate date entries
		for(int i = NLin - 1; i >= 0; i--)if(IsDate(i))
			if(Lines[i].Date == D)
				#ifndef __OMBCONVERT_BIN__
					return true;
				#else
					return false;
				#endif // __OMBCONVERT_BIN__

	if(Parsing){
		Lines[NLin].Id= id;
		Lines[NLin].IsDate = true;
		Lines[NLin].Date = D;
		Lines[NLin].Time = wxInvalidDateTime;
		Lines[NLin].Type = toNULL;
		Lines[NLin].Value = FormDigits(T);
		Lines[NLin].Reason = wxEmptyString;
		Lines[NLin].CategoryIndex = -1;}
	else
		#ifdef _OMB_USE_CIPHER
      #ifdef __OPENSUSE__
        {
          wxString Sql = L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		#if( wxCHECK_VERSION(3,0,0) )
																			ombFromCDouble(T) +
																		#else
																			FormDigits(T) +
																		#endif
																		L"', 0, -1);";
          ExecuteUpdate(database, Sql.c_str());
        }
      #else
        ExecuteUpdate(database, L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		#if( wxCHECK_VERSION(3,0,0) )
																			ombFromCDouble(T) +
																		#else
																			FormDigits(T) +
																		#endif
																		L"', 0, -1);");
      #endif // __OPENSUSE__
		#else
			database->ExecuteUpdate(L"insert into Transactions values (NULL, 1, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", 0, 0, '" +
																		#if( wxCHECK_VERSION(3,0,0) )
																			ombFromCDouble(T) +
																		#else
																			FormDigits(T) +
																		#endif
																		L"', 0, -1);");
		#endif // _OMB_USE_CIPHER

  FileData.Modified=true;
  return true;}

bool TData::AddOper(int id, wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, long N){
	if(T==toNULL||V.IsEmpty()||M.IsEmpty()) return false;
	//wxString Tot=FormDigits(Tot_Funds);

	if(Parsing){
		Lines[NLin].Id = id;
		Lines[NLin].IsDate = false;
		Lines[NLin].Date = D;
		Lines[NLin].Time = O;
		Lines[NLin].Type = T;
		Lines[NLin].Value = V;
		Lines[NLin].Reason = M;
		Lines[NLin].CategoryIndex = N;}
	else{
		M = DoubleQuote(M);
		if(T > 8) V = DoubleQuote(V);

		// Last date check
		bool found = false;
		if(NLin < 1){
			AddDate(-1, D, Tot_Funds);
			found = true;}
		else for(int i = NLin - 1; i >= 0; i--)if(IsDate(i)){
			if( Lines[i].Date.IsSameDate(D)){
				found = true;
				break;}}
		if (! found) AddDate(-1, D, Tot_Funds);

		#ifdef _OMB_USE_CIPHER
			wxString Sql = L"insert into Transactions values (NULL, 0, " +
																			wxString::Format(L"%d", (int) D.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", (int) O.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", T) +
																			L", '" +
																			V +
																			L"', '" +
																			M +
																			L"', " +
																			wxString::Format(L"%ld", N) +

																			L");";
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
		#else
			database->ExecuteUpdate(L"insert into Transactions values (NULL, 0, " +
																		wxString::Format(L"%d", (int) D.GetTicks()) +
																		L", " +
																		wxString::Format(L"%d", (int) O.GetTicks()) +
																		L", " +
																		wxString::Format(L"%d", T) +
																		L", '" +
																		V +
																		L"', '" +
																		M +
																		L"', " +
																		wxString::Format(L"%ld", N) +
																		L");");
		#endif // _OMB_USE_CIPHER
		FileData.Modified=true;
		ParseDatabase();
		//Sort();
	}
	return true;}

#ifndef _OMB_USE_CIPHER
bool TData::OpenDatabase(wxString File){
	bool file_exist = ::wxFileExists(File);
	wxString Name;

	#ifndef __OMBCONVERT_BIN__
    if(file_exist){
    	if(! CheckAndPromptForConversion(File, false)){
    		Parsing = false;
    		return false;}}
	#endif // __OMBCONVERT_BIN__

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Opening database...");
	#endif // __OPENMONEYBOX_EXE__
	database->Open(File, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Database loaded.");
	#endif // __OPENMONEYBOX_EXE__

	if(file_exist){
		FileData.FileName = File;
		::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
		FileData.FileView = Name;

		#ifndef __OMBCONVERT_BIN__
			// Check if present Android backup to be archived in master db
			// Read backup entry
			wxSQLite3Table Table;
			Table = database->GetTable(L"select data from Information where id = " +
																	wxString::Format(L"%d", dbMeta_mobile_export) +
																	L";");
			Table.SetRow(0);
			wxString archive = Table.GetString(0, wxEmptyString);

			if(! archive.IsEmpty()){
				wxString Path, backup_file;
				::wxFileName::SplitPath(File, &Path, NULL, NULL, wxPATH_NATIVE);

				#ifdef __WXGTK__
          backup_file = Path + L"/_" + archive;
        #elif defined ( __WXMSW__ )
          backup_file = Path + "\\_" + archive;
        #endif // __WXGTK__

				if(::wxFileExists(backup_file)){
					wxString master = Get_MasterDB();
					wxString trailname = L"_" + archive;
					bool has_cat = Prepare_MasterDB(master, trailname);

					#if ( wxCHECK_VERSION(3, 0, 0) )
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS master;", master));
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS ext;", backup_file));

						// Archive data in master database from external archive
						database->ExecuteUpdate(L"INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM ext.Transactions;");
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM ext.Funds%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM ext.Credits%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM ext.Debts%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM ext.Loans%s;", trailname, trailname));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM ext.Borrows%s;", trailname, trailname));
					#else
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS master;", master.c_str()));
						database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS ext;", backup_file.c_str()));

						// Archive data in master database from external archive
						database->ExecuteUpdate(L"INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM ext.Transactions;");
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM ext.Funds%s;", trailname.c_str(), trailname.c_str()));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM ext.Credits%s;", trailname.c_str(), trailname.c_str()));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM ext.Debts%s;", trailname.c_str(), trailname.c_str()));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM ext.Loans%s;", trailname.c_str(), trailname.c_str()));
						database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM ext.Borrows%s;", trailname.c_str(), trailname.c_str()));
					#endif

					if(! has_cat) database->ExecuteUpdate(L"INSERT INTO master.Categories SELECT * FROM ext.Categories;");

					database->ExecuteUpdate(L"DETACH DATABASE 'master';");
					database->ExecuteUpdate(L"DETACH DATABASE 'ext';");

					database->ExecuteUpdate(L"update Information set data = \"\" where id = " +
																wxString::Format(L"%d", dbMeta_mobile_export) +
																L";");
					::wxRemoveFile(backup_file);
				}
			}
		#endif // __OMBCONVERT_BIN__

		//database->Backup(BackupName());
	}
	else database->ExecuteUpdate(L"pragma user_version = " +
																						::wxString::Format(L"%d", 31) +
																						L";");

	// Set restore savepoint
	database->Savepoint(L"rollback");
	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Rollback savepoint set in database");
	#endif // __OPENMONEYBOX_EXE__

	#if wxCHECK_VERSION(3, 0, 0)
		// Write file metadata
		wxString metadata;
		metadata = L"OS: ";
		#ifdef __WXGTK__
			metadata += wxGetLinuxDistributionInfo().Description;
		#elif defined ( __WXMSW__)
			metadata += wxGetOsDescription();
		#else
			,etadata += L"unknown";
		#endif // __WXGTK__
		metadata += L"\n\n";
		#ifndef __OMBCONVERT_BIN__
			metadata += wxGetLibraryVersionInfo().ToString();
		#else
			metadata += _("Converted with ombconvert");
		#endif // __OMBCONVERT_BIN__
	#endif // wxCHECK_VERSION
	if(! database->TableExists(L"Information")){
		database->ExecuteUpdate(cs_information);

		#if wxCHECK_VERSION(3, 0, 0)
			database->ExecuteUpdate(L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');");
		#endif // wxCHECK_VERSION
		database->ExecuteUpdate(L"insert into Information values (" +
															wxString::Format(L"%d", dbMeta_default_fund) +
															L", 'default');");
		database->ExecuteUpdate(L"insert into Information values (" +
															wxString::Format(L"%d", dbMeta_mobile_export) +
															L", '');");
	}
	#if wxCHECK_VERSION(3, 0, 0)
		else database->ExecuteUpdate(L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";");
	#endif // wxCHECK_VERSION

	// Set currency when if not present (added later in dbVersion 31)
	wxSQLite3Table currencyTable;
	currencyTable = database->GetTable(L"select data from Information where id = " +
															wxString::Format(L"%d", dbMeta_currency) +
															L";");
	if(currencyTable.GetRowCount() == 0){
		wxString curr = GetCurrencySymbol();
		database->ExecuteUpdate(L"insert into Information values (" +
															wxString::Format(L"%d", dbMeta_currency) +
															L", '" +
															curr +
															L"');");
	}

	#ifdef __OPENMONEYBOX_EXE__
		wxLogMessage(L"Database metadata updated");
	#endif // __OPENMONEYBOX_EXE__

	// Transaction table init
	if(! database->TableExists(L"Transactions")) database->ExecuteUpdate(cs_transactions_v31);

	// Category table init
	if(! database->TableExists(L"Categories")) database->ExecuteUpdate(cs_categories);

	ParseDatabase();
	FileData.Modified = false;

	return true;}
#endif // _OMB_USE_CIPHER

double TData::GetTot(TTypeVal T){
  double Ret;
  switch(T){
    case tvFou:
		Ret=Tot_Funds;
    break;
    case tvCre:
    Ret=Tot_Credits;
    break;
    case tvDeb:
    Ret=Tot_Debts;
    break;
    default:Ret=-1;}
  return Ret;}

bool TData::IsDate(int R){
  bool Ret=Lines[R].IsDate;
  return Ret;}

/*
void TData::Sort(void){
	if(NLin < 2)return;
	int Min;
	for(int i = 0; i < NLin - 1; i++){
		Min=i;
		for(int j = i + 1; j < NLin; j++){
			if(IsDate(Min)){
				if(Lines[Min].Date>Lines[j].Date)Min=j;}
			else{
				if(Lines[Min].Date>Lines[j].Date)Min=j;
				else if(Lines[Min].Date==Lines[j].Date){
					if(IsDate(j))Min=j;
					else{
					#if(wxCHECK_VERSION(3,0,0))
						// NOTE (igor#1#): Workaround necessary due to use of wxTimePickerCtrl
						Lines[Min].Time.SetYear(1904);						// use leap year to avoid exceptions
						Lines[Min].Time.SetMonth(Day.GetMonth());
						Lines[Min].Time.SetDay(1);
						Lines[j].Time.SetYear(1904);							// use leap year to avoid exceptions
						Lines[j].Time.SetMonth(Day.GetMonth());
						Lines[j].Time.SetDay(1);
					#endif

					if(Lines[Min].Time>Lines[j].Time)Min=j;}

					}}}
		if(Min>i){
			TLine Tmp=Lines[i];
			Lines[i]=Lines[Min];
			Lines[Min]=Tmp;}}}
*/

TData::~TData(void){
	#ifdef _OMB_USE_CIPHER
		if(database != NULL) sqlite3_close(database);	// NOTE (igor#1#): To be debugged
	#else
		if(database->IsOpen())	database->Close();
	#endif // _OMB_USE_CIPHER

	delete[] Funds;
	delete[] Credits;
	delete[] Debts;
	delete[] Lent;
	delete[] Borrowed;
	delete[] ShopItems;
	delete[] Lines;
	MattersBuffer->Clear();
	delete MattersBuffer;}

/*
bool TData::SortValues(TTypeVal T){
  int Min, id, x, y;
	wxString N;
	double V;
	switch(T){
		case tvFou:
		for(x = 0; x < NFun; x++){
			Min=x;
			for(y = x + 1; y < NFun; y++)if(Funds[Min].Name > Funds[y].Name)Min = y;
			if(Min!=x){
				id = Funds[x].Id;
				N=Funds[x].Name;
				V=Funds[x].Value;
				Funds[x].Id = Funds[Min].Id;
				Funds[x].Name=Funds[Min].Name;
				Funds[x].Value=Funds[Min].Value;
				Funds[Min].Id = id;
				Funds[Min].Name=N;
				Funds[Min].Value=V;}}
    break;
    case tvCre:
    for(x=0;x<NCre;x++){
      Min=x;
      for(y=x+1;y<NCre;y++)if(Credits[Min].Name>Credits[y].Name)Min=y;
      if(Min!=x){
        id = Credits[x].Id;
        N=Credits[x].Name;
        V=Credits[x].Value;
        Credits[x].Id = Credits[Min].Id;
        Credits[x].Name=Credits[Min].Name;
        Credits[x].Value=Credits[Min].Value;
        Credits[Min].Id = id;
        Credits[Min].Name=N;
        Credits[Min].Value=V;}}
    break;
    case tvDeb:
    for(x = 0; x < NDeb; x++){
      Min=x;
      for(y = x + 1; y < NDeb; y++)if(Debts[Min].Name>Debts[y].Name)Min=y;
      if(Min!=x){
        id = Debts[x].Id;
        N=Debts[x].Name;
        V=Debts[x].Value;
        Debts[x].Id = Debts[Min].Id;
        Debts[x].Name=Debts[Min].Name;
        Debts[x].Value=Debts[Min].Value;
        Debts[Min].Id = id;
        Debts[Min].Name=N;
        Debts[Min].Value=V;}}
    break;
    default:
    return false;}
  return true;}
*/

/*
// NOTE (igor#1#): Do not remove, may be restored in future
void TData::CheckMemory(void){
	int x=NFon/MAX_FUNDS*100;
	if(x>90)NotEnoughMemory=true;}
*/

/*
bool TData::SetTot(TTypeVal T, double Tot){
  switch(T){
    case tvFou:
		Tot_Funds=Tot;
    break;
    case tvCre:
    Tot_Credits=Tot;
    break;
    case tvDeb:
    Tot_Debts=Tot;
    break;
    default:
    return false;}
  return true;}
*/

/*
bool TData::UpdateMatters(TOpType T){
	bool Ex;
  int i;
  unsigned int j;
  MattersBuffer->Clear();
  for(i = 0; i < NLin; i++)if(Lines[i].Type==T){
		Ex=false;
		for(j=0;j<MattersBuffer->Count();j++)if(MattersBuffer->Item(j).CmpNoCase(Lines[i].Reason)==0){
			Ex=true;
			break;}
		if(!Ex)MattersBuffer->Add(Lines[i].Reason);}
	MattersBuffer->Sort();
	return true;}
*/

/*
#ifndef __OMBCONVERT_BIN__
	void TData::XMLExport(wxString F1,wxString F2){
		bool FirstDate=false;	// set when first date is inserted
		wxString App;	// line to append
		double cur; // value storage
		//wxArrayString RemarkTokens;
		// path selection
		wxString dir = wxDirSelector(_("Select a folder"),GetBilDocPath());
		if(dir.IsEmpty())return;
		::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		// Folder creation
		#ifdef __WXMSW__
			dir = dir + L"\\" + GetDocPrefix() + L"_" + ::wxString::Format("%d", FileData.Year) + L"-"
				+ ::wxString::Format("%02d", FileData.Month);
		#else
			dir = dir + L"/" + GetDocPrefix() + L"_" + ::wxString::Format(L"%d", FileData.Year);
			#if(wxCHECK_VERSION(3,0,0))
				dir = dir + L"-" + ::wxString::Format(L"%02d", int(FileData.Month));
			#else
				dir = dir + L"-" + ::wxString::Format(L"%02d", FileData.Month);
			#endif
		#endif // __WXMSW__
		if(!wxDirExists(dir))if(!wxMkdir(dir,0777))return;
		// xsl template copy
		#ifdef __WXMSW__
			wxString xsltempl=GetInstallationPath()+L"\\data";
		#else
			wxString xsltempl=GetDataDir();
		#endif // __WXMSW__
		#ifdef __WXMSW__
			wxLanguage Lan=FindLang();
		#endif
		switch(Lan){
			case wxLANGUAGE_ITALIAN:
				#ifndef __WXMSW__
					xsltempl+=L"/it/";
				#else
					xsltempl+=L"\\it\\";
				#endif // __WXMSW__
				break;
			default:
				#ifndef __WXMSW__
					xsltempl+=L"/en/";
				#else
					xsltempl+=L"\\en\\";
				#endif // __WXMSW__
		}
		xsltempl += L"ombexport.xsl";
		wxCopyFile(xsltempl, dir + L"/ombexport.xsl");
		// xml document creation
		wxString xmlfile = dir + L"/" + GetDocPrefix() + L".xml";
		wxTextFile *file=new wxTextFile(xmlfile);
		file->Create();
		file->AddLine(L"<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
		#ifdef __WXMSW__
			file->AddLine(L"<!--File Created By OpenMoneyBox 3.1 on " + wxDateTime::Today().Format(L"%d/%m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#else
			file->AddLine(L"<!--File Created By OpenMoneyBox 3.1 on " + wxDateTime::Today().Format(L"%d/%02m/%Y") + L" "+wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#endif // __WXMSW__
		file->AddLine(L"<?xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"?>");
		file->AddLine(L"<groups><headers>");
		wxString Header=_("Monthly report of");
		#ifdef __WXMSW__
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format("%d",FileData.Year)+L"\" />");
		#else
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format(L"%d",FileData.Year)+L"\" />");
		#endif // __WXMSW__
		file->AddLine(L"</headers><days>");
		// line parsing
		for(int i = 0; i < NLin; i++){
			App=wxEmptyString;
			if(IsDate(i)){
				if(!FirstDate)FirstDate=true;
				else	file->AddLine(L"</day>");
				App+=L"<day date=\"";
				#ifdef __WXMSW__
					App+=Lines[i].Date.Format("%a %d/%m/%Y",wxDateTime::Local);
				#else
					App+=Lines[i].Date.Format(L"%a %d/%m/%Y",wxDateTime::Local);
				#endif // __WXMSW__
				App+=L"\" total=\"";
				Lines[i].Value.ToDouble(&cur);
				App+=FormDigits(cur);
				App+=L"\">";
				file->AddLine(App);}
			else{
				App+=L"<item time=\"";
				#ifdef __WXMSW__
					App+=Lines[i].Time.Format("%H:%M",wxDateTime::Local);
				#else
					App+=Lines[i].Time.Format(L"%H:%M",wxDateTime::Local);
				#endif // __WXMSW__
				App+=L"\" type=\"";
				switch(Lines[i].Type){
					case toGain:
					App+=L"1";
					break;
					case toExpe:
					App+=L"2";
					break;
					case toSetCre:
					App+=L"3";
					break;
					case toRemCre:
					App+=L"4";
					break;
					case toConCre:
					App+=L"5";
					break;
					case toSetDeb:
					App+=L"6";
					break;
					case toRemDeb:
					App+=L"7";
					break;
					case toConDeb:
					App+=L"8";
					break;
					case toGetObj:
					App+=L"9";
					break;
					case toGivObj:
					App+=L"10";
					break;
					case toLenObj:
					App+=L"11";
					break;
					case toBakObj:
					App+=L"12";
					break;
					case toBorObj:
					App+=L"13";
					break;
					case toRetObj:
					App+=L"14";
					break;
					default:
					App+=L"0";
					break;}
				App += L"\" value=\"";
				if(Lines[i].Type<9){
					Lines[i].Value.ToDouble(&cur);
					App += FormDigits(cur);}
				else App += Lines[i].Value;
				App+=L"\" reason=\"";

				if(Lines[i].Reason.Find(L"\"") != wxNOT_FOUND){
					wxString reas = Lines[i].Reason;
					reas.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
					App += reas;
				}
				else App+=Lines[i].Reason;

				App+=L"\" category=\"";
				App=SubstSpecialChars(App);
				file->AddLine(App);
				if(Lines[i].CategoryIndex != -1)
					for(int j = 0; j < NCat; j++)if(Categories[j].Id == Lines[i].CategoryIndex){
						file->AddLine(Categories[j].Name);
						break;}
				file->AddLine(L"\"/>");}}
		// file completion
		file->AddLine(L"</day></days></groups>");
		file->Write(wxTextFileType_Unix,wxConvUTF8);
		// file closure
		file->Close();
		delete file;
		// Logo picture copy
		#ifndef __WXMSW__
			xsltempl=GetDataDir()+L"logo.png";
		#else
			xsltempl=GetInstallationPath()+L"\\data\\logo.png";
		#endif
		wxCopyFile(xsltempl,dir+L"/logo.png");
		// Chart copy
		wxCopyFile(F1, dir+L"/chart1.png");
		wxCopyFile(F2, dir+L"/chart2.png");
		::wxEndBusyCursor();}
#endif // __OMBCONVERT_BIN__
*/

/*
#ifndef __OMBCONVERT_BIN__
	void TData::XMLExport_archive(wxString F1, wxString F2, wxDateTime date){
		bool FirstDate=false;	// set when first date is inserted
		wxString App;	// line to append
		double cur; // value storage
		// path selection
		wxString dir = wxDirSelector(_("Select a folder"),GetBilDocPath());
		if(dir.IsEmpty())return;
		::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		// Folder creation
		#ifdef __WXMSW__
			dir = dir + L"\\" + GetDocPrefix() + L"_" + date.Format(L"%Y-%m");
		#else
			dir = dir + L"/" + GetDocPrefix() + L"_" + date.Format(L"%Y-%m");
		#endif // __WXMSW__
		if(!wxDirExists(dir))if(!wxMkdir(dir,0777))return;
		// xsl template copy
		#ifdef __WXMSW__
			wxString xsltempl=GetInstallationPath()+L"\\data";
		#else
			wxString xsltempl=GetDataDir();
		#endif // __WXMSW__
		#ifdef __WXMSW__
			wxLanguage Lan=FindLang();
		#endif
		switch(Lan){
			case wxLANGUAGE_ITALIAN:
				#ifndef __WXMSW__
					xsltempl+=L"/it/";
				#else
					xsltempl+=L"\\it\\";
				#endif // __WXMSW__
				break;
			default:
				#ifndef __WXMSW__
					xsltempl+=L"/en/";
				#else
					xsltempl+=L"\\en\\";
				#endif // __WXMSW__
		}
		xsltempl += L"ombexport.xsl";
		wxCopyFile(xsltempl, dir + L"/ombexport.xsl");
		// xml document creation
		wxString xmlfile = dir + L"/" + GetDocPrefix() + L".xml";
		wxTextFile *file=new wxTextFile(xmlfile);
		file->Create();
		file->AddLine(L"<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
		#ifdef __WXMSW__
			file->AddLine(L"<!--File Created By OpenMoneyBox 3.1 on " + wxDateTime::Today().Format(L"%d/%m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#else
			file->AddLine(L"<!--File Created By OpenMoneyBox 3.1 on " + wxDateTime::Today().Format(L"%d/%02m/%Y") + L" " + wxDateTime::Now().Format(L"%H:%M") + L"-->");
		#endif // __WXMSW__
		file->AddLine(L"<?xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"?>");
		file->AddLine(L"<groups><headers>");
		wxString Header=_("Monthly report of");
		#ifdef __WXMSW__
			file->AddLine(L"<title heading=\""+Header+L" \" month=\""+IntToMonth(FileData.Month)+L" "+::wxString::Format("%d",FileData.Year)+L"\" />");
		#else
			file->AddLine(L"<title heading=\"" + Header + L" \" month=\"" + IntToMonth(date.GetMonth() + 1) + L" " +
											::wxString::Format(L"%d", date.GetYear()) + L"\" />");
		#endif // __WXMSW__
		file->AddLine(L"</headers><days>");

		int min_date, max_date;
		wxDateTime min_datetime, max_datetime, d_date, time;
		wxSQLite3Table transaction_table;
		bool is_date;
		wxString value_string, reas;

		min_datetime = date;
		min_datetime.SetDay(1);
		min_datetime.SetHour(0);
		min_datetime.SetMinute(0);
		min_datetime.SetSecond(0);
		min_datetime.SetMillisecond(0);
		min_date = min_datetime.GetTicks();

		max_datetime = date;
		max_datetime.SetToLastMonthDay(max_datetime.GetMonth(), max_datetime.GetYear());
		max_datetime.SetHour(23);
		max_datetime.SetMinute(59);
		max_datetime.SetSecond(59);
		max_datetime.SetMillisecond(999);
		max_date = max_datetime.GetTicks();

		transaction_table = database->GetTable(::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d;", min_date, max_date));

		// line parsing
		for(int i = 0; i < transaction_table.GetRowCount(); i++){
			transaction_table.SetRow(i);
			App = wxEmptyString;
			is_date = transaction_table.GetInt(L"isdate", false);
			if(is_date){
				if(! FirstDate) FirstDate = true;
				else	file->AddLine(L"</day>");
				App += L"<day date=\"";
				d_date = wxDateTime((time_t) transaction_table.GetInt(2, 0));
				#ifdef __WXMSW__
					App += d_date.Format("%a %d/%m/%Y", wxDateTime::Local);
				#else
					App += d_date.Format(L"%a %d/%m/%Y", wxDateTime::Local);
				#endif // __WXMSW__
				App += L"\" total=\"";
				value_string = transaction_table.GetString(5, wxEmptyString);
				#if( wxCHECK_VERSION(3,0,0) )
					value_string.ToCDouble(&cur);
				#else
					value_string.ToDouble(&cur);
				#endif
				App += FormDigits(cur);
				App += L"\">";
				file->AddLine(App);}
			else{
				App+=L"<item time=\"";
				#ifdef __WXMSW__
					App+=Lines[i].Time.Format("%H:%M",wxDateTime::Local);
				#else
					time = wxDateTime((time_t) transaction_table.GetInt(3, 0));
					App += time.Format(L"%H:%M",wxDateTime::Local);
				#endif // __WXMSW__
				App += L"\" type=\"";
				switch(transaction_table.GetInt(4, 0)){
					case toGain:
						App += L"1";
						break;
					case toExpe:
						App += L"2";
						break;
					case toSetCre:
						App += L"3";
						break;
					case toRemCre:
						App += L"4";
						break;
					case toConCre:
						App += L"5";
						break;
					case toSetDeb:
						App += L"6";
						break;
					case toRemDeb:
						App += L"7";
						break;
					case toConDeb:
						App += L"8";
						break;
					case toGetObj:
						App += L"9";
						break;
					case toGivObj:
						App += L"10";
						break;
					case toLenObj:
						App += L"11";
						break;
					case toBakObj:
						App += L"12";
						break;
					case toBorObj:
						App += L"13";
						break;
					case toRetObj:
						App += L"14";
						break;
					default:
						App += L"0";
						break;}
				App += L"\" value=\"";
				value_string = transaction_table.GetString(5, wxEmptyString);
				if(transaction_table.GetInt(4, 0) < 9){
					#if( wxCHECK_VERSION(3,0,0) )
						value_string.ToCDouble(&cur);
					#else
						value_string.ToDouble(&cur);
					#endif
					App += FormDigits(cur);}
				else App += value_string;
				App += L"\" reason=\"";

				reas = transaction_table.GetString(6, wxEmptyString);
				if(reas.Find(L"\"") != wxNOT_FOUND)
					reas.Replace(L"\"", L"[OMB_ESCAPEQUOTES]", true);	// intermediate string to be processed by SubstSpecialChars
				App += reas;

				App += L"\" category=\"";
				App = SubstSpecialChars(App);
				file->AddLine(App);
				if(transaction_table.GetInt(7, 0) != -1)
					for(int j = 0; j < NCat; j++)if(Categories[j].Id == transaction_table.GetInt(7, 0)){
						file->AddLine(Categories[j].Name);
						break;}
				file->AddLine(L"\"/>");}}
		// file completion
		file->AddLine(L"</day></days></groups>");
		file->Write(wxTextFileType_Unix,wxConvUTF8);
		// file closure
		file->Close();
		delete file;
		// Logo picture copy
		#ifndef __WXMSW__
			xsltempl=GetDataDir()+L"logo.png";
		#else
			xsltempl=GetInstallationPath()+L"\\data\\logo.png";
		#endif
		wxCopyFile(xsltempl,dir+L"/logo.png");
		// Chart copy
		wxCopyFile(F1, dir+L"/chart1.png");
		wxCopyFile(F2, dir+L"/chart2.png");
		::wxEndBusyCursor();}
#endif // __OMBCONVERT_BIN__
*/

#ifndef __OMBCONVERT_BIN__
void TData::UpdateProgress(int i){
		if(Progress != NULL){
			bool nonzero = false;
			if(Progress->GetValue()) nonzero=true;
			Progress->SetValue(i);
			if(nonzero){
				if(i == 0) Progress->Show(false);}
			else if(i > 0) Progress->Show(true);
			Progress->Update();
			wxTheApp->Yield(false);}}
#endif // __OMBCONVERT_BIN__

#if(wxCHECK_VERSION(3, 0, 0))
	wxString TData::ombFromCDouble(double value){
		// NOTE (igor#1#): Function added as workaround to precision loss of wxString::FromCDouble()
		#define TRIGGER 10000
		wxString result;
		if(value < TRIGGER) result = wxString::FromCDouble(value, 2);
		else{
			int lead = value / TRIGGER;
			value -= (lead * TRIGGER);
			result = wxString::FromDouble(lead);
			if(value < (TRIGGER / 10)) result += L"0";
			if(value < (TRIGGER / 100)) result += L"0";
			if(value < (TRIGGER / 1000)) result += L"0";
			if(value < (TRIGGER / 10000)) result += L"0";
			result += wxString::FromCDouble(value, 2);}
		return result;}
#endif

void TData::ParseDatabase(void){
	Parsing = true;

	#ifdef _OMB_USE_CIPHER
		bool is_date;
		int Rows, id, i;	// Rows: number of rows in the query table
											// id: id of table item
		long ind;					// ind: category index of transaction

		double val,
			lat, lon;
		TOpType type;
		wxString str, reason;
		wxDateTime date, time;
		wxSQLite3Table Table;

		// Read Funds
		if(TableExists(L"Funds", wxEmptyString, database)){
			Table = GetTable(database, "select * from Funds order by name;");
			NFun = 0;
			Tot_Funds = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvFou, id, str, val)){
					NFun++;
					Tot_Funds += val;}}}

		// Read default fund
		#ifdef __OPENSUSE__
      wxString Sql = L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";";
      Table = GetTable(database, Sql.c_str());
		#else
      Table = GetTable(database, L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";");
		#endif // __OPENSUSE__
		Table.SetRow(0);
		FileData.DefFund = Table.GetString(0, wxEmptyString);

		// Read Credits
		if(TableExists(L"Credits", wxEmptyString, database)){
			Table = GetTable(database, "select * from Credits order by name;");
			NCre = 0;
			Tot_Credits = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvCre, id, str, val)){
					NCre++;
					Tot_Credits += val;}}}

		// Read Debts
		if(TableExists(L"Debts", wxEmptyString, database)){
			Table = GetTable(database, "select * from Debts order by name;");
			NDeb = 0;
			Tot_Debts = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvDeb, id, str, val)){
					NDeb++;
					Tot_Debts += val;}}}

		// Read Loans
		if(TableExists(L"Loans", wxEmptyString, database)){
			Table = GetTable(database, "select * from Loans order by name;");
			NLen = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddObject(toPre, id, str, reason, date)) NLen++;}}

		// Read Borrows
		if(TableExists(L"Borrows", wxEmptyString, database)){
			Table = GetTable(database, "select * from Borrows order by name;");
			NBor = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddObject(toInP, id, str, reason, date)) NBor++;}}

		// Read Categories
		Table = GetTable(database, "select * from Categories order by name;");
		NCat = 0;
		Rows = Table.GetRowCount();
		for (i = 0; i < Rows; i++){
			Table.SetRow(i)	;
			id = Table.GetInt(0, 0);
			str = Table.GetString(1, wxEmptyString);
			is_date = Table.GetInt(2, 0);
			if(is_date)AddCategory(id, str);}

		// Read Transactions
		wxDateTime LastDate = wxDateTime(1, wxDateTime::Jan, 1970);
		Table = /*database->*/GetTable(database, "select * from Transactions order by date;");
		NLin = 0;
		Rows = Table.GetRowCount();
		if(Rows){
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				is_date = Table.GetInt(1, 0);
				date = wxDateTime((time_t) Table.GetInt(2, 0));
				if(date.IsLaterThan(LastDate)) LastDate = date;
				time = wxDateTime((time_t) Table.GetInt(3, 0));
				type = (TOpType) Table.GetInt(4, 0);
				str = Table.GetString(5, wxEmptyString);
				reason = Table.GetString(6, wxEmptyString);
				ind = Table.GetInt(7, 0);

				if(is_date){
					#if (wxCHECK_VERSION(3, 0, 0) )
						str.ToCDouble(&val);
					#else
						str.ToDouble(&val);
					#endif
					if(AddDate(id, date, val)) NLin++;}
				else{
					#if (wxCHECK_VERSION(3, 0, 0) )
						Table.GetAsString(9).ToCDouble(&lat);
						Table.GetAsString(10).ToCDouble(&lon);
					#else
						Table.GetAsString(9).ToDouble(&lat);
						Table.GetAsString(10).ToDouble(&lon);
					#endif
					if((int) type < 9){
						#if (wxCHECK_VERSION(3, 0, 0) )
							str.ToCDouble(&val);
						#else
							str.ToDouble(&val);
						#endif
						str = FormDigits(val);}
					if(AddOper(id, date, time, type, str, reason, ind)) NLin++;}}}
		else LastDate = wxDateTime::Today();

		FileData.Year = LastDate.GetYear();
		// Following code is a workaround for GetMonth() bug in Linux
		// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
		wxString temp;
		#ifdef __WXMSW__
			temp = LastDate.Format("%m", wxDateTime::Local).c_str();
		#else
			temp = LastDate.Format(L"%m", wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month, 10);

		Day = LastDate;

		// Read shopping list
		if(TableExists(L"Shoplist", wxEmptyString, database)){
			Table = GetTable(database, "select * from Shoplist order by item collate nocase;");
			NSho = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				ind = Table.GetInt(2, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddShopItem(id, str, date)) NSho++;}}
	#else
		bool is_date;
		int Rows, id, i;	// Rows: number of rows in the query table
											// id: id of table item
		long ind;					// ind: category index of transaction
		double val;
		TOpType type;
		wxString str, reason;
		wxDateTime date, time;
		wxSQLite3Table Table;

		// Read Funds
		if(database->TableExists(L"Funds")){
			Table = database->GetTable("select * from Funds order by name;");
			NFun = 0;
			Tot_Funds = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvFou, id, str, val)){
					NFun++;
					Tot_Funds += val;}}}

		// Read default fund
		Table = database->GetTable(L"select data from Information where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";");
		Table.SetRow(0);
		FileData.DefFund = Table.GetString(0, wxEmptyString);

		// Read Credits
		if(database->TableExists(L"Credits")){
			Table = database->GetTable("select * from Credits order by name;");
			NCre = 0;
			Tot_Credits = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvCre, id, str, val)){
					NCre++;
					Tot_Credits += val;}}}

		// Read Debts
		if(database->TableExists(L"Debts")){
			Table = database->GetTable("select * from Debts order by name;");
			NDeb = 0;
			Tot_Debts = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				val = Table.GetDouble(2, 0);
				if(AddValue(tvDeb, id, str, val)){
					NDeb++;
					Tot_Debts += val;}}}

		// Read Loans
		if(database->TableExists(L"Loans")){
			Table = database->GetTable("select * from Loans order by name;");
			NLen = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddObject(toPre, id, str, reason, date)) NLen++;}}

		// Read Borrows
		if(database->TableExists(L"Borrows")){
			Table = database->GetTable("select * from Borrows order by name;");
			NBor = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				reason = Table.GetString(2, wxEmptyString);
				ind = Table.GetInt(3, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddObject(toInP, id, str, reason, date)) NBor++;}}

		// Read Categories
		Table = database->GetTable("select * from Categories order by name;");
		NCat = 0;
		Rows = Table.GetRowCount();
		for (i = 0; i < Rows; i++){
			Table.SetRow(i)	;
			id = Table.GetInt(0, 0);
			str = Table.GetString(1, wxEmptyString);
			is_date = Table.GetInt(2, 0);
			if(is_date)AddCategory(id, str);}

		// Read Transactions
		wxDateTime LastDate = wxDateTime(1, wxDateTime::Jan, 1970);
		Table = database->GetTable("select * from Transactions order by date;");
		NLin = 0;
		Rows = Table.GetRowCount();
		if(Rows){
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				is_date = Table.GetInt(1, 0);
				date = wxDateTime((time_t) Table.GetInt(2, 0));
				if(date > LastDate) LastDate = date;
				time = wxDateTime((time_t) Table.GetInt(3, 0));
				type = (TOpType) Table.GetInt(4, 0);
				str = Table.GetString(5, wxEmptyString);
				reason = Table.GetString(6, wxEmptyString);
				ind = Table.GetInt(7, 0);
				if(is_date){
					#if (wxCHECK_VERSION(3, 0, 0) )
						str.ToCDouble(&val);
					#else
						str.ToDouble(&val);
					#endif
					if(AddDate(id, date, val)) NLin++;}
				else{
					if((int) type < 9){
						#if (wxCHECK_VERSION(3, 0, 0) )
							str.ToCDouble(&val);
						#else
							str.ToDouble(&val);
						#endif
						str = FormDigits(val);}
					if(AddOper(id, date, time, type, str, reason, ind)) NLin++;}}}
		else LastDate = wxDateTime::Today();

		FileData.Year = LastDate.GetYear();
		// Following code is a workaround for GetMonth() bug in Linux
		// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
		wxString temp;
		#ifdef __WXMSW__
			temp = LastDate.Format("%m", wxDateTime::Local).c_str();
		#else
			temp = LastDate.Format(L"%m", wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month, 10);

		Day = LastDate;

		// Read shopping list
		if(database->TableExists(L"Shoplist")){
			Table = database->GetTable("select * from Shoplist order by item collate nocase;");
			NSho = 0;
			Rows = Table.GetRowCount();
			for (i = 0; i < Rows; i++){
				Table.SetRow(i);
				id = Table.GetInt(0, 0);
				str = Table.GetString(1, wxEmptyString);
				ind = Table.GetInt(2, 0);
				if(ind == -1) date = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
				else date = wxDateTime((time_t) ind);
				if(AddShopItem(id, str, date)) NSho++;}}
	#endif // _OMB_USE_CIPHER

	Parsing = false;
}

void TData::SetDefaultFundValue(double value){
	for(int i = 0; i < NFun; i++)
		if(Funds[i].Name.CmpNoCase(FileData.DefFund) == 0){
			/*
			wxString curstr = ::wxString::FromCDouble(value, 2);
			database->ExecuteUpdate("update Funds set value = " +
																	curstr +
																	" where id = " +
																	wxString::Format("%d", Funds[i].Id) +
																	";");
			*/
			ChangeFundValue(tvFou, Funds[i].Id, value);

			ParseDatabase();
			break;}

}

void TData::SetDefaultFund(wxString def){
	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"update Information set data = '" +
																def +
																L"' where id = " +
																wxString::Format(L"%d", dbMeta_default_fund) +
																L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"update Information set data = '" +
															def +
															L"' where id = " +
															wxString::Format(L"%d", dbMeta_default_fund) +
															L";");
	#endif // _OMB_USE_CIPHER
	FileData.DefFund = def;
	FileData.Modified = true;
	ParseDatabase();
}

bool TData::ChangeFundValue(TTypeVal type, int id, double V){
	wxString table;
	switch(type){
		case tvFou:
			table = L"Funds";
			break;
		case tvCre:
			table = L"Credits";
			break;
		case tvDeb:
			table = L"Debts";
			break;
		default:
			table = wxEmptyString;}

	#if (wxCHECK_VERSION(3, 0, 0) )
		wxString curstr = ::wxString::FromCDouble(V, 2);
	#else
		wxString curstr = ::wxString::Format(L"%f", V);

		#ifndef __OMBCONVERT_BIN__
			switch(Lan){
				case wxLANGUAGE_ITALIAN:
					SubstLocaleDecimalSeparator(curstr, ',');
					break;
				default:
					;
			}
		#else
			SubstLocaleDecimalSeparator(curstr, ',');
		#endif // __OMBCONVERT_BIN__

	#endif	// wxCHECK_VERSION

	#ifdef _OMB_USE_CIPHER
		wxString Sql = L"update " + table + L" set value = " +
																curstr +
																L" where id = " +
																wxString::Format(L"%d", id) +
																L";";
		ExecuteUpdate(database, Sql.c_str()/*, false*/);
	#else
		database->ExecuteUpdate(L"update " + table + L" set value = " +
															curstr +
															L" where id = " +
															wxString::Format(L"%d", id) +
															L";");
	#endif // _OMB_USE_CIPHER

	ParseDatabase();
	FileData.Modified=true;
	return true;}

void TData::AddCategory(int id, wxString name){
	Categories[NCat].Id = id;
	Categories[NCat].Name = name;
	Categories[NCat].test = false;
	NCat++;}

#ifndef __OMBCONVERT_BIN__
	void TData::UpdateCategories(wxListBox *lbox){
		bool exists;
		int i, x;	// x: pointer in Data->Lines
							// E: pointer to existing value
		wxString value;

		bool master_exist = false;
		#ifdef _OMB_USE_CIPHER
			sqlite3 *db;
		#else
			wxSQLite3Database *db = new wxSQLite3Database();
		#endif // _OMB_USE_CIPHER

		wxString master = Get_MasterDB();
		if(::wxFileExists(master)){
			#ifdef _OMB_USE_CIPHER
				sqlite3_open(master.c_str(), &db);
				if(TableExists(L"Categories", master.c_str(), db)) master_exist = true;
				sqlite3_close(db);
			#else
				db->Open(master, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
				if(db->TableExists(L"Categories")) master_exist = true;
				else db->Close();
			#endif // _OMB_USE_CIPHER
		}

		for (x = 0; x < int (lbox->GetCount()); x++){
			value = lbox->GetString(x);
			value = DoubleQuote(value);
			lbox->SetString(x, value);
		}

		// Check removed items
		for(i = NCat - 1; i >= 0; i--){
			exists = false;
			value = Categories[i].Name;
			for (x = 0; x < int (lbox->GetCount()); x++)if(value == lbox->GetString(x)){
				Categories[i].test = true;
				break;}}

		#ifdef _OMB_USE_CIPHER
			wxString Sql;
		#endif // _OMB_USE_CIPHER

		for(i = NCat - 1; i >= 0; i--) if(! Categories[i].test){
			#ifdef _OMB_USE_CIPHER
				Sql = L"update Transactions set cat_index = -1 where cat_index = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";";
				ExecuteUpdate(database, Sql.c_str());

				Sql = L"update Categories set active = 0 where id = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";";
				ExecuteUpdate(database, Sql.c_str());
			#else
				database->ExecuteUpdate(L"update Transactions set cat_index = -1 where cat_index = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");

				database->ExecuteUpdate(L"update Categories set active = 0 where id = " +
																wxString::Format(L"%d", Categories[i].Id) +
																L";");
			#endif // _OMB_USE_CIPHER

			if(master_exist){
				#ifdef _OMB_USE_CIPHER
					Sql = L"update Transactions set cat_index = -1 where cat_index = " +
																		wxString::Format(L"%d", Categories[i].Id) +
																		L";";
					ExecuteUpdate(db, Sql.c_str());

					Sql = L"update Categories set active = 0 where id = " +
																		wxString::Format(L"%d", Categories[i].Id) +
																		L";";
					ExecuteUpdate(db, Sql.c_str());
				#else
					db->ExecuteUpdate(L"update Transactions set cat_index = -1 where cat_index = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";");

					db->ExecuteUpdate(L"update Categories set active = 0 where id = " +
																	wxString::Format(L"%d", Categories[i].Id) +
																	L";");
				#endif // _OMB_USE_CIPHER
			}
		}

		// Check added items
		for(i = 0; i < (int) lbox->GetCount(); i++){
			exists = false;
			value = lbox->GetString(i);
			for(x = 0; x < NCat; x++) if(value == Categories[x].Name){
				exists = true;
				break;}
			if(! exists){
				#ifdef _OMB_USE_CIPHER
					Sql = L"insert into Categories values (NULL, '" +
																			value +
																			L"', 1);";
					ExecuteUpdate(database, Sql.c_str()/*, false*/);
				#else
					database->ExecuteUpdate(L"insert into Categories values (NULL, '" +
																		value +
																		L"', 1);");
				#endif // _OMB_USE_CIPHER
				if(master_exist){
					#ifdef _OMB_USE_CIPHER
						Sql = L"insert into Categories values (NULL, '" +
																		value +
																		L"', 1);";
						ExecuteUpdate(db, Sql.c_str()/*, false*/);
					#else
						db->ExecuteUpdate(L"insert into Categories values (NULL, '" +
																		value +
																		L"', 1);");
					#endif // _OMB_USE_CIPHER
				}
			}
		}

		if(master_exist)
			#ifdef _OMB_USE_CIPHER
				sqlite3_close(db);
			#else
				db->Close();
			#endif // _OMB_USE_CIPHER

		FileData.Modified = true;
		ParseDatabase();
	}
#endif // __OMBCONVERT_BIN__

bool TData::Prepare_MasterDB(wxString dbPath, wxString trailname){
	// master:		database to attach and archive data in
	// trailname:	suffix for tables

	#ifdef _OMB_USE_CIPHER
		sqlite3 *db;
		wxString Sql;
	#else
		wxSQLite3Database *db = new wxSQLite3Database();
	#endif // _OMB_USE_CIPHER

	bool file_exist = ::wxFileExists(dbPath);

	#ifdef _OMB_USE_CIPHER
		sqlite3_open(dbPath.c_str(), &db);
	#else
		db->Open(dbPath, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
	#endif // _OMB_USE_CIPHER

	if(! file_exist){
		#ifdef _OMB_USE_CIPHER
			Sql = L"pragma user_version = " + ::wxString::Format(L"%d", 31) + L";";
			ExecuteUpdate(db, Sql.c_str());
		#else
			db->ExecuteUpdate(L"pragma user_version = " +
																						::wxString::Format(L"%d", 31) +
																						L";");
		#endif // _OMB_USE_CIPHER
	}
	// Set restore savepoint
	//db->Savepoint(L"rollback");

	wxString metadata;
	#if wxCHECK_VERSION(3, 0, 0)
		// Write file metadata
		metadata = L"OS: ";
		#ifdef __WXGTK__
			metadata += wxGetLinuxDistributionInfo().Description;
		#elif defined ( __WXMSW__)
			metadata += wxGetOsDescription();
		#else
			metadata += L"unknown";
		#endif // __WXGTK__
		metadata += L"\n\n";
		#ifndef __OMBCONVERT_BIN__
			metadata += wxGetLibraryVersionInfo().ToString();
		#else
			metadata += _("Converted with ombconvert");
		#endif // __OMBCONVERT_BIN__
	#else
		metadata = L"wxWidgets 2.8.0";
	#endif // wxCHECK_VERSION

	#ifdef _OMB_USE_CIPHER
		if(! TableExists(L"Information", dbPath.c_str(), database)){
			ExecuteUpdate(db, cs_information.c_str()/*, false*/);
			Sql = L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');";
			ExecuteUpdate(db, Sql.c_str()/*, false*/);
		}
		else {
			Sql = L"update Information set data = \"" +
																metadata +
																L"\" where id = " +
																wxString::Format(L"%d", dbMeta_application_info) +
																L";";
			ExecuteUpdate(db, Sql.c_str()/*, false*/);
		}
	#else
		if(! db->TableExists(L"Information")){
			db->ExecuteUpdate(cs_information);
			db->ExecuteUpdate(L"insert into Information values (" +
																wxString::Format(L"%d", dbMeta_application_info) +
																L", '" +
																metadata +
																L"');");
		}
		else db->ExecuteUpdate(L"update Information set data = \"" +
															metadata +
															L"\" where id = " +
															wxString::Format(L"%d", dbMeta_application_info) +
															L";");
	#endif // _OMB_USE_CIPHER

	// Transaction table init
	#ifdef _OMB_USE_CIPHER
		if(! TableExists(L"Transactions", dbPath.c_str(), db)) ExecuteUpdate(db, cs_transactions_v31.c_str()/*, false*/);
	#else
		if(! db->TableExists(L"Transactions")) db->ExecuteUpdate(cs_transactions_v31);
	#endif // _OMB_USE_CIPHER

	// Category table init
	bool master_had_categories =
		#ifdef _OMB_USE_CIPHER
			TableExists(L"Categories", dbPath.c_str(), db);
		#else
			db->TableExists(L"Categories");
		#endif // _OMB_USE_CIPHER

	if(master_had_categories){
		#if(wxCHECK_VERSION(3, 0, 0))
			#ifdef _OMB_USE_CIPHER
				if(! TableExists("Categories" + trailname, dbPath.c_str(), db)){
					Sql = wxString::Format(cs_categories_master, trailname);
					ExecuteUpdate(db, Sql.c_str()/*, false*/);
				}
			#else
				if(! db->TableExists("Categories" + trailname)) db->ExecuteUpdate(wxString::Format(cs_categories_master, trailname));
			#endif // _OMB_USE_CIPHER
		#else
			if(! db->TableExists(L"Categories" + trailname)) db->ExecuteUpdate(wxString::Format(cs_categories_master, trailname.c_str()));
		#endif	// wxCHECK_VERSION
	}
	#ifdef _OMB_USE_CIPHER
		else ExecuteUpdate(db, cs_categories.c_str()/*, false*/);
	#else
		else db->ExecuteUpdate(cs_categories);
	#endif // _OMB_USE_CIPHER

	#if(wxCHECK_VERSION(3, 0, 0))
		#ifdef _OMB_USE_CIPHER
			if(! TableExists("Funds" + trailname, dbPath.c_str(), db)) {
				Sql = wxString::Format(cs_funds_master, trailname);
				ExecuteUpdate(db, Sql.c_str()/*, false*/);
			}
			if(! TableExists("Credits" + trailname, dbPath.c_str(), db)) {
				Sql = wxString::Format(cs_credits_master_v31, trailname);
				ExecuteUpdate(db, Sql.c_str()/*, false*/);
			}
			if(! TableExists("Debts" + trailname, dbPath.c_str(), db)) {
				Sql = wxString::Format(cs_debts_master_v31, trailname);
				ExecuteUpdate(db, Sql.c_str()/*, false*/);
			}
			if(! TableExists("Loans" + trailname, dbPath.c_str(), db)) {
				Sql = wxString::Format(cs_loans_master_v31, trailname);
				ExecuteUpdate(db, Sql.c_str()/*, false*/);
			}
			if(! TableExists("Borrows" + trailname, dbPath.c_str(), db)) {
				Sql = wxString::Format(cs_borrows_master_v31, trailname);
				ExecuteUpdate(db, Sql.c_str()/*, false*/);
			}
		#else
			if(! db->TableExists("Funds" + trailname)) db->ExecuteUpdate(wxString::Format(cs_funds_master, trailname));
			if(! db->TableExists("Credits" + trailname)) db->ExecuteUpdate(wxString::Format(cs_credits_master_v31, trailname));
			if(! db->TableExists("Debts" + trailname)) db->ExecuteUpdate(wxString::Format(cs_debts_master_v31, trailname));
			if(! db->TableExists("Loans" + trailname)) db->ExecuteUpdate(wxString::Format(cs_loans_master_v31, trailname));
			if(! db->TableExists("Borrows" + trailname)) db->ExecuteUpdate(wxString::Format(cs_borrows_master_v31, trailname));
		#endif // _OMB_USE_CIPHER
	#else
		if(! db->TableExists(L"Funds" + trailname)) db->ExecuteUpdate(wxString::Format(cs_funds_master, trailname.c_str()));
		if(! db->TableExists(L"Credits" + trailname)) db->ExecuteUpdate(wxString::Format(cs_credits_master_v31, trailname.c_str()));
		if(! db->TableExists(L"Debts" + trailname)) db->ExecuteUpdate(wxString::Format(cs_debts_master_v31, trailname.c_str()));
		if(! db->TableExists(L"Loans" + trailname)) db->ExecuteUpdate(wxString::Format(cs_loans_master_v31, trailname.c_str()));
		if(! db->TableExists(L"Borrows" + trailname)) db->ExecuteUpdate(wxString::Format(cs_borrows_master_v31, trailname.c_str()));
	#endif	// wxCHECK_VERSION

	#ifdef _OMB_USE_CIPHER
		sqlite3_close(db);
	#else
		db->Close();
	#endif // _OMB_USE_CIPHER

	return master_had_categories;
}

void TData::Archive_inMaster(wxString master, wxString trailname, bool archive_categories){
	// master:		database to attach and archive data in
	// trailname:	suffix for tables
	// archive_categories:	true to archive also categories

	#if(wxCHECK_VERSION(3, 0, 0))
		#ifdef _OMB_USE_CIPHER
			wxString Sql = wxString::Format("ATTACH DATABASE '%s' AS master;", master);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);

			// Archive data in master database
			Sql = "INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM Transactions;";
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
			Sql = wxString::Format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
			Sql = wxString::Format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
			Sql = wxString::Format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
			Sql = wxString::Format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
			Sql = wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname);
			ExecuteUpdate(database, Sql.c_str()/*, false*/);
		#else
			database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));

			// Archive data in master database
			database->ExecuteUpdate("INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM Transactions;");
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname));
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname));
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname));
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname));
			database->ExecuteUpdate(wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname));
		#endif // _OMB_USE_CIPHER
	#else
		database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS master;", master.c_str()));

		// Archive data in master database
		database->ExecuteUpdate("INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM Transactions;");
		database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname.c_str()));
		database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname.c_str()));
		database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname.c_str()));
		database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname.c_str()));
		database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname.c_str()));
	#endif	// wxCHECK_VERSION

	#ifdef _OMB_USE_CIPHER
		if(archive_categories) ExecuteUpdate(database, "INSERT INTO master.Categories SELECT * FROM Categories;");
		ExecuteUpdate(database, "DETACH DATABASE 'master';");
	#else
		if(archive_categories) database->ExecuteUpdate("INSERT INTO master.Categories SELECT * FROM Categories;");
		database->ExecuteUpdate("DETACH DATABASE 'master';");
	#endif // _OMB_USE_CIPHER
}

#ifndef __OMBCONVERT_BIN__
	void TData::Import_inMaster(wxString master, wxString trailname){
		// master:		database to attach and archive data in
		// trailname:	suffix for tables
		// archive_categories:	true to archive also categories

		::wxBeginBusyCursor();

		#if(wxCHECK_VERSION(3, 0, 0))
			#ifdef _OMB_USE_CIPHER
				wxString Sql = wxString::Format("ATTACH DATABASE '%s' AS master;", master);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);

				// Archive data in master database
				Sql = "INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM import.Transactions;";
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(40);
				Sql = wxString::Format("INSERT INTO master.Funds%s SELECT * FROM import.Funds;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(50);
				Sql = wxString::Format("INSERT INTO master.Credits%s SELECT * FROM import.Credits;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(60);
				Sql = wxString::Format("INSERT INTO master.Debts%s SELECT * FROM import.Debts;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(70);
				Sql = wxString::Format("INSERT INTO master.Loans%s SELECT * FROM import.Loans;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(80);
				Sql = wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM import.Borrows;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(90);
				Sql = wxString::Format("INSERT INTO master.Categories%s SELECT * FROM import.Categories;", trailname);
				ExecuteUpdate(database, Sql.c_str()/*, false*/);
				UpdateProgress(100);
				UpdateProgress(0);
			#else
				database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));

				// Archive data in master database
				database->ExecuteUpdate("INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM import.Transactions;");
				UpdateProgress(40);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Funds%s SELECT * FROM import.Funds;", trailname));
				UpdateProgress(50);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Credits%s SELECT * FROM import.Credits;", trailname));
				UpdateProgress(60);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Debts%s SELECT * FROM import.Debts;", trailname));
				UpdateProgress(70);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Loans%s SELECT * FROM import.Loans;", trailname));
				UpdateProgress(80);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Borrows%s SELECT * FROM import.Borrows;", trailname));
				UpdateProgress(90);
				database->ExecuteUpdate(wxString::Format("INSERT INTO master.Categories%s SELECT * FROM import.Categories;", trailname));
				UpdateProgress(100);
				UpdateProgress(0);
			#endif // _OMB_USE_CIPHER
		#else
			database->ExecuteUpdate(wxString::Format(L"ATTACH DATABASE '%s' AS master;", master.c_str()));

			// Archive data in master database
			database->ExecuteUpdate(L"INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM import.Transactions;");
			UpdateProgress(40);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Funds%s SELECT * FROM import.Funds;", trailname.c_str()));
			UpdateProgress(50);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Credits%s SELECT * FROM import.Credits;", trailname.c_str()));
			UpdateProgress(60);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Debts%s SELECT * FROM import.Debts;", trailname.c_str()));
			UpdateProgress(70);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Loans%s SELECT * FROM import.Loans;", trailname.c_str()));
			UpdateProgress(80);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Borrows%s SELECT * FROM import.Borrows;", trailname.c_str()));
			UpdateProgress(90);
			database->ExecuteUpdate(wxString::Format(L"INSERT INTO master.Categories%s SELECT * FROM import.Categories;", trailname.c_str()));
			UpdateProgress(100);
			UpdateProgress(0);
		#endif	// wxCHECK_VERSION

		#ifdef _OMB_USE_CIPHER
			ExecuteUpdate(database, "DETACH DATABASE 'master';");
		#else
			database->ExecuteUpdate("DETACH DATABASE 'master';");
		#endif // _OMB_USE_CIPHER

		::wxEndBusyCursor();
	}
#endif // __OMBCONVERT_BIN__

wxString TData::GenerateTrailName(wxString dbPath){
	wxString return_string = wxEmptyString;

	#ifdef _OMB_USE_CIPHER
		sqlite3 *db;
	#else
		wxSQLite3Database *db = new wxSQLite3Database();
	#endif // _OMB_USE_CIPHER

	if(::wxFileExists(dbPath)){
		#ifdef _OMB_USE_CIPHER
			sqlite3_open(dbPath.c_str(), &db);
			if(TableExists(L"Transactions", L"db", db)){
				wxSQLite3Table table;
				table = GetTable(db, "select * from Transactions order by 1;");
				if(table.GetRowCount()){
					int month, year;
					table.SetRow(0);
					wxDateTime date = wxDateTime((time_t) table.GetInt(2, 0));
					month = date.GetMonth() + 1;
					year = date.GetYear();
					return_string = ::wxString::Format(L"_%d_%02d", year, month);
				}
			}
			sqlite3_close(db);
		#else
			db->Open(dbPath, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
			if(db->TableExists(L"Transactions")){
				wxSQLite3Table table;
				table = db->GetTable("select * from Transactions order by 1;");
				if(table.GetRowCount()){
					int month, year;
					table.SetRow(0);
					wxDateTime date = wxDateTime((time_t) table.GetInt(2, 0));
					month = date.GetMonth() + 1;
					year = date.GetYear();
					return_string = ::wxString::Format(L"_%d_%02d", year, month);
				}
			}
			db->Close();
		#endif // _OMB_USE_CIPHER
	}
	return return_string;
}

#endif // BIL31CORE_CPP_INCLUDED
