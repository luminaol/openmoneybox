/***************************************************************
 * Name:      dataconverter.h
 * Purpose:   convertion Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DATACONVERTER_H
#define DATACONVERTER_H

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#if(! wxCHECK_VERSION(3,0,0))
	#include <wx/datetime.h>
#endif
#include <wx/filename.h>

#include "../omb33core.h"

class dataconverter : public TData_v32
{
	public:
		/** Default constructor */
		dataconverter(void);
		/** Default destructor */
		//virtual ~dataconverter();
		#ifdef _OMB_FORMAT30x
			wxString LoadFromFile_0301(wxString AName, bool App);
			bool AddOper_0301(wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, wxString N);
			wxString LoadFromFile_0302(/*bool Auto, */wxString AName, /*wxString F, */bool App);
		#endif // _OMB_FORMAT30x
		void BuildDatabase(int num_funds, int num_credits, int num_debits, int num_loans, int num_borrows, int num_transactions,
																	 int num_categories, int num_shoplists, wxString defaultFund);

		bool Upgrade_0301to0302(void);
		bool UpgradeMaster_0301to0302(void);

		#ifdef _OMB_OLDFORMATSAVE_FEATURE
			bool SaveToFile_0301(bool Auto,bool Archive, wxString AFile,wxString F);
			bool SaveToFile_0302(bool Auto, bool Archive, wxString AFile);

			bool SaveToFile_031x();	// TODO (igor#1#): Write code
		#endif // _OMB_OLDFORMATSAVE_FEATURE

		bool Upgrade_030301to030302(void);
		bool UpgradeMaster_030301to030302(void);
	protected:

	private:
		#ifdef _OMB_FORMAT30x
			wxDateTime Omb_StrToTime(wxString S);
			#if (wxCHECK_VERSION(3, 0, 0) )
				bool SubstLocaleDecimalSeparator(wxString &S,char ToSub);
			#endif
			//wxString DoubleQuote(wxString S);
			wxString Crip(wxString S);
		#endif // _OMB_FORMAT30x
};

/*
// Calls to module igilogo
WXIMPORT wxString iUpperCase(wxString S);
*/
extern wxString iUpperCase(wxString S);
extern wxString DoubleQuote(wxString S);
// Calls to module igiomb
extern wxDateTime Omb_StrToDate(wxString S);
#endif // DATACONVERTER_H

