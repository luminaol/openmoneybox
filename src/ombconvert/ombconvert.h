/***************************************************************
 * Name:      ombconvert.h
 * Purpose:   old format conversion tool for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2017-09-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBCONVERTAPP_H
#define OMBCONVERTAPP_H

#include <wx/app.h>

#include <wx/filename.h>

#ifdef __WXMSW__
  class ombconvert : public wxApp
#else
  #if(wxCHECK_VERSION(3,0,0))
	  class ombconvert : public wxAppConsole
	#else
	  class ombconvert : public wxApp
	#endif
#endif // __WXMSW__
{
    public:
        virtual bool OnInit();
};

WXIMPORT wxString GetShareDir(void);

#endif // OMBCONVERTAPP_H
