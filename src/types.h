/***************************************************************
 * Name:      types.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2013-10-21
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef TVersionH
#define TVersionH
/*
#ifndef __WXWIN__
		typedef wxUint32 LANGID;
#endif
*/
typedef struct TVersion{
	long Maj;
	long Min;
	long Rel;
	long Bui;
}Version;
#endif

