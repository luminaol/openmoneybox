///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __WXADVSHEET_H__
#define __WXADVSHEET_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/checkbox.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/panel.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class AdvSheet
///////////////////////////////////////////////////////////////////////////////
class AdvSheet : public wxPanel 
{
	private:
	
	protected:
	
	public:
		wxCheckBox* cb_CheckUpdates;
		
		AdvSheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,300 ), long style = wxTAB_TRAVERSAL ); 
		~AdvSheet();
	
};

#endif //__WXADVSHEET_H__
