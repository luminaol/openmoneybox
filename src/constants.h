/***************************************************************
 * Name:      constants.h
 * Purpose:   Header with constants for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-18
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/datetime.h>

#ifndef OMB_CONSTANTS
	#define OMB_CONSTANTS

	// file format identifiers
	enum ombFileFormat{
		ombFFORMAT_UNKNOWN = 0,
		#ifdef _OMB_FORMAT2x
			bilFFORMAT_2		= 2,	// bilancio 2.0 format
		#endif // _OMB_FORMAT2x
		#ifdef _OMB_FORMAT30x
			bilFFORMAT_301	= 3,	// bilancio 3.0.1 format
			bilFFORMAT_302	= 4,	// bilancio 3.0.2 format
		#endif // _OMB_FORMAT30x
		//bilFFORMAT_31		= 5,	// bilancio 3.1 format
		ombFFORMAT_31		= 5,	// openmoneybox 3.1 format
		ombFFORMAT_32		= 6,	// openmoneybox 3.2 format
		#ifdef _OMB_USE_CIPHER
			ombFFORMAT_33 = 7,	// openmoneybox 3.3 format (encryption support)
			ombWRONGPASSWORD = 8,	// wrong password
		#endif // _OMB_USE_CIPHER
		ombFFORMAT_332 = 9,	// openmoneybox 3.3.2 format
};

	// date constants
	const wxDateTime invalidDate = wxDateTime(double(2415020));

	const double ombInvalidLatitude = 91;
	const double ombInvalidLongitude = 181;

#endif // OMB_CONSTANTS

