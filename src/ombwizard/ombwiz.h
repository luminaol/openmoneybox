/***************************************************************
 * Name:      bilwiz.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2016-10-09
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/wx.h>

#ifdef __WXMSW__
	#include "../platformsetup.h"
#endif // __WXMSW__

#ifndef BilWizH
#define BilWizH

WXEXPORT int RunWizard(wxGauge* Prog,wxString &S, bool def);

#endif	// BilWizH
