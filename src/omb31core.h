/***************************************************************
 * Name:      omb31core.h
 * Purpose:   Core Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-02-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB31CORE_HEADER_INCLUDED
#define OMB31CORE_HEADER_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>
#ifndef __OMBCONVERT_BIN__
	#include <wx/gauge.h>
#endif

#ifdef _OMB_USE_CIPHER
	#include <sqlite3.h>
#else
	#include <wx/wxsqlite3.h>
#endif // _OMB_USE_CIPHER

#if(! wxCHECK_VERSION(3,0,0))
	#include <wx/datetime.h>
#endif

#include "types.h"

#ifndef Omb31CoreH
#define Omb31CoreH

// Product Capabilities
const int MAX_FUNDS     	=   100;
const int MAX_CREDITS   	=   100;
const int MAX_DEBTS     	=   100;
const int MAX_LENT      	=  1000;
const int MAX_BORROWED  	=  1000;
const int MAX_SHOPS     	=  1000;
const int MAX_LINES     	= 10000;
const int MAX_CATEGORIES	=   100;
//______________________________

typedef enum TTypeVal{tvFou=1,tvCre,tvDeb}TypeVal;
typedef struct TVal{
	int Id;
	wxString Name;
	double Value;
}Val;

typedef enum TObjType{toPre=1,toInP}ObjType;
typedef struct TObj{
	int Id;
	wxString Name, Object;
	wxDateTime Alarm;
}Obj;

typedef struct TShopItem{
	int Id;
	wxString Name;
	wxDateTime Alarm;}ShopItem;

typedef enum TOpType{
  toNULL=0,
  toGain=1,toExpe,
  toSetCre,toRemCre,toConCre,
  toSetDeb,toRemDeb,toConDeb,
  toGetObj,toGivObj,
  toLenObj,toBakObj,
  toBorObj,toRetObj}OpType;

typedef struct TLine{
	int Id;
	bool IsDate;
	wxDateTime Date,Time;
	TOpType Type;
	wxString Value,
	// FIXME (igor#1#):	Known limitation: Value is defined as WxString also in case it contains numeric data. ...
	//									So it is saved as a locale-dependent string.
	Reason;
	long CategoryIndex;
	#ifdef __OMBCONVERT_BIN__
		wxString Remark0301;	// "Bil3.0"
	#endif // __OMBCONVERT_BIN__
	}Line;

// Database information
enum dbMetaDataEntry{
	dbMeta_application_info = 1,
	dbMeta_default_fund,
	dbMeta_mobile_export,
	dbMeta_currency
};

typedef struct TCategory{
	int Id;
	wxString Name;
	bool test;	// used during category update
	}Category;

class TData
{
friend class ombMainFrame;
friend class TData_v32;

private:
	// Non-GUI components

	double Tot_Credits,Tot_Debts;

	// Routines
	//bool SortValues(TTypeVal T);
	//void CheckMemory(void); // NOTE (igor#1#): Do not remove, may be restored in future
	void ParseDatabase(void);
	//wxString BackupName(void);
	bool Prepare_MasterDB(wxString dbPath, wxString trailname);
	void Archive_inMaster(wxString master, wxString trailname, bool archive_categories);
protected:
	// Non-GUI components
	double Tot_Funds;

	// Routines
	#ifndef __OMBCONVERT_BIN__
		void UpdateProgress(int i);
	#endif // __OMBCONVERT_BIN__

	#if(wxCHECK_VERSION(3, 0, 0))
		wxString ombFromCDouble(double value);
	#endif
	//void Sort(void);
public:
	// Non-GUI components
	//bool NotEnoughMemory;

	#ifdef _OMB_USE_CIPHER
		sqlite3 *database;
	#else
		wxSQLite3Database *database;
	#endif // _OMB_USE_CIPHER

	bool Parsing; // Set to true during file parsing
	wxDateTime Day;
	wxDateTime Hour;
	int NFun, NCre, NDeb, NLen, NBor, NSho, NLin, NCat;
	struct ACC{
		bool	Modified,
					ReadOnly;
		int Year;
		wxDateTime DateStamp;
		long Month;
		wxString	FileName,
							FileView,
							DefFund;}FileData;
	TVal *Funds,*Credits,*Debts;
	TObj *Lent,*Borrowed;
	TShopItem *ShopItems;
	TLine *Lines;
	TCategory *Categories;
	wxArrayString *MattersBuffer;
	#ifdef __OMBCONVERT_BIN__
		wxArrayString *CategoryDB;
		TData(void);
	#else
		wxGauge *Progress;
		explicit TData(wxGauge *ProgressBar);
	#endif // __OMBCONVERT_BIN__

	// Routines
	~TData(void);
	void Initialize(bool Creating);
	bool AddDate(int id, wxDateTime D, double T);
	bool AddValue(TTypeVal T, int id, wxString N, double V);
	bool DelValue(TTypeVal T,int I);
	bool AddObject(TObjType T, int id, wxString N, wxString O, wxDateTime D);
	bool DelObject(TObjType T,int I);
	bool AddShopItem(int id, wxString N, wxDateTime A);
	bool DelShopItem(int I);
	bool AddOper(int id, wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, long N);
	#ifndef __OMBCONVERT_BIN__
		//void XMLExport(wxString F1, wxString F2);
		//void XMLExport_archive(wxString F1, wxString F2, wxDateTime date);
		void UpdateCategories(wxListBox *lbox);
	#endif // __OMBCONVERT_BIN__
	double GetTot(TTypeVal T);
	//bool SetTot(TTypeVal T,double Tot);
	bool IsDate(int R);
	//bool UpdateMatters(TOpType T);
	bool OpenDatabase(wxString File);
	void SetDefaultFundValue(double value);
	void SetDefaultFund(wxString def);
	bool ChangeFundValue(TTypeVal type, int id, /*wxString N,*/ double V);
	void AddCategory(int id, wxString name);
	//void RestoreBackup(void);
	wxString GenerateTrailName(wxString dbPath);
	void Import_inMaster(wxString master, wxString trailname);
};

#ifndef __OMBCONVERT_BIN__
	WXIMPORT wxString FormDigits(double Val);
	WXIMPORT wxDateTime Omb_StrToDate(wxString S);
	WXIMPORT wxString IntToMonth(int M);
	WXIMPORT void Error(int Err,wxString Opt);
	WXIMPORT bool AutoConvert(void);
	WXIMPORT wxString GetLogString(int I);
	WXIMPORT wxString SubstSpecialChars(wxString S);
	#ifdef _OMB_USE_CIPHER
		WXIMPORT int CheckFileFormat(wxString File, wxString pwd = wxEmptyString, bool archive = false);
	#else
		WXIMPORT int CheckFileFormat(wxString File);
	#endif // _OMB_USE_CIPHER
	WXIMPORT wxString GetBilDocPath(void);
	WXIMPORT wxString GetDataDir(void);
	WXIMPORT wxString GetOSDocDir(void);
	#ifdef _OMB_USE_CIPHER
		WXIMPORT bool CheckAndPromptForConversion(wxString File, bool master, wxString pwd = wxEmptyString);
	#else
		WXIMPORT bool CheckAndPromptForConversion(wxString File, bool master);
	#endif // _OMB_USE_CIPHER
	#ifdef __WXMSW__
		WXIMPORT wxLanguage FindLang(void);
		WXIMPORT wxString GetInstallationPath(void);
	#endif //__WXMSW__

	WXIMPORT wxString Get_MasterDB(void);

	#if (! wxCHECK_VERSION(3, 0, 0) )
		WXIMPORT bool SubstLocaleDecimalSeparator(wxString &S, char ToSub);
	#endif

	WXIMPORT wxString DoubleQuote(wxString S);

	WXIMPORT wxString GetDocPrefix(void);

#else
	extern wxString Crip(wxString S);
	extern wxString FormDigits(double Val);
	extern wxDateTime Omb_StrToDate(wxString S);
	extern void Error(int Err,wxString Opt);
	#ifdef _OMB_USE_CIPHER
		extern int CheckFileFormat(wxString File, wxString pwd = wxEmptyString, bool archive = false);
	#else
		extern int CheckFileFormat(wxString File);
	#endif // _OMB_USE_CIPHER

	#if (! wxCHECK_VERSION(3, 0, 0) )
		extern bool SubstLocaleDecimalSeparator(wxString &S, char ToSub);
	#endif

	extern wxString DoubleQuote(wxString S);
#endif // __OMBCONVERT_BIN__

WXIMPORT wxString GetCurrencySymbol(void);

#endif // Omb31CoreH

#endif  // OMB31CORE_HEADER_INCLUDED
