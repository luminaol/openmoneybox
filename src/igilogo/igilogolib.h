/***************************************************************
 * Name:      igilogolib.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>

	// String manipulation functions
	WXEXPORT wxString iUpperCase(wxString S);
	//	Dialogs functions
	WXEXPORT void Logo(wxString Product);
	/*
	wxDialog* LogoWithInfo(wxString Product);
	// NOTE (igor#1#): experimental
	*/
	#if __HAS_NATIVE_ABOUT == 0
		WXEXPORT void About(wxString Name,wxString ShortVer,wxString LongVer,wxString Date);
	#endif // __HAS_NATIVE_ABOUT == 0
	WXEXPORT void DonateDialog(wxString olURL/*,bool EN*/);
#endif // __OMBCONVERT_BIN__

