/***************************************************************
 * Name:      splashf.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef SplashFH
#define SplashFH

#include "wxsplash.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TSplash : public wxSplash
{
private:
	// Routines
	void OnTimer(wxTimerEvent& event);
public:
	// Routines
	explicit TSplash(wxWindow *parent);
};

#endif

