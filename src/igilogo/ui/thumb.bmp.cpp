#include "thumb.bmp.h"

wxBitmap& thumb_bmp_to_wx_bitmap()
{
	static wxMemoryInputStream memIStream( thumb_bmp, sizeof( thumb_bmp ) );
	static wxImage image( memIStream, wxBITMAP_TYPE_BMP );
	static wxBitmap bmp( image );
	return bmp;
};

