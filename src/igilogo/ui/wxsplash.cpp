///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  5 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxsplash.h"

#include "thumb.bmp.h"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxSplash, wxFrame )
	EVT_TIMER( wxID_ANY, wxSplash::_wxFB_OnTimer )
END_EVENT_TABLE()

wxSplash::wxSplash( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	this->SetBackgroundColour( wxColour( 0, 0, 0 ) );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 3, 1, 0, 0 );
	fgSizer1->AddGrowableCol( 0 );
	fgSizer1->AddGrowableRow( 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer2->AddGrowableCol( 1 );
	fgSizer2->AddGrowableRow( 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	Image1 = new wxStaticBitmap( this, wxID_ANY, thumb_bmp_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, 0 );
	Image1->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer2->Add( Image1, 1, wxALL, 5 );
	
	Vendor_lbl = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT|wxST_NO_AUTORESIZE );
	Vendor_lbl->Wrap( -1 );
	Vendor_lbl->SetFont( wxFont( 30, 74, 90, 90, false, wxT("Ubuntu") ) );
	Vendor_lbl->SetForegroundColour( wxColour( 255, 128, 255 ) );
	
	fgSizer2->Add( Vendor_lbl, 0, wxALL|wxALIGN_RIGHT|wxEXPAND, 5 );
	
	
	fgSizer1->Add( fgSizer2, 1, wxEXPAND, 5 );
	
	Info = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Info->Wrap( -1 );
	Info->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	Info->SetForegroundColour( wxColour( 255, 255, 255 ) );
	Info->Hide();
	
	fgSizer1->Add( Info, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	Label1 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT|wxST_NO_AUTORESIZE );
	Label1->Wrap( -1 );
	Label1->SetFont( wxFont( 20, 74, 90, 90, false, wxT("Ubuntu") ) );
	Label1->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	fgSizer1->Add( Label1, 0, wxALL|wxALIGN_RIGHT|wxEXPAND, 5 );
	
	
	this->SetSizer( fgSizer1 );
	this->Layout();
	Timer.SetOwner( this, wxID_ANY );
	Timer.Start( 5000, true );
	
	
	this->Centre( wxBOTH );
}

wxSplash::~wxSplash()
{
}
