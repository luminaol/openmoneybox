/***************************************************************
 * Name:      splashf.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef SPLASH_H_INCLUDED
#define SPLASH_H_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxsplash.h"
#include "splashf.h"
#include "../ProductVersions.h"

TSplash::TSplash(wxWindow *parent):wxSplash(parent,wxID_ANY,Vendor,wxDefaultPosition,wxSize(320,194),wxSTAY_ON_TOP|wxFRAME_NO_TASKBAR){
	Vendor_lbl->SetLabel(Vendor);}

void TSplash::OnTimer(wxTimerEvent& event){
	Close();
	Destroy();}


#endif // SPLASH_H_INCLUDED
