///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  5 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __WXSPLASH_H__
#define __WXSPLASH_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/timer.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxSplash
///////////////////////////////////////////////////////////////////////////////
class wxSplash : public wxFrame 
{
	DECLARE_EVENT_TABLE()
	private:
		
		// Private event handlers
		void _wxFB_OnTimer( wxTimerEvent& event ){ OnTimer( event ); }
		
	
	protected:
		wxStaticBitmap* Image1;
		wxStaticText* Vendor_lbl;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnTimer( wxTimerEvent& event ) { event.Skip(); }
		
	
	public:
		wxStaticText* Info;
		wxStaticText* Label1;
		wxTimer Timer;
		
		wxSplash( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 320,194 ), long style = wxFRAME_NO_TASKBAR|wxSTAY_ON_TOP|wxTAB_TRAVERSAL );
		
		~wxSplash();
	
};

#endif //__WXSPLASH_H__
