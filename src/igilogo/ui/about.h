/***************************************************************
 * Name:      about.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef AboutH
#define AboutH

#include "wxabout.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TAboutBox : public wxAboutBox
{
//__published:
private:
	void FormDestroy(void);
	//void FormShow(void);
public:
	explicit TAboutBox(wxWindow *parent);
};

//extern PACKAGE TAboutBox *AboutBox;
#endif


