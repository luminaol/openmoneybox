/***************************************************************
 * Name:      mac_build.h
 * Purpose:   macOs build information for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-04-26
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

const char *__BUILD_NUMBER = {"0"} ;
const char *__BUILD_DATE = {"20190426"};
