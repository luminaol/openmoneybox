///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Feb 16 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __WXEDITCATEGORIES_H__
#define __WXEDITCATEGORIES_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/listbox.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxFEditCategories
///////////////////////////////////////////////////////////////////////////////
class wxFEditCategories : public wxDialog 
{
	DECLARE_EVENT_TABLE()
	private:
		
		// Private event handlers
		void _wxFB_FormShow( wxActivateEvent& event ){ FormShow( event ); }
		void _wxFB_OnListBox( wxCommandEvent& event ){ OnListBox( event ); }
		void _wxFB_AddButtonClick( wxCommandEvent& event ){ AddButtonClick( event ); }
		void _wxFB_RemoveButtonClick( wxCommandEvent& event ){ RemoveButtonClick( event ); }
		void _wxFB_OKBtnClick( wxCommandEvent& event ){ OKBtnClick( event ); }
		
	
	protected:
		enum
		{
			bilAdd = 1000,
			bilRemove
		};
		
		wxTextCtrl* EditBox;
		wxButton* m_button3;
		wxButton* Remove;
		wxButton* OKBtn;
		wxButton* CancelBtn;
		
		// Virtual event handlers, overide them in your derived class
		virtual void FormShow( wxActivateEvent& event ) { event.Skip(); }
		virtual void OnListBox( wxCommandEvent& event ) { event.Skip(); }
		virtual void AddButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxListBox* CategoryList;
		
		wxFEditCategories( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit the categories"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 440,235 ), long style = wxCAPTION|wxDEFAULT_DIALOG_STYLE ); 
		~wxFEditCategories();
	
};

#endif //__WXEDITCATEGORIES_H__
