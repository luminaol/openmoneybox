/***************************************************************
 * Name:      gainexpense.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-06
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/dialog.h>

#ifndef GainExpenseH
#define GainExpenseH

#include "wxgainexpense.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TOperationF : public wxTOperationF
{
private:
	bool CheckValues;	// true if formal checks must be made on OK button press
	// Routines
	void OKBtnClick(wxCommandEvent& event);
	void FundNameKeyPress(wxKeyEvent& event);
	void Focus(int F);
	void CurrencyClick(wxCommandEvent& event);
public:
	explicit TOperationF(wxWindow *parent);
	void InitLabels(int kind);
};
#endif

WXIMPORT wxString iUpperCase(wxString S);
WXIMPORT wxString CheckValue(wxString Val);
WXIMPORT void Error(int Err,wxString Opt);
WXIMPORT wxString GetCurrencySymbol(void);
