///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Feb 16 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxeditcategories.h"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxFEditCategories, wxDialog )
	EVT_ACTIVATE( wxFEditCategories::_wxFB_FormShow )
	EVT_LISTBOX( wxID_ANY, wxFEditCategories::_wxFB_OnListBox )
	EVT_BUTTON( bilAdd, wxFEditCategories::_wxFB_AddButtonClick )
	EVT_BUTTON( bilRemove, wxFEditCategories::_wxFB_RemoveButtonClick )
	EVT_BUTTON( wxID_OK, wxFEditCategories::_wxFB_OKBtnClick )
END_EVENT_TABLE()

wxFEditCategories::wxFEditCategories( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ) );
	
	wxFlexGridSizer* fgSizer7;
	fgSizer7 = new wxFlexGridSizer( 3, 1, 0, 0 );
	fgSizer7->AddGrowableCol( 0 );
	fgSizer7->AddGrowableRow( 1 );
	fgSizer7->SetFlexibleDirection( wxBOTH );
	fgSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	CategoryList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxSize( -1,115 ), 0, NULL, 0 ); 
	fgSizer7->Add( CategoryList, 1, wxEXPAND|wxALL, 5 );
	
	wxFlexGridSizer* fgSizer5;
	fgSizer5 = new wxFlexGridSizer( 0, 3, 0, 0 );
	fgSizer5->AddGrowableCol( 0 );
	fgSizer5->SetFlexibleDirection( wxBOTH );
	fgSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	EditBox = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer5->Add( EditBox, 1, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	m_button3 = new wxButton( this, bilAdd, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer5->Add( m_button3, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	Remove = new wxButton( this, bilRemove, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	Remove->Enable( false );
	
	fgSizer5->Add( Remove, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );
	
	
	fgSizer7->Add( fgSizer5, 1, wxEXPAND, 5 );
	
	wxFlexGridSizer* fgSizer8;
	fgSizer8 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer8->AddGrowableCol( 0 );
	fgSizer8->AddGrowableRow( 0 );
	fgSizer8->SetFlexibleDirection( wxBOTH );
	fgSizer8->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );
	OKBtn->SetDefault(); 
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer8->Add( OKBtn, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );
	
	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 74, 90, 90, false, wxT("Ubuntu") ) );
	
	fgSizer8->Add( CancelBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	
	fgSizer7->Add( fgSizer8, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( fgSizer7 );
	this->Layout();
}

wxFEditCategories::~wxFEditCategories()
{
}
