/***************************************************************
 * Name:      setcreddeb.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef SetCredDebH
#define SetCredDebH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxsetcreddeb.h"

class TSCredDeb : public wxTSCredDeb
{
private:
	// Routines
	void Focus(bool Err);
protected:
	void OKBtnClick(wxCommandEvent& event);
public:
	// Routines
	explicit TSCredDeb(wxWindow *parent);
	void InitLabels(bool E);
};

WXIMPORT wxString iUpperCase(wxString S);
WXIMPORT wxString CheckValue(wxString Val);
WXIMPORT void Error(int Err, wxString Opt);
#endif // SetCredDebH

