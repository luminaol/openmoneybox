/***************************************************************
 * Name:      editcategories.cpp
 * Purpose:   Category Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include "editcategories.h"

wxEditCategories::wxEditCategories( wxWindow* parent )
:
wxFEditCategories( parent )
{
	modified = false;
}

void wxEditCategories::OKBtnClick( wxCommandEvent& event ){
	EndModal(wxID_OK);}

void wxEditCategories::OnListBox(wxCommandEvent& event){
	Remove->Enable(CategoryList->GetSelection() > -1);}

void wxEditCategories::AddButtonClick(wxCommandEvent& event){
	wxString value = iUpperCase(EditBox->GetValue());
	if(value != wxEmptyString){
		bool Ex=false;
		for(unsigned int i = 0; i < CategoryList->GetCount(); i++){
			if(CategoryList->GetString(i) == value){
				Ex=true;
				break;}}
		if(!Ex){
			CategoryList->Append(value);
			modified = true;}}}

void wxEditCategories::RemoveButtonClick(wxCommandEvent& event){
	int OldIndex = CategoryList->GetSelection();
	CategoryList->Delete(CategoryList->GetSelection());
	if(((int)CategoryList->GetCount()) >= (OldIndex + 1))CategoryList->SetSelection(OldIndex);
	else if(CategoryList->GetCount())CategoryList->SetSelection(CategoryList->GetCount() - 1);
	wxCommandEvent evt(wxEVT_NULL, 0);
	OnListBox(evt);
	modified = true;}

