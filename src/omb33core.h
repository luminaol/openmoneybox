/***************************************************************
 * Name:      omb33core.h
 * Purpose:   Core Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB33CORE_HEADER_INCLUDED
#define OMB33CORE_HEADER_INCLUDED

#ifndef __OMBCONVERT_BIN__
	#include <wx/gauge.h>
#endif

#include "constants.h"
#include "omb31core.h"

typedef struct TVal_v32 : public TVal{
	long ContactIndex;

	/* TODO -cImprovement : To be implemented
	TDate CreationDate;
	TDate DeletionDate;
	*/
}Val_v32;

typedef struct TObj_v32 : public TObj{
	long ContactIndex;

	/* TODO -cImprovement : To be implemented
	wxDateTime CreationDate;
	wxDateTime DeletionDate;
	*/
}Obj_v32;

typedef struct TLine_v32 : public TLine{
	long ContactIndex;
	double Latitude;
	double Longitude;

	long CurrencyIndex;
	double CurrencyRate;
	wxString CurrencySymbol;
}Line_v32;

// Column identifiers in the database, 'Transactions' table
enum dbTransactionColId{
	ombTColIdId = 0,
	ombTColIdIsDate,
	ombTColIdDate,
	ombTColIdTime,
	ombTColIdType,	// = 4,
	ombTColIdValue,
	ombTColIdReason,
	ombTColIdCategoryIndex,
	ombTColIdContactIndex, //= 8,
	ombTColIdLatitude,
	ombTColIdLongitude,

	ombTColIdCurrencyId,
	ombTColIdCurrencyRate,
	ombTColIdCurrencySymbol,
};

class TData_v32 : public TData
{
	friend class ombMainFrame;
	private:
		#ifdef _OMB_USE_CIPHER
			bool	IsEncrypted,
						IsEncrypted_master;
			bool PasswordPrompt(sqlite3 *db, bool Archive);
		#endif // _OMB_USE_CIPHER
		void Initialize(bool Creating);
		bool AddDate(int id, wxDateTime D, double T);
		// void CleanMaster(void);
		bool Prepare_MasterDB(wxString dbPath, wxString trailname);
		void Archive_inMaster(wxString master, wxString trailname, bool archive_categories);
	public:
		TVal_v32 /**Funds,*/ *Credits, *Debts;
		TObj_v32 *Lent, *Borrowed;
		TLine_v32 *Lines;

		#ifdef __OMBCONVERT_BIN__
			TData_v32(void);
		#else
			explicit TData_v32(wxGauge *ProgressBar);
		#endif // __OMBCONVERT_BIN__

		bool UpdateMatters(TOpType T);

		#if defined (__OMBCONVERT_BIN__) && defined (_OMB_USE_CIPHER)
			bool OpenDatabase(wxString File, wxString pwd);
		#else
			bool OpenDatabase(wxString File);
		#endif // defined

		void ParseDatabase(void);
		bool IsDate(int R);
		bool AddValue(TTypeVal T, int id, wxString N, double V, long c_index = -1);
		bool AddOper(int id, wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, long N, long c_index,
									bool hasLocation = false, double lat = ombInvalidLatitude, double lon = ombInvalidLongitude, long Curr_id = -1, double Curr_rate = 1, wxString Curr_Symb = wxEmptyString);
		bool AddObject(TObjType T, int id, wxString N, wxString O, wxDateTime D, long c_index);
		bool ChangeFundValue(TTypeVal type, int id, double V);
		bool FindContact(long contact_id);
		#ifndef __OMBCONVERT_BIN__
			void ModPass(void);
			void XMLExport(wxString F1, wxString F2);
			void XMLExport_archive(wxString F1, wxString F2, wxDateTime date);
			bool AttachMaster(void);
		#endif // __OMBCONVERT_BIN__
		void ChangeTransactionCategory(int id, int new_index);
};

/*
#ifndef __OMBCONVERT_BIN__
	WXIMPORT bool CheckAndPromptForConversion(wxString File, bool master);
#endif // __OMBCONVERT_BIN__
*/

#ifdef _OMB_USE_CIPHER
	WXIMPORT bool SetKey(wxString Key, bool Archive = false);
	WXIMPORT wxString GetKey(bool Archive = false);
	WXIMPORT bool IsEncryptedDB(wxString File, wxString Pwd, wxString OldPwd = wxEmptyString, bool CheckOnly = false);
#endif // _OMB_USE_CIPHER

#ifdef __WXMAC__
	WXIMPORT wxLanguage FindLang(void);
#endif // __WXMAC__

#endif	// OMB33CORE_HEADER_INCLUDED
