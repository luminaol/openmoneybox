/***************************************************************
 * Name:      datawithalarms.h
 * Purpose:   Alarm extension for TData
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2017-09-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DATAWITHALARMS_H
#define DATAWITHALARMS_H

#include "../omb33core.h"

class TDataWithAlarms : public TData_v32
{
	private:
		wxString SoundFile;
	public:
		TDataWithAlarms(void);
		bool HasAlarms(void);
		void CheckShopList(void);
		void CheckLentObjects(void);
		void CheckBorrowedObjects(void);
};

WXIMPORT wxString GetDataDir(void);

#endif // DATAWITHALARMS_H
