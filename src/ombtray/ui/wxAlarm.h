///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __WXALARM_H__
#define __WXALARM_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxAlarmF
///////////////////////////////////////////////////////////////////////////////
class wxAlarmF : public wxDialog 
{
	private:
	
	protected:
		wxButton* CancelBtn;
		wxButton* PostponeBtn;
		
		// Virtual event handlers, overide them in your derived class
		virtual void PostponeBtnClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxStaticText* Text;
		wxDatePickerCtrl* DatePicker;
		wxCheckBox* cb_PostponeAll;
		
		wxAlarmF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Reminder"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE ); 
		~wxAlarmF();
	
};

#endif //__WXALARM_H__
