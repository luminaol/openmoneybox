/***************************************************************
 * Name:      Alarm.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef AlarmH
#define AlarmH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>

#include "wxAlarm.h"

class TAlarmF : public wxAlarmF
{
private:
	void PostponeBtnClick(wxCommandEvent& event);
public:
	explicit TAlarmF(wxWindow* parent);
};

WXIMPORT void Error(int Err, wxString Opt);

#endif	// AlarmH

