/***************************************************************
 * Name:      Alarm.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2014-10-22
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "Alarm.h"
#include "../../../rsrc/icons/Icon_Application.xpm"


TAlarmF::TAlarmF(wxWindow* parent):
wxAlarmF(parent,wxID_ANY,_("Reminder"),wxDefaultPosition,wxSize( 352, 183 ),wxDEFAULT_FRAME_STYLE ){
	SetTitle(_("Reminder"));
	SetIcon(wxIcon(Logo_xpm));}

void TAlarmF::PostponeBtnClick(wxCommandEvent& event){
	SetReturnCode(wxID_NONE);
	wxDateTime date = DatePicker->GetValue();
	if(! date.IsValid()){
		Error(46, wxEmptyString);
		date = wxDateTime::Today();
		DatePicker->SetValue(date);
		return;
	}
	EndModal(wxID_OK);}

