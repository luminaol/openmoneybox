/***************************************************************
 * Name:      password.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-02-03
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>

//#include "wxpassword.h"
#include "password.h"
#include "../types.h"

//TPassF *PassF;

TPassF::TPassF(wxWindow *parent):wxTPassF(parent, -1, wxEmptyString, wxDefaultPosition, wxSize(355, 215), wxDEFAULT_DIALOG_STYLE){
	SetTitle(_("Access control"));
	Pass->SetFocus();}

void TPassF::OKClick(wxCommandEvent& event){
	SetReturnCode(wxID_NONE);
	if(Pass->GetValue().IsEmpty()){
		Error(40,wxEmptyString);
		Pass->SetFocus();
		//SetReturnCode(wxID_NONE);
		return;}
	if(Pass2->IsShown()&&(Pass->GetValue()!=Pass2->GetValue())){
		SetReturnCode(wxID_RETRY);
		Pass->SetFocus();
		return;}
	EndModal(wxID_OK);}

void TPassF::ResizeF(void){
	int x,y;
	GetClientSize(&x,&y);
	SetClientSize(x,y-60);}
