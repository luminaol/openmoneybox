/***************************************************************
 * Name:      password.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-08-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/fileconf.h>

#ifndef PasswordH
#define PasswordH

#include "wxpassword.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TPassF : public wxTPassF
{
protected:
	void OKClick(wxCommandEvent& event);
public:
	// Routines
	explicit TPassF(wxWindow *parent);
	void ResizeF(void);
};

WXIMPORT void Error(int Err,wxString Opt);

#endif

