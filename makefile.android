# (C) Igor Calì 19/05/2019 (igor.cali0@gmail.com)

#Product identification
# DEVMARKER COULD BE EITHER b (beta) or rc (release candidate)
MAJVER = 3.2
DEVMARKER = 
VERSION = $(MAJVER)$(DEVMARKER).1
APPNAME = openmoneybox

# Android
ANDROID_FOLDER ?= android/
#override ANDROID_API_LEVEL for other Android versions
ANDROID_API_LEVEL ?= API26

# Name of text file containing build number.
BUILD_NUMBER_FILE=build-number.txt

# Manual docbook templates
MANUAL_POTS = $(ANDROID_FOLDER)help/en/AddShopItem.pot \
	$(ANDROID_FOLDER)help/en/Expense_operations.pot \
	$(ANDROID_FOLDER)help/en/Alarms.pot \
	$(ANDROID_FOLDER)help/en/Export_file.pot \
	$(ANDROID_FOLDER)help/en/Options.pot \
	$(ANDROID_FOLDER)help/en/Author.pot \
	$(ANDROID_FOLDER)help/en/Password.pot \
	$(ANDROID_FOLDER)help/en/Gain_operations.pot \
	$(ANDROID_FOLDER)help/en/Borrow.pot \
	$(ANDROID_FOLDER)help/en/GetBack.pot \
	$(ANDROID_FOLDER)help/en/Remove_fund.pot \
	$(ANDROID_FOLDER)help/en/GiveBack.pot \
	$(ANDROID_FOLDER)help/en/Remove_item.pot \
	$(ANDROID_FOLDER)help/en/Credit_condone.pot \
	$(ANDROID_FOLDER)help/en/Global_datetime.pot \
	$(ANDROID_FOLDER)help/en/Credit_remove.pot \
	$(ANDROID_FOLDER)help/en/History.pot \
	$(ANDROID_FOLDER)help/en/Credit_set.pot \
	$(ANDROID_FOLDER)help/en/Introduction.pot \
	$(ANDROID_FOLDER)help/en/Debt_condone.pot \
	$(ANDROID_FOLDER)help/en/Lend.pot \
	$(ANDROID_FOLDER)help/en/Debt_remove.pot \
	$(ANDROID_FOLDER)help/en/Total_fund.pot \
	$(ANDROID_FOLDER)help/en/Debt_set.pot \
	$(ANDROID_FOLDER)help/en/Default_fund.pot \
	$(ANDROID_FOLDER)help/en/New_fund.pot \
	$(ANDROID_FOLDER)help/en/Wizard.pot \
	$(ANDROID_FOLDER)help/en/Edit_fund.pot \
	$(ANDROID_FOLDER)help/en/Object_Get.pot \
	$(ANDROID_FOLDER)help/en/Object_Given.pot \
	$(ANDROID_FOLDER)help/en/category_select.pot \
	$(ANDROID_FOLDER)help/en/category_edit.pot \
	$(ANDROID_FOLDER)help/en/archive_export.pot \
	$(ANDROID_FOLDER)help/en/contact_browse.pot \
	$(ANDROID_FOLDER)help/en/custom_currency.pot

MANUAL_TRANS = AddShopItem History Alarms Introduction Author Lend Manual_time Borrow New_file Copy New_fund Credit_condone Object_Get Credit_remove Object_Given \
	Credit_set Open_file Debt_condone Options Debt_remove Password Debt_set Default_fund Remove_fund Edit_fund Remove_item Exit Revert_file \
	Expense_operations SaveAs_file Export_file Save_file Find Shortcuts Gain_operations Total_fund GetBack Update GiveBack Wizard Global_datetime \
	category_select category_edit archive_export contact_browse custom_currency

# Build number file. Increment if any object file changes.
$(BUILD_NUMBER_FILE)_android:
ifeq ($(INCREMENT),0)
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 1 > $(BUILD_NUMBER_FILE); fi
else
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 0 > $(BUILD_NUMBER_FILE); fi
endif
ifeq ($(INCREMENT),1)
	@echo $$(($$(cat $(BUILD_NUMBER_FILE)) + 1)) > $(BUILD_NUMBER_FILE)
endif

create_translation_templates_manual: $(MANUAL_POTS)
	xml2pot help/omb_en.xml > help/omb_en.pot

$(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/help/en/%.pot: $(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/help/en/%.xml
	xml2pot $< > $@

create_translation_messages_manual: $(MANUAL_TRANS)
# After creating messages it is necessary to manually update:
#      - main file: paths for entries and lang id
#      - Author.xml: 'Author' text
# Italian
	po2xml $(ANDROID_FOLDER)help/omb_en.xml $(ANDROID_FOLDER)help/omb_it.po \
		 > $(ANDROID_FOLDER)help/omb_it.xml

#help/it/%.xml: help/en/%_it.pot
%:
# Italian
	mkdir -p $(ANDROID_FOLDER)help/it
	@if test -f $(ANDROID_FOLDER)help/en/$@.xml; then po2xml $(ANDROID_FOLDER)help/en/$@.xml \
		$(ANDROID_FOLDER)help/en/$@_it.po > $(ANDROID_FOLDER)help/it/$@.xml; fi

create_manual:
	xsltproc -o $(ANDROID_FOLDER)help/$(APPNAME)-android_en.html $(ANDROID_FOLDER)help/android.xsl \
		$(ANDROID_FOLDER)help/omb_en.xml
	xsltproc -o $(ANDROID_FOLDER)help/$(APPNAME)-android_it.html $(ANDROID_FOLDER)help/android.xsl \
		$(ANDROID_FOLDER)help/omb_it.xml

android:  $(BUILD_NUMBER_FILE)_android
	(cd $(ANDROID_FOLDER)$(ANDROID_API_LEVEL) && ./gradlew assembleRelease)
	mv $(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/build/outputs/apk/$(ANDROID_API_LEVEL)-release.apk ../openmoneybox_$(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))).apk
	@echo $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) $(ANDROID_API_LEVEL) built.

