# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-09-06 14:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Credit_set.xml:1
#, no-c-format
msgid "Store a new credit"
msgstr ""

#. Tag: para
#: Credit_set.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Credits</guimenu> -&gt; <guimenuitem>Set</guimenuitem> (or the related button in the toolbar) to store a new credit."
msgstr ""

#. Tag: para
#: Credit_set.xml:7
#, no-c-format
msgid "Type in the first edit ox the name of new credit (usually the debtor name)."
msgstr ""

#. Tag: para
#: Credit_set.xml:8
#, no-c-format
msgid "Type in the second edit box the credit value. If a credit with this name already exists the new inserted value will be added to the one previously stored."
msgstr ""

#. Tag: chapter
#: Credit_set.xml:10
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Credit_set.xml:12
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: Credit_set.xml:13
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> <keycap>CTRL</keycap> <keycap>ALT</keycap> <keycap>C</keycap> </keycombo>"
msgstr ""

