# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Igor Calì <igor.cali0@gmail.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-08-31 13:34+0000\n"
"PO-Revision-Date: 2014-08-31 16:31+0200\n"
"Last-Translator: Igor Calì <igor.cali0@gmail.com>\n"
"Language-Team: Italiano <>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#. Tag: title
#: Remove_fund.xml:1
#, no-c-format
msgid "Remove a fund"
msgstr "Rimuovere un fondo"

#. Tag: para
#: Remove_fund.xml:2
#, no-c-format
msgid ""
"Click the menu item <guimenu>Funds</guimenu> -&gt;<guimenuitem>Remove</"
"guimenuitem> (or related button in the toolbar) to remove a previously "
"stored fund."
msgstr ""
"Premere la voce di menu <guimenu>Fondi</guimenu> -&gt;<guimenuitem>Rimuovi</"
"guimenuitem> (od il relativo pulsante sulla barra degli strumenti) per "
"rimuovere un fondo."

#. Tag: para
#: Remove_fund.xml:7
#, no-c-format
msgid ""
"Select from the dropbox the fund to be removed: it will be removed from the "
"list in the left of the client window."
msgstr ""
"Selezionare dalla casella a cascata il fondo da rimuovere: esso verrà "
"eliminato dalla lista dei dati sulla sinistra della finestra del programma. "

#. Tag: para
#: Remove_fund.xml:9
#, no-c-format
msgid ""
"The value of removed fund will be added to the <link linkend=\"deffund"
"\">default fund</link>."
msgstr ""
"Il valore del fondo eliminato verrà aggiunto al <link linkend=\"deffund"
"\">fondo predefinito</link>."

#. Tag: para
#: Remove_fund.xml:10
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""
"In caso di inserimenti mancanti od errati verrà generato un messaggio di "
"errore."

#. Tag: para
#: Remove_fund.xml:11
#, no-c-format
msgid ""
"<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> "
"<keycap>CTRL</keycap> <keycap>ALT</keycap> <keycap>R</keycap> </keycombo>"
msgstr ""
"<link linkend=\"scuts\">Tasto di scelta rapida</link>: <keycombo "
"action=\"simul\"> <keycap>CTRL</keycap>  <keycap>ALT</keycap>  <keycap>R</"
"keycap></keycombo>"

#. Tag: para
#: Remove_fund.xml:18
#, no-c-format
msgid "Related topics: <link linkend=\"deffund\">Set the default fund</link>."
msgstr ""
"Argomenti correlati: <link linkend=\"deffund\">Impostare il fondo "
"predefinito</link>."
