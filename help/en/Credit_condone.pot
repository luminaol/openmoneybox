# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-09-06 14:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Credit_condone.xml:1
#, no-c-format
msgid "Condone a credit"
msgstr ""

#. Tag: para
#: Credit_condone.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Credits</guimenu> -&gt; <guimenuitem>Condone</guimenuitem> (or the related button in the toolbar) to condone a previously stored credit."
msgstr ""

#. Tag: para
#: Credit_condone.xml:7
#, no-c-format
msgid "Select in the drop-box the credit to be condoned."
msgstr ""

#. Tag: para
#: Credit_condone.xml:8
#, no-c-format
msgid "Remove:"
msgstr ""

#. Tag: listitem
#: Credit_condone.xml:11
#, no-c-format
msgid "<emphasis>the whole value</emphasis>: completely condones the credit;"
msgstr ""

#. Tag: listitem
#: Credit_condone.xml:14
#, no-c-format
msgid "<emphasis>partially</emphasis>: condones only a part of the credit. In this case, type in the edit box below the condoned amount of money."
msgstr ""

#. Tag: chapter
#: Credit_condone.xml:19
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Credit_condone.xml:21
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

