# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-08 22:23+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Lend.xml:1
#, no-c-format
msgid "Store an object loan"
msgstr ""

#. Tag: para
#: Lend.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Objects</guimenu> -&gt; <guimenuitem>Lend</guimenuitem> (or the related button in the toolbar) to store an object loan."
msgstr ""

#. Tag: para
#: Lend.xml:7
#, no-c-format
msgid "Type in the first edit-box the lent object."
msgstr ""

#. Tag: para
#: Lend.xml:8
#, no-c-format
msgid "Type in the second edit-box the object receiver."
msgstr ""

#. Tag: para
#: Lend.xml:9
#, no-c-format
msgid "If it is necessary to set an <link linkend=\"alm\">alarm</link> for the object, mark the alarm checkbox and select the loan expiration date."
msgstr ""

#. Tag: note
#: Lend.xml:10
#, no-c-format
msgid "This function is useful only if the <link linkend=\"tray\">Alarm Management Utility</link> is installed.."
msgstr ""

#. Tag: para
#: Lend.xml:11
#, no-c-format
msgid "The borrowed object will be visible under the name of who borrowed the object."
msgstr ""

#. Tag: sect1
#: Lend.xml:11
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Lend.xml:13
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: Lend.xml:14
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> <keycap>SHIFT</keycap> <keycap>F3</keycap> </keycombo>"
msgstr ""

