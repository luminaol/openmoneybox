# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-03 16:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Gain_operations.xml:1
#, no-c-format
msgid "Store a profit"
msgstr ""

#. Tag: para
#: Gain_operations.xml:2
#, no-c-format
msgid "Tap the button <guiicon>Profit</guiicon> in the Dashboard tab to store a profit."
msgstr ""

#. Tag: para
#: Gain_operations.xml:6
#, no-c-format
msgid "Select from the dropbox the fund to add the profit to."
msgstr ""

#. Tag: para
#: Gain_operations.xml:7
#, no-c-format
msgid "Type in the first edit box the value to add to the fund."
msgstr ""

#. Tag: para
#: Gain_operations.xml:8
#, no-c-format
msgid "Insert in the second edit box the reason of this profit (<emphasis>required</emphasis>)."
msgstr ""

#. Tag: chapter
#: Gain_operations.xml:8
#, no-c-format
msgid "&remark;"
msgstr ""

#. Tag: para
#: Gain_operations.xml:10
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

