# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-03 17:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Credit_remove.xml:1
#, no-c-format
msgid "Remove a credit"
msgstr ""

#. Tag: para
#: Credit_remove.xml:2
#, no-c-format
msgid "Tap the button <guiicon>Remove credit</guiicon> in the Credits tab to remove a previously stored credit."
msgstr ""

#. Tag: para
#: Credit_remove.xml:6
#, no-c-format
msgid "Select in the drop-box the credit to be modified or removed."
msgstr ""

#. Tag: para
#: Credit_remove.xml:7
#, no-c-format
msgid "Remove:"
msgstr ""

#. Tag: listitem
#: Credit_remove.xml:10
#, no-c-format
msgid "<emphasis>the whole value</emphasis>: completely removes the credit;"
msgstr ""

#. Tag: listitem
#: Credit_remove.xml:13
#, no-c-format
msgid "<emphasis>partially</emphasis>: removes only a part of the credit. In this case, type in the edit box below the collected amount of money."
msgstr ""

#. Tag: para
#: Credit_remove.xml:19
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

