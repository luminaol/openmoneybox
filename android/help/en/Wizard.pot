# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-08-03 17:01+0000\n"
"PO-Revision-Date: 2017-03-11 21:49+0200\n"
"Last-Translator: Igor Calì <igor.cali0@gmail.com>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Wizard.xml:1
#, no-c-format
msgid "Document creation wizard - OmbWizard"
msgstr ""

#. Tag: para
#: Wizard.xml:2
#, no-c-format
msgid "OmbWizard is a tool for the assisted creation of OpenMoneyBox documents."
msgstr ""

#. Tag: para
#: Wizard.xml:3
#, no-c-format
msgid "First time OpenMoneyBox is started - or whenever there is no default document set - a dialog allows to browse for an existing OpenMoneyBox document or to run OmbWizard."
msgstr ""

#. Tag: para
#: Wizard.xml:4
#, no-c-format
msgid "Tap the button <guiicon>Document creation Wizard</guiicon> to create a new document using OmbWizard assisted procedure."
msgstr ""

