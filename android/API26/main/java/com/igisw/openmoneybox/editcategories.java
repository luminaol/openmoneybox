/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-11-24
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class editcategories extends Activity {

	private boolean modified;
	private int item_pos;
	private ArrayList<String> categoryList;
	private ArrayAdapter<String> categoryAdapter;
	private ImageButton btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.categories);
		
		Bundle bundle=getIntent().getExtras();
		
	    categoryList = bundle.getStringArrayList("categories");

	    // Create ArrayAdapter  
	    categoryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, categoryList);
	    // Set the ArrayAdapter as the ListView's adapter.  
	    ListView categoryView = findViewById( R.id.CategoryView );
	    categoryView.setAdapter( categoryAdapter );
	    
	    btn = findViewById( R.id.imageButton2 );
	    btn.setEnabled(false);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_categories);
			ll.setBackgroundColor(0xff000000);
		}

	    categoryView.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
	        	btn.setEnabled(position >= 0);
	        	if(position >= 0)item_pos = position;
	        }
	    });
	    
	    modified = false;

	}
	
	public void okBtnClick(View view){
		int ReturnCode /*= RESULT_CANCELED*/;
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean("modified", modified);
		if(modified)bundle.putStringArrayList("categories", categoryList);
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
	
	public void addCategory(View view){
 		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(this.getResources().getString(R.string.categories_add));

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		//input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);

		alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String value = input.getText().toString();
				if(!value.isEmpty()){
					value = value.trim();
					value = omb_library.iUpperCase(value);
					boolean Ex = false;
					for(int i = 0; i < categoryList.size(); i++){
						if(categoryList.get(i).compareToIgnoreCase(value) == 0){
							Ex = true;
							break;}}
					if(!Ex){
						categoryList.add(value);
						categoryAdapter.notifyDataSetChanged();
						modified = true;}}
				
		  }
		});

		alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
			//return false;
		  }
		});
		
		alert.show();

		//return Response;
		
	}

	public void removeCategory(View view){
		categoryList.remove(item_pos);
		categoryAdapter.notifyDataSetChanged();
		modified = true;
	}
}
