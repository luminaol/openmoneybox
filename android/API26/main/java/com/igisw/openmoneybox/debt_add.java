/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-03-10
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class debt_add extends Activity {

	private boolean browsed_contacts;
	private String id;
	private EditText nameText, valueText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.funds_add);

		browsed_contacts = false;

		TextView caption_label = findViewById(R.id.Caption);
		caption_label.setText(getResources().getString(R.string.debt_add));
		nameText = findViewById(R.id.Name);
		nameText.setHint(getResources().getString(R.string.debt_insert));
		valueText = findViewById(R.id.Value);
		valueText.setHint(getResources().getString(R.string.debt_value));

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_funds);
			ll.setBackgroundColor(0xff000000);
			caption_label.setBackgroundColor(0xff000000);
			caption_label.setTextColor(0xffffffff);
			nameText.setHintTextColor(0xff333333);
			nameText.setTextColor(0xffffffff);
			valueText.setHintTextColor(0xff333333);
			valueText.setTextColor(0xffffffff);
		}
	}
	
	public void okBtnClick(View view){
		double cur;
		int ReturnCode /*= RESULT_CANCELED*/;
		
		omb_library.appContext = getApplicationContext();

		//TextView value = (TextView) findViewById(R.id.opValue);
		String name = nameText.getText().toString();
		if(name.isEmpty()){
			omb_library.Error(30, "");
			nameText.requestFocus();
			return;}
		
		if(valueText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			valueText.requestFocus();
			return;}
		cur = Double.parseDouble(valueText.getText().toString());
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		name = omb_library.iUpperCase(name);
		name = name.trim();
		bundle.putString("debt", name);
		bundle.putDouble("value", cur);

		boolean found = false;
		// String name = null;
		if(browsed_contacts){
			Cursor phones = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI
					, null, null, null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC");
			while (phones.moveToNext()) {
				name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));

				if(id != null) {
					if(name.compareTo(nameText.getText().toString()) == 0){
						found = true;
						break;
					}
				}

			}
			phones.close();
		}

		if(found){
			if(name.compareTo(nameText.getText().toString()) == 0) {
				bundle.putString("contact_id", id);
				bundle.putString("contact_name", name);
			}
		}
		else bundle.putString("contact_id", "-1");

		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}

	public void searchContact(View view){
		omb_library.appContext = getApplicationContext();
		if(! omb_library.checkContactPermission(this)) return;

		Intent intent = new Intent(this, ContactActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("str", nameText.getText().toString());
		intent.putExtras(bundle);
		startActivityForResult(intent, 1);
	}

	@Override
	public void onActivityResult(int requestCode,int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != RESULT_OK) return;
		switch(requestCode){
			case 1:	// Contact selected
				Bundle bundle = data.getExtras();

				nameText.setText(bundle.getString("contact"));
				id = bundle.getString("id");
				browsed_contacts = true;
		}
	}

	/*
	private boolean checkContactPermission(){
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
				!= PackageManager.PERMISSION_GRANTED) {

			// Request the permission.
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.READ_CONTACTS},
					MY_PERMISSIONS_REQUEST_READ_CONTACTS);
			// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
			// app-defined int constant. The callback method gets the
			// result of the request.

			return false;
		}
		else return true;
	}
	*/

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS:
				boolean canUseContacts = false;
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					canUseContacts = true;
				}

				if (!canUseContacts) {
					Toast.makeText(getApplicationContext(), "Contacts cannot be accessed.", Toast.LENGTH_LONG).show();
					finish();
				}
				/*
				else {
					// user now provided permission
					// perform function for what you want to achieve
				}
				*/
				break;
		}
	}

}
