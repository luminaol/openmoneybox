/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-26
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Currency;
import java.util.Locale;

public class operation extends Activity {

	private EditText value_edit;
	private AutoCompleteTextView matters_View;
	private Spinner fund_spinner;
	private Spinner category_spinner;
	private CheckBox currency_checkbox;
	private EditText currency_symbol;
	private EditText currency_rate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operation);
		
		Bundle bundle=getIntent().getExtras();

		TextView caption = findViewById(R.id.Caption);
		TextView fund_label = findViewById(R.id.fundLabel);
		TextView category_label = findViewById(R.id.categoryLabel);
		value_edit = findViewById(R.id.opValue);
		matters_View = findViewById(R.id.Matter);

		currency_checkbox = findViewById(R.id.currencyCheckbox);
		currency_symbol = findViewById(R.id.currency);
		currency_rate = findViewById(R.id.rate);

		currency_symbol.setText(Currency.getInstance(Locale.getDefault()).getSymbol());

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_operation);
			ll.setBackgroundColor(0xff000000);
			caption.setBackgroundColor(0xff000000);
			caption.setTextColor(0xffffffff);
			fund_label.setBackgroundColor(0xff000000);
			fund_label.setTextColor(0xffffffff);
			category_label.setBackgroundColor(0xff000000);
			category_label.setTextColor(0xffffffff);
			value_edit.setHintTextColor(0xff333333);
			value_edit.setTextColor(0xffffffff);
			matters_View.setHintTextColor(0xff333333);
			matters_View.setTextColor(0xffffffff);

			currency_checkbox.setTextColor(0xffffffff);
			currency_symbol.setTextColor(0xffffffff);
			currency_symbol.setHintTextColor(0xff333333);
			currency_rate.setTextColor(0xffffffff);
			currency_rate.setHintTextColor(0xff333333);

		}

		if(bundle.getBoolean("change_labels")){
			caption.setText(getResources().getString(R.string.expense_caption));
			fund_label.setText(getResources().getString(R.string.expense_fund));
			value_edit.setHint(getResources().getString(R.string.hint_insert_value));
			matters_View.setHint(getResources().getString(R.string.hint_insert_reason));
			category_label.setText(getResources().getString(R.string.expense_category));
		}

        // Autocomplete matters
		ArrayAdapter<String> matters_adapter = new ArrayAdapter<>(this,
            android.R.layout.simple_dropdown_item_1line, bundle.getStringArrayList("matters"));
        matters_View.setAdapter(matters_adapter);

        // Fill spinner w/ funds
        fund_spinner = findViewById(R.id.Fund);
        ArrayAdapter<String> funds_adapter = new ArrayAdapter<>(this,
             android.R.layout.simple_spinner_item, bundle.getStringArrayList("funds"));
        funds_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fund_spinner.setAdapter(funds_adapter);
        fund_spinner.setSelection(bundle.getInt("defaultfundindex"));

        // Fill spinner w/ categories
        category_spinner = findViewById(R.id.Category);
        ArrayAdapter<String> categories_adapter = new ArrayAdapter<>(this,
             android.R.layout.simple_spinner_item, bundle.getStringArrayList("categories"));
        categories_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(categories_adapter);

        currencyClick(null);
	}
	
	public void okBtnClick(View view){
		double cur;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		if(value_edit.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			value_edit.requestFocus();
			return;}
		cur = Double.parseDouble(value_edit.getText().toString());
		String matter = matters_View.getText().toString();
		if(matter.isEmpty()){
			omb_library.Error(29, "");
			matters_View.requestFocus();
			return;}

		if(! currency_checkbox.isChecked()){
			if(currency_symbol.getText().toString().isEmpty()){
				omb_library.Error(49, "");
				currency_symbol.requestFocus();
				return;}
			if(currency_rate.getText().toString().isEmpty()){
				omb_library.Error(25, "");
				currency_rate.requestFocus();
				return;}

		}

		matter = omb_library.iUpperCase(matter);
		matter = matter.trim();

		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString("fund", fund_spinner.getSelectedItem().toString());
		bundle.putDouble("value", cur);
		bundle.putString("matter", matter);
		bundle.putInt("category", category_spinner.getSelectedItemPosition());

		long curr_index;
		double curr_rate;
		if(currency_checkbox.isChecked()){
			curr_index = -1;
			curr_rate = 1;
		}
		else{
			curr_index = 1;
			curr_rate = Double.parseDouble(currency_rate.getText().toString());
		}
		bundle.putLong("currency", curr_index);
		bundle.putDouble("rate", curr_rate);
		bundle.putString("symbol", currency_symbol.getText().toString());

		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}

	public void currencyClick(View view){
		boolean show = ! currency_checkbox.isChecked();

		if(show){
			currency_symbol.setVisibility(View.VISIBLE);
			currency_rate.setVisibility(View.VISIBLE);
		}
		else{
			currency_symbol.setVisibility(View.GONE);
			currency_rate.setVisibility(View.GONE);
		}

	}
}
