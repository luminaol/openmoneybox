/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-11-24
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class mainoptions extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
        if(Opts.getBoolean("GDarkTheme", false)) {
            this.setTheme(R.style.DarkTheme);
        }

        try {
            getClass().getMethod("getFragmentManager");
            getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PF()).commit();
        } catch (NoSuchMethodException e) { //Api < 11

        }
    }

    public static class PF extends PreferenceFragment
    {       
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.options);
        }
    }

}
