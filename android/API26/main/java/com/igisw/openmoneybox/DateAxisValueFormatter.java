/* **************************************************************
 * Name:      
 * Purpose:   Date formatter for MPAndroidChart
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-04-14
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;

class DateAxisValueFormatter extends ValueFormatter
{
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");

    public String getFormattedValue(float value) {
        return sdf.format(new Date((long)value)); }
}

