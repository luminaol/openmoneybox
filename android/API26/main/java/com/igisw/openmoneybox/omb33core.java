/* *************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.ContactsContract;
import android.widget.ProgressBar;

import net.sqlcipher.database.SQLiteConstraintException;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.igisw.openmoneybox.constants.ombFileFormat.bilFFORMAT_UNKNOWN;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_31;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_33;

class omb33core extends omb31core
{
	boolean successfulKey = false;	//	set to true when Key is ok

	public class TVal extends TVal_v31{
		public long ContactIndex;
		TVal(){
			super();
			ContactIndex = -1;
		}
	}

	public class TObj extends TObj_v31{
		public long ContactIndex;
		TObj(){
			super();
			ContactIndex = -1;
		}
	}

	public class TLine extends TLine_v31{
		public long ContactIndex;
		public double Latitude;
		public double Longitude;

		public long currencyIndex;
		public double currencyRate;
		public String currencySymbol;

		TLine(){
			super();
			ContactIndex = -1;
			Latitude = constants.ombInvalidLatitude;
			Longitude = constants.ombInvalidLongitude;

			currencyIndex = -1;
			currencyRate = 1;
			currencySymbol = "";
		}
	}

	public final ArrayList<TVal> Credits;
	public final ArrayList<TVal> Debts;
	public final ArrayList<TObj> Lent;
	public final ArrayList<TObj> Borrowed;
    public final ArrayList<TLine> Lines;

    public omb33core(String document, mainactivity mainframe, ProgressBar ProgressBar){
		super (document, mainframe, ProgressBar);

		Credits = new ArrayList<>();
		Debts = new ArrayList<>();
		Lent = new ArrayList<>();
		Borrowed = new ArrayList<>();
        Lines = new ArrayList<>();
		//NLin = 0;
		openDatabase(document);
	}

	public boolean findContact(String contact_id, String contact_name){
		boolean contact_exists = false;
		if(! tableExists(database, "Contacts"))
			database.execSQL(constants.cs_contacts);
		else{
			Cursor contactsTable = database.query("Contacts", new String[] { "contact", "status" }, "mobile_id = " + contact_id, null, null, null, null);
			while (contactsTable.moveToNext()) {
				String name = contactsTable.getString(0);
				if(contactsTable.getInt(1) > 0){
					if(contact_name.compareTo(name) == 0){
						contact_exists = true;
						break;
					}
				}
			}
			contactsTable.close();
		}

		if(! contact_exists){
			ContentValues con = new ContentValues();
			con.put("mobile_id", contact_id);
			con.put("contact", contact_name);
			con.put("status", true);
			if(database.insert("Contacts", null, con) == -1)
				return false;

			generateContactImage(contact_id);
		}

		return true;
	}

	private void updateContactTable(String contact_id/*, String contact_name*/){
		boolean contact_exists = false;
		String name = null;
		//String valid = null;
		// No need to check table existence
		Cursor contactsTable = database.query("Contacts", new String[] { "mobile_id", "contact"/*, "status"*/ }, "mobile_id = " + contact_id, null, null, null, null);
		while (contactsTable.moveToNext()) {
			if(contact_id.compareTo(contactsTable.getString(0)) == 0){
				name = contactsTable.getString(1);
				//valid = contactsTable.getString(2);
				contact_exists = true;
				break;
			}
		}
		contactsTable.close();

		if(contact_exists){

			// The index of the _ID column in the Cursor
			int mIdColumn;
			int mNameColumn;

			Cursor phones = frame.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
					null, null, null,
					ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC");
			boolean found_name = false;
			String phone_id = null;
			while (phones.moveToNext()) {
				// Gets the primary name column index
				mNameColumn = phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY);
				String phone_name = phones.getString(mNameColumn);
				if(phone_name.compareTo(name) == 0) {
					found_name = true;

					mIdColumn = phones.getColumnIndex(ContactsContract.Contacts._ID);
					phone_id = phones.getString(mIdColumn);

					break;
				}

			}
			phones.close();

			if(found_name){
				// Update Contacts table
				ContentValues cv = new ContentValues();
				cv.put("mobile_id", phone_id);
				database.update("Contacts", cv, "mobile_id=?",
						new String[]{contact_id});

				cv.clear();
				cv.put("contact_index", phone_id);

				// Update Transactions table
				database.update("Transactions", cv, "contact_index=?",
						new String[]{contact_id});
				// Update Credits table
				if(tableExists(database, "Credits"))
					database.update("Credits", cv, "contact_index=?",
							new String[]{contact_id});
				// Update Debts table
				if(tableExists(database, "Debts"))
					database.update("Debts", cv, "contact_index=?",
							new String[]{contact_id});
				// Update Borrows table
				if(tableExists(database, "Borrows"))
					database.update("Borrows", cv, "contact_index=?",
							new String[]{contact_id});
				// Update Loans table
				if(tableExists(database, "Loans"))
					database.update("Loans", cv, "contact_index=?",
							new String[]{contact_id});

				// TODO: update master database

				boolean modified = FileData.Modified;
				parseDatabase();
				FileData.Modified = modified;

				generateContactImage(phone_id);

			}
			else{
				ContentValues cv = new ContentValues();
				cv.put("status", false);
				database.update("Contacts", cv, "mobile_id=?", new String[]{contact_id});
			}

		}
	}

	private boolean generateContactImage(String id){
		// Retrieve contact image
		String img = getContactImage(Integer.parseInt(id));
		if(img == null) return false;
        // path selection
        String document = Opts.getString("GDDoc", "NULL");
        File file = new File(document);
        String dir = file.getParent() + "/ombContactImgs";
        file = new File (dir);
        if(! file.exists())
            if (! file.mkdir()) return false;

        String filename = dir + "/" + id + ".png";

        Bitmap bmp = omb_library.loadContactPhotoThumbnail(img);
        if (bmp == null) return false;
        OutputStream os;
        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            return false;
        }
        bmp.compress(Bitmap.CompressFormat.PNG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            //e.printStackTrace();
            return false;
        }
        return true;
	}

	public boolean addOper(int id, GregorianCalendar D, GregorianCalendar O, TOpType T, String V,
						   String M, long N, long c_index,
						   boolean haslocation, double lat, double lon,
						   long curr_Id, double curr_Rate, String curr_Symb)
	{
		if(T == TOpType.toNULL || V.isEmpty() || M.isEmpty())return false;
		String Tot = omb_library.FormDigits(Tot_Funds, true);
		if(Parsing){
			TLine temp = new TLine();
			temp.Id = id;
			temp.IsDate = false;
			temp.Date = D;
			temp.Time = O;
			temp.Type = T;
			temp.Value = V;
			temp.Reason = M;
			temp.CategoryIndex = N;
			temp.ContactIndex = c_index;
			temp.Latitude = lat;
			temp.Longitude = lon;
			temp.currencyIndex = curr_Id;
			temp.currencyRate = curr_Rate;
			temp.currencySymbol = curr_Symb;
			Lines.add(NLin, temp);
		}
		else{
			// Last date check
			boolean found = false;
			if(NLin < 1){
				addDate(-1, D, Tot);
				found = true;
			}
			else for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate){
				GregorianCalendar cal1 = Lines.get(i).Date;
				boolean sameday = cal1.get(Calendar.YEAR) == D.get(Calendar.YEAR) &&
						cal1.get(Calendar.DAY_OF_YEAR) == D.get(Calendar.DAY_OF_YEAR);
				if(sameday){
					found = true;
					break;}}
			if (! found) addDate(-1, D, Tot);

			ContentValues cv = new ContentValues();

			cv.put("isdate", false);
			cv.put("date", D.getTimeInMillis() / 1000);
			cv.put("time", O.getTimeInMillis() / 1000);

			int value = 0;
			switch (T) {
				case toGain:
					value = 1;
					break;
				case toExpe:
					value = 2;
					break;
				case toSetCre:
					value = 3;
					break;
				case toRemCre:
					value = 4;
					break;
				case toConCre:
					value = 5;
					break;
				case toSetDeb:
					value = 6;
					break;
				case toRemDeb:
					value = 7;
					break;
				case toConDeb:
					value = 8;
					break;
				case toGetObj:
					value = 9;
					break;
				case toGivObj:
					value = 10;
					break;
				case toLenObj:
					value = 11;
					break;
				case toBakObj:
					value = 12;
					break;
				case toBorObj:
					value = 13;
					break;
				case toRetObj:
					value = 14;
					break;
			}

			cv.put("type", value);
			cv.put("value", V);
			cv.put("reason", M);
			cv.put("cat_index", N);
			cv.put("contact_index", c_index);
			if(haslocation) {
				cv.put("latitude", lat);
				cv.put("longitude", lon);
			}
			else{
				cv.put("latitude", constants.ombInvalidLatitude);
				cv.put("longitude", constants.ombInvalidLongitude);
			}

			cv.put("currencyid", curr_Id);
			cv.put("currencyrate", curr_Rate);
			cv.put("currencysymb", curr_Symb);
			database.insert("Transactions", null, cv);

			FileData.Modified=true;
			parseDatabase();
		}
		return true;
	}

	private void openDatabase(String File){
		String pwd = getKey(false);
		String pwd_archive = getKey(true);

		java.io.File f = new File(File);
		boolean file_exist = f.exists();

		SQLiteDatabaseHook hook = null;
		if(! file_exist) {
			hook = new SQLiteDatabaseHook() {
				public void preKey(SQLiteDatabase database) {
					database.execSQL(constants.cipher_compat_sql);
				}

				public void postKey(SQLiteDatabase database) {
				}
			};
		}

		constants.ombFileFormat format = omb_library.checkFileFormat(File, pwd);
		if(format == bilFFORMAT_UNKNOWN) return;
		database = SQLiteDatabase.openOrCreateDatabase(File, pwd, null, hook);

		//database.rawQuery("pragma journal_mode=memory;", null);

		// Set restore savepoint
		database.execSQL("SAVEPOINT rollback;");

		if(file_exist){

			/*constants.ombFileFormat format = omb_library.checkFileFormat(File, pwd);
			if(format == bilFFORMAT_UNKNOWN) return;
			else*/ successfulKey = true;

			File fMaster;
			String master;
			ContentValues cv;
			SQLiteDatabase db;
			switch(format){
				case bilFFORMAT_UNKNOWN:	// Unknown format
					omb_library.Error(2, File);
					Parsing = false;
					return;
				case ombFFORMAT_332:
					break;
				case ombFFORMAT_33:
					database.execSQL("ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
					database.execSQL("ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
					database.execSQL("ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

					cv = new ContentValues();
					cv.put("currencyid", -1);
					database.update("Transactions", cv, "isdate = 0", null);
					database.setVersion(constants.dbVersion);
					database.execSQL("RELEASE rollback;");

					fMaster = frame.getDatabasePath(constants.archive_name);
					master = fMaster.toString();
					if(fMaster.exists()){
						if(omb_library.checkFileFormat(master, pwd_archive) == ombFFORMAT_33) {
							db = SQLiteDatabase.openOrCreateDatabase(master, pwd_archive, null);

							db.execSQL("ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
							db.execSQL("ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
							db.execSQL("ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

							db.update("Transactions", cv, "isdate = 0", null);

							db.setVersion(constants.dbVersion);
							db.execSQL("RELEASE rollback;");
						}
					}

					break;
				case ombFFORMAT_32:
					break;
				case ombFFORMAT_31:
					database.execSQL("ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
					database.execSQL("ALTER TABLE Transactions ADD COLUMN latitude REAL;");
					database.execSQL("ALTER TABLE Transactions ADD COLUMN longitude REAL;");

					cv = new ContentValues();
					cv.put("latitude", omb_library.FormDigits(constants.ombInvalidLatitude, true));
					cv.put("longitude", omb_library.FormDigits(constants.ombInvalidLongitude, true));
					database.update("Transactions", cv, "isdate = 0", null);

					if(tableExists(database, "Credits"))
						database.execSQL("ALTER TABLE Credits ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Debts"))
						database.execSQL("ALTER TABLE Debts ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Borrows"))
						database.execSQL("ALTER TABLE Borrows ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Loans"))
						database.execSQL("ALTER TABLE Loans ADD COLUMN contact_index INTEGER;");
					database.setVersion(constants.dbVersion);
					database.execSQL("RELEASE rollback;");

					fMaster = frame.getDatabasePath(constants.archive_name);
					master = fMaster.toString();
					if(fMaster.exists()){
						if(omb_library.checkFileFormat(master, pwd_archive) == ombFFORMAT_31) {
							String colName;
							Cursor tableList;
							db = SQLiteDatabase.openOrCreateDatabase(master, pwd_archive, null);

							db.execSQL("ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
							db.execSQL("ALTER TABLE Transactions ADD COLUMN latitude REAL;");
							db.execSQL("ALTER TABLE Transactions ADD COLUMN longitude REAL;");

							db.update("Transactions", cv, "isdate = 0", null);

							tableList = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table';", null);
							for(int i = 0; i < tableList.getCount(); i++){
								tableList.moveToPosition(i);
								colName = tableList.getString(0);
								if(colName.startsWith("Credits"))
									db.execSQL(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Debts"))
									db.execSQL(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Loans"))
									db.execSQL(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Borrows"))
									db.execSQL(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
							}
							db.setVersion(constants.dbVersion);
							db.execSQL("RELEASE rollback;");

							tableList.close();
						}
					}

					break;

				// Comment following line to compile with bilFFORMAT_302 support
				/*
				case bilFFORMAT_302:
					omb30core olddoc = new omb30core(File);
					int i;
					//double cur;

					database = SQLiteDatabase.openOrCreateDatabase(File, null);

					// Write Funds
					if(! tableExists(database, "Funds")) database.execSQL(constants.cs_funds);
					for(i = 0; i < olddoc.NFon; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Funds.get(i).Name);
						cv.put("value", olddoc.Funds.get(i).Value);
						database.insert("Funds", null, cv);
					}
					// Write default fund
					if(! tableExists(database, "Information")){
						database.execSQL(constants.cs_information);

						ContentValues cv = new ContentValues();

						String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
								+ android.os.Build.VERSION.RELEASE;
						cv.put("id", String.format("%d", constants.dbMeta_application_info));
						cv.put("data", metadata);
						database.insert("Information", null, cv);

						cv.clear();
						cv.put("id", String.format("%d", constants.dbMeta_default_fund));
						cv.put("data", olddoc.FileData.DefFund);
						database.insert("Information", null, cv);
					}

					// Write Credits
					if(! tableExists(database, "Credits")) database.execSQL(constants.cs_credits);
					for(i = 0; i < olddoc.NCre; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Credits.get(i).Name);
						cv.put("value", olddoc.Credits.get(i).Value);
						database.insert("Credits", null, cv);
					}

					// Write Debts
					if(! tableExists(database, "Debts")) database.execSQL(constants.cs_debts);
					for(i = 0; i < olddoc.NDeb; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Debts.get(i).Name);
						cv.put("value", olddoc.Debts.get(i).Value);
						database.insert("Debts", null, cv);
					}

					// Write Loans
					if(! tableExists(database, "Loans")) database.execSQL(constants.cs_loans);
					for(i = 0; i < olddoc.NLen; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Lent.get(i).Name);
						cv.put("item", olddoc.Lent.get(i).Object);
						cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Loans", null, cv);
					}

					// Write Borrows
					if(! tableExists(database, "Borrows")) database.execSQL(constants.cs_borrows);
					for(i = 0; i < olddoc.NBor; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Lent.get(i).Name);
						cv.put("item", olddoc.Lent.get(i).Object);
						cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Borrows", null, cv);
					}

					// Write Categories
					if(! tableExists(database, "Categories")) database.execSQL(constants.cs_categories);
					for(i = 0; i < olddoc.CategoryDB.size(); i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.CategoryDB.get(i));
						cv.put("active", 1);
						database.insert("Categories", null, cv);
					}

					// Write Transactions
					if(! tableExists(database, "Transactions")) database.execSQL(constants.cs_transactions);
					for(i = 0; i < olddoc.NLin; i++){
						ContentValues cv = new ContentValues();
						cv.put("isdate", olddoc.Lines.get(i).IsDate);
						cv.put("date", olddoc.Lines.get(i).Date.getTimeInMillis() / 1000);
						cv.put("time", olddoc.Lines.get(i).Time.getTimeInMillis() / 1000);

						int value = 0;
						omb30core.TOpType T = olddoc.Lines.get(i).Type;
						if(T == omb30core.TOpType.toGain)value = 1;
						else if(T == omb30core.TOpType.toExpe)value = 2;
						else if(T == omb30core.TOpType.toSetCre)value = 3;
						else if(T == omb30core.TOpType.toRemCre)value = 4;
						else if(T == omb30core.TOpType.toConCre)value = 5;
						else if(T == omb30core.TOpType.toSetDeb)value = 6;
						else if(T == omb30core.TOpType.toRemDeb)value = 7;
						else if(T == omb30core.TOpType.toConDeb)value = 8;
						else if(T == omb30core.TOpType.toGetObj)value = 9;
						else if(T == omb30core.TOpType.toGivObj)value = 10;
						else if(T == omb30core.TOpType.toLenObj)value = 11;
						else if(T == omb30core.TOpType.toBakObj)value = 12;
						else if(T == omb30core.TOpType.toBorObj)value = 13;
						else if(T == omb30core.TOpType.toRetObj)value = 14;

						cv.put("type", value);
						String cnv = olddoc.Lines.get(i).Value.replace(",", ".");
						cv.put("value", cnv);
						cv.put("reason", olddoc.Lines.get(i).Reason);
						cv.put("cat_index", olddoc.Lines.get(i).CategoryIndex + 1);
						database.insert("Transactions", null, cv);

					}

					// Write shopping list
					if(! tableExists(database, "Shoplist")) database.execSQL(constants.cs_shoplist);
					for(i = 0; i < olddoc.NSho; i++){
						ContentValues cv = new ContentValues();
						cv.put("item", olddoc.ShopItems.get(i).Name);
						cv.put("alarm", olddoc.ShopItems.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Shoplist", null, cv);

					}

					database.close();

					break;
				*/ // bilFFORMAT_302 ---
				default:
					Parsing = false;
					return /*false*/;}
		}
		else database.setVersion(constants.dbVersion);

		// Write file metadata
		String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
				+ android.os.Build.VERSION.RELEASE;

		ContentValues cv = new ContentValues();

		if (!tableExists(database, "Information")) {
			database.execSQL(constants.cs_information);

			cv.put("id", String.format("%d", constants.dbMeta_application_info));
			cv.put("data", metadata);
			database.insert("Information", null, cv);
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_default_fund));
			cv.put("data", "default");
			database.insert("Information", null, cv);
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_mobile_export));
			cv.put("data", "");
			database.insert("Information", null, cv);
		} else {
			cv.put("data", metadata);
			database.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
		}

		// Set currency when if not present (added later in dbVersion 31)
		Cursor currencyTable = database.query("Information", new String[] { "data" }, "id = " + String.format("%d", constants.dbMeta_currency), null, null, null, null);
		if(currencyTable.getCount() == 0){
			String curr = Currency.getInstance(Locale.getDefault()).getSymbol();
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_currency));
			cv.put("data", curr);
			database.insert("Information", null, cv);
		}

		// Transaction table init
		if (!tableExists(database, "Transactions"))
			database.execSQL(constants.cs_transactions);

		// Category table init
		if (!tableExists(database, "Categories"))
			database.execSQL(constants.cs_categories);

		parseDatabase();
		FileData.Modified = false;

		currencyTable.close();

		//return /*true*/;
	}

    void parseDatabase(){
        Parsing = true;

        boolean is_date;
        int Rows, id, i;	// Rows: number of rows in the query table
        long ind,			// ind: category index of transaction
            contact_in;     // contact_in: contact index of transaction or object
        double val, latitude, longitude;
        TOpType type;
        String str, reason;
        GregorianCalendar date, time, lastdate = new GregorianCalendar();
        Cursor Table;

        // Read Funds
        if(tableExists(database, "Funds")){
            Table = database.query("Funds", null, null, null, null,null, "name");
            Funds.clear();
            NFun = 0;
            Tot_Funds = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                val = Table.getDouble(2);
                if(addValue(TTypeVal.tvFou, id, str, val)){
                    NFun++;
                    Tot_Funds += val;}}}

        // Read default fund
        Table = database.query("Information", new String[] { "data" }, "id = " + String.format("%d", constants.dbMeta_default_fund), null, null, null, null);
        Table.moveToPosition(0);
        FileData.DefFund = Table.getString(0);

        // Read Credits
        if(tableExists(database, "Credits")){
            Table = database.query("Credits", null, null, null, null,null, "name");
            Credits.clear();
            NCre = 0;
            Tot_Credits = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                val = Table.getDouble(2);
				contact_in = Table.getInt(3);
                if(addValue(TTypeVal.tvCre, id, str, val, contact_in)){
                    NCre++;
                    Tot_Credits += val;}}}

        // Read Debts
        if(tableExists(database, "Debts")){
            Table = database.query("Debts", null, null, null, null,null, "name");
            Debts.clear();
            NDeb = 0;
            Tot_Debts = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                val = Table.getDouble(2);
				contact_in = Table.getInt(3);
                if(addValue(TTypeVal.tvDeb, id, str, val, contact_in)){
                    NDeb++;
                    Tot_Debts += val;}}}

        // Read Loans
        if(tableExists(database, "Loans")){
            Table = database.query("Loans", null, null, null, null,null, "name");
            Lent.clear();
            NLen = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                reason = Table.getString(2);
                ind = Table.getInt(3);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				contact_in = Table.getInt(4);
                if(addObject(TObjType.toPre, id, str, reason, date, contact_in)) NLen++;}}

        // Read Borrows
        if(tableExists(database, "Borrows")){
            Table = database.query("Borrows", null, null, null, null,null, "name");
            Borrowed.clear();
            NBor = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                reason = Table.getString(2);
                ind = Table.getInt(3);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				contact_in = Table.getInt(4);
                if(addObject(TObjType.toInP, id, str, reason, date, contact_in)) NBor++;}}

        // Read Categories
        Table = database.query("Categories", null, null, null, null,null, "name");
        CategoryDB.clear();
        NCat = 0;
        Rows = Table.getCount();
        for (i = 0; i < Rows; i++){
            Table.moveToPosition(i)	;
            id = Table.getInt(0);
            str = Table.getString(1);
            is_date = (Table.getInt(2) != 0);
            if(is_date) addCategory(id, str);
        }

        // Read Transactions
        Table = database.query("Transactions", null, null, null, null,null, "date");
        if(Lines != null) Lines.clear();
        NLin = 0;
        Rows = Table.getCount();
        for (i = 0; i < Rows; i++){
            Table.moveToPosition(i);
            id = Table.getInt(0);
            is_date = (Table.getInt(1) != 0);
            date = new GregorianCalendar();
            ind = Table.getInt(2);
            date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
            ind = Table.getInt(3);
            time = new GregorianCalendar();
            time.setTimeInMillis(ind * 1000);	// dates are stored in seconds

            str = Table.getString(5);
            reason = Table.getString(6);
            ind = Table.getInt(7);

            contact_in = Table.getInt(8);
			latitude = Table.getDouble(9);
			longitude = Table.getDouble(10);

			// Read Currency information
			int currId = Table.getInt(11);
			double currRate = 1;
			String currSymb = "";
			if(currId >= 0){
				currRate = Table.getDouble(12);
				currSymb = Table.getString(13);
			}

			if(is_date){
                if(addDate(id, date, str)) NLin++;
                lastdate = date;}
            else{
                switch(Table.getInt(4)){
                    default:
                    case 0:
                        type = TOpType.toNULL;
                        break;
                    case 1:
                        type = TOpType.toGain;
                        break;
                    case 2:
                        type = TOpType.toExpe;
                        break;
                    case 3:
                        type = TOpType.toSetCre;
                        break;
                    case 4:
                        type = TOpType.toRemCre;
                        break;
                    case 5:
                        type = TOpType.toConCre;
                        break;
                    case 6:
                        type = TOpType.toSetDeb;
                        break;
                    case 7:
                        type = TOpType.toRemDeb;
                        break;
                    case 8:
                        type = TOpType.toConDeb;
                        break;
                    case 9:
                        type = TOpType.toGetObj;
                        break;
                    case 10:
                        type = TOpType.toGivObj;
                        break;
                    case 11:
                        type = TOpType.toLenObj;
                        break;
                    case 12:
                        type = TOpType.toBakObj;
                        break;
                    case 13:
                        type = TOpType.toBorObj;
                        break;
                    case 14:
                        type = TOpType.toBorObj;
                }

                if(addOper(id, date, time, type, str, reason, ind, contact_in, true, latitude, longitude, currId, currRate, currSymb))
                	NLin++;}
        }

        // Read shopping list
        if(tableExists(database, "Shoplist")){
            Table = database.query("Shoplist", null, null, null, null,null, "item collate nocase");
            ShopItems.clear();
            NSho = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                ind = Table.getInt(2);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
                if(addShopItem(id, str, date)) NSho++;}}

        FileData.Year = lastdate.get(Calendar.YEAR);
        FileData.Month = lastdate.get(Calendar.MONTH);

        Parsing = false;

        Table.close();

    }

	public boolean addDate(int id, GregorianCalendar D, String T)
	{
		if(! Parsing){
			int M, Y;
			Y = Day.get(GregorianCalendar.YEAR);
			M = Day.get(GregorianCalendar.MONTH);
			if((Y != FileData.Year) || (M != FileData.Month)){
				if(Opts.getBoolean("GConv", false)) {
					//if(frame.ChaFragment == null) frame.displayView(R.id.nav_chart);
					frame.textConvClick(null);
				}

				String Path = FileData.FileName.getParent();
				String month = String.format("%02d", FileData.Month + 1);
				String File = Opts.getString("GDPrefix", "OpenMoneyBox") + "_";
				File += String.format("%d", FileData.Year);
				File += "-" + month + ".omb";
				FileData.Modified = true;

				File fPath, fMaster, fExport;
				SQLiteDatabase db, exp;
				fMaster = frame.getDatabasePath(constants.archive_name);
				fPath = new File(fMaster.getParent());
				fPath.mkdirs();
				String master = fMaster.toString();

				String monthstring = String.format("%d_%02d", FileData.Year, FileData.Month + 1);
				String trailname = "_" + monthstring;
				String export = Path + "/" + trailname;
				fExport = new File(export);

				boolean master_exist = fMaster.exists();
				boolean export_exist = fExport.exists();

				/*
				if(! master_exist){
					String key = getKey(false);
					SharedPreferences.Editor editor = Opts.edit();
					editor.putString("key_archive", key);
					editor.commit();
				}
                */

				db = SQLiteDatabase.openOrCreateDatabase(master, getKey(true), null);
				exp = SQLiteDatabase.openOrCreateDatabase(export, getKey(false), null);

				if(! master_exist) db.setVersion(constants.dbVersion);
				if(! export_exist) exp.setVersion(constants.dbVersion);

				// Write file metadata
				String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
						+ android.os.Build.VERSION.RELEASE;

				ContentValues cv = new ContentValues();

				if(! tableExists(db, "Information")){
					db.execSQL(constants.cs_information);

					cv.put("id", String.format("%d", constants.dbMeta_application_info));
					cv.put("data", metadata);
					db.insert("Information", null, cv);
					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_default_fund));
					cv.put("data", "default");
					db.insert("Information", null, cv);
				}
				else{
					cv.put("data", metadata);
					db.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
				}

				if(! tableExists(exp, "Information")){
					exp.execSQL(constants.cs_information);

					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_application_info));
					cv.put("data", metadata);
					exp.insert("Information", null, cv);
					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_default_fund));
					cv.put("data", "default");
					exp.insert("Information", null, cv);
				}
				else{
					cv.put("data", metadata);
					exp.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
				}

				// Transaction table init
				if(! tableExists(db, "Transactions")) db.execSQL(constants.cs_transactions);
				if(! tableExists(exp, "Transactions")) exp.execSQL(constants.cs_transactions);

				// Category table init
				boolean master_had_categories = tableExists(db, "Categories");
				if(! master_had_categories) db.execSQL(constants.cs_categories);
				boolean export_had_categories = tableExists(exp, "Categories");
				if(! export_had_categories) exp.execSQL(constants.cs_categories);

				//String trailname = "_" + monthstring;
				if(! tableExists(db, "Funds" + trailname)) db.execSQL(String.format(constants.cs_funds_master, trailname));
				if(! tableExists(db, "Credits" + trailname)) db.execSQL(String.format(constants.cs_credits_master, trailname));
				if(! tableExists(db, "Debts" + trailname)) db.execSQL(String.format(constants.cs_debts_master, trailname));
				if(! tableExists(db, "Loans" + trailname)) db.execSQL(String.format(constants.cs_loans_master, trailname));
				if(! tableExists(db, "Borrows" + trailname)) db.execSQL(String.format(constants.cs_borrows_master, trailname));
				if(! tableExists(exp, "Funds" + trailname)) exp.execSQL(String.format(constants.cs_funds_master, trailname));
				if(! tableExists(exp, "Credits" + trailname)) exp.execSQL(String.format(constants.cs_credits_master, trailname));
				if(! tableExists(exp, "Debts" + trailname)) exp.execSQL(String.format(constants.cs_debts_master, trailname));
				if(! tableExists(exp, "Loans" + trailname)) exp.execSQL(String.format(constants.cs_loans_master, trailname));
				if(! tableExists(exp, "Borrows" + trailname)) exp.execSQL(String.format(constants.cs_borrows_master, trailname));

				db.close();
				exp.close();

				database.execSQL("RELEASE rollback;");
				// backup database
				try {
					omb_library.copyFile(FileData.FileName, Path + "/" + File);
				} catch (IOException e) {
					//e.printStackTrace();
					return false;
				}

				database.execSQL(String.format("ATTACH DATABASE '%s' AS master KEY '%s';", master, getKey(true)));
				database.execSQL(String.format("ATTACH DATABASE '%s' AS export KEY '%s';", export, getKey(false)));
				//List<Pair<String, String>> debug = database.getAttachedDbs();

				// Archive data in master database
				database.execSQL("INSERT INTO master.Transactions (isdate, date, time, type, value, " +
						"reason, cat_index, contact_index, latitude, longitude) SELECT isdate, date, " +
						"time, type, value, reason, cat_index, contact_index, latitude, longitude " +
						"FROM Transactions;");
				try {
					database.execSQL(String.format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname));
					database.execSQL(String.format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname));
					database.execSQL(String.format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname));
					database.execSQL(String.format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname));
					database.execSQL(String.format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname));
				}
				catch(SQLiteConstraintException e){
					// Table in the try block already exist
					e.printStackTrace();
				}
				if(! master_had_categories) database.execSQL("INSERT INTO master.Categories SELECT * FROM Categories;");

				// Archive data in export database
				database.execSQL("INSERT INTO export.Transactions (isdate, date, time, type, value, " +
						"reason, cat_index, contact_index, latitude, longitude) SELECT isdate, date, " +
						"time, type, value, reason, cat_index, contact_index, latitude, longitude " +
						"FROM Transactions;");
				database.execSQL(String.format("INSERT INTO export.Funds%s SELECT * FROM Funds;", trailname));
				database.execSQL(String.format("INSERT INTO export.Credits%s SELECT * FROM Credits;", trailname));
				database.execSQL(String.format("INSERT INTO export.Debts%s SELECT * FROM Debts;", trailname));
				database.execSQL(String.format("INSERT INTO export.Loans%s SELECT * FROM Loans;", trailname));
				database.execSQL(String.format("INSERT INTO export.Borrows%s SELECT * FROM Borrows;", trailname));
				if(! master_had_categories) database.execSQL("INSERT INTO export.Categories SELECT * FROM Categories;");

				cv.clear();
				cv.put("data", monthstring);
				database.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_mobile_export), null);

				database.execSQL("DELETE FROM Transactions");
				// ________________________________

				database.execSQL("DETACH DATABASE 'master';");
				database.execSQL("DETACH DATABASE 'export';");
				database.execSQL("VACUUM");
				database.execSQL("SAVEPOINT rollback;");

				/*
				for(int i = 0; i <= CategoryDB.size() - 1;i++)
					CategoryDB.set(i, omb_library.iUpperCase(CategoryDB.get(i)));
				Collections.sort(CategoryDB);
				*/

				Lines.clear();
				NLin = 0;
				FileData.Month = M;
				FileData.Year = Y;
			}}

		else	// Robustness code to avoid creation of duplicate date entries
			for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate)
				if(Lines.get(i).Date == D) return true;

		if(Parsing){
			TLine temp = new TLine();
			temp.Id = id;
			temp.IsDate = true;
			temp.Date = D;
			//temp.Time.set(1900, 1, 1);
			temp.Type = TOpType.toNULL;
			temp.Value = T;
			temp.Reason = null;
			temp.CategoryIndex = -1;
			temp.ContactIndex = -1;

			temp.currencyIndex = -1;
			temp.currencyRate = 1;
			temp.currencySymbol	= "";
			Lines.add(NLin, temp);
		}
		else{
			ContentValues cv = new ContentValues();

			cv.put("isdate", true);
			cv.put("date", D.getTimeInMillis() / 1000);
			cv.put("time", -1);
			cv.put("type", 0);
			cv.put("value", T);
			cv.put("reason", "");
			cv.put("cat_index", -1);
			cv.put("contact_index", -1);

			cv.put("currencyid", -1);
			cv.put("currencyrate", 1);
			cv.put("currencysymb", "");
			database.insert("Transactions", null, cv);
		}
		FileData.Modified=true;
		return true;
	}

	public boolean isDate(int R)
	{
		return Lines.get(R).IsDate;
	}

	//public QuickContactBadge createContactBadge(long c_id){
	public String getContactImage(long c_id){

		// The Cursor that contains contact rows
		//Cursor mCursor;
		// The index of the _ID column in the Cursor
		int mIdColumn;
		// The index of the LOOKUP_KEY column in the Cursor
		//int mLookupKeyColumn;
		// A content URI for the desired contact
		//Uri mContactUri;
		// A handle to the QuickContactBadge view
		//QuickContactBadge mBadge;
		// The thumbnail URI, expressed as a String.
		String mThumbnailUri = null;

		//mBadge = (QuickContactBadge) findViewById(R.id.quickbadge);
		//mBadge = new QuickContactBadge(frame.getApplicationContext());
		/*
		 * Insert code here to move to the desired cursor row
		 */

		Cursor contactsTable = database.query("Contacts", new String[] { "status" },
				"mobile_id = " + String.format("%d", c_id), null, null, null, null);
		boolean valid = false;
		while (contactsTable.moveToNext()) {
			//String name = contactsTable.getString(2);
			valid = (contactsTable.getInt(0) > 0);
		}
		contactsTable.close();

		if(! valid) return null;

		omb_library.appContext = frame.getApplicationContext();
		if(! omb_library.checkContactPermission(frame)) return null;

		Cursor phones = frame.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI
                /*ContactsContract.CommonDataKinds.Callable.CONTENT_URI*/, /*PROJECTION*/null, /*ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME + " LIKE ?"*/null, /*mSelectionArgs*/null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC"/*null*/);
		boolean found_id = false;
		while (phones.moveToNext()) {
			// Gets the _ID column index
			mIdColumn = phones.getColumnIndex(ContactsContract.Contacts._ID);
			String id = phones.getString(mIdColumn);

			if(c_id == Long.parseLong(id)) {
				found_id = true;

				// Gets the LOOKUP_KEY index
				//mLookupKeyColumn = phones.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
				// Gets a content URI for the contact
				//mContactUri = ContactsContract.Contacts.getLookupUri(phones.getLong(mIdColumn), phones.getString(mLookupKeyColumn));
				//mBadge.assignContactUri(mContactUri);

				// The column in which to find the thumbnail ID
				int mThumbnailColumn;
				/*
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					mThumbnailColumn =
							phones.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
					// Otherwise, sets the thumbnail column to the _ID column
				} else
				*/
				{
					mThumbnailColumn = mIdColumn;
				}
				mThumbnailUri = phones.getString(mThumbnailColumn);

				/*
				if(mThumbnailUri != null) {
					// Decodes the thumbnail file to a Bitmap.
					// Bitmap mThumbnail = loadContactPhotoThumbnail(mThumbnailUri);
					// mBadge.setImageBitmap(mThumbnail);
				}
				*/

				break;

			}

		}

		if(! found_id) updateContactTable(String.format("%d", c_id));

		phones.close();

		//return mBadge;
		return mThumbnailUri;
	}

	public boolean addObject(TObjType T, int id, String N, String O, GregorianCalendar D, long c_index)
	{
		if(N.isEmpty() || O.isEmpty())return false;
		//if (D == invalidDate) return false;	// TODO: to be implemented
		switch(T){
			case toPre:
				if(Parsing){
					TObj temp = new TObj();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					temp.ContactIndex = c_index;
					Lent.add(temp);
				}
				else{
					if(! tableExists(database, "Loans"))
						database.execSQL(constants.cs_loans);
					ContentValues cv = new ContentValues();
					cv.put("name", N);
					cv.put("item",  O);
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);
					else cv.put("alarm", -1);
					cv.put("contact_index", c_index);
					database.insert("Loans", null, cv);
					parseDatabase();
				}
				break;
			case toInP:
				if(Parsing){
					TObj temp = new TObj();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					temp.ContactIndex = c_index;
					Borrowed.add(temp);}
				else{
					if(! tableExists(database, "Borrows"))
						database.execSQL(constants.cs_borrows);
					ContentValues cv = new ContentValues();
					cv.put("name", N);
					cv.put("item",  O);
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);
					else
						cv.put("alarm", -1);
					cv.put("contact_index", c_index);
					database.insert("Borrows", null, cv);
					parseDatabase();
				}
		}
		FileData.Modified=true;
		return true;}

	public boolean HasAlarms(){
		boolean result = false;
		int i;
		GregorianCalendar checkdate = new GregorianCalendar();
		checkdate.add(Calendar.YEAR, 90);
		for(i = 0; i < NSho/*-1*/; i++)if(ShopItems.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		if(result) return true;
		for(i = 0; i < NLen/*-1*/; i++)if(Lent.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		if(result) return true;
		for(i = 0; i < NBor/*-1*/; i++)if(Borrowed.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		return result;}

	public boolean addValue(TTypeVal T, int id, String N, double V, long c_index)
	{
		if(! Parsing) if(N.isEmpty() || V == 0) return false;
		boolean E = false;
		int x;
		TVal temp = new TVal();
		switch(T){
			/* TODO igor: implement ContactIndex when merging former omb31core functions
			case tvFou:
				for(x = 0; x < NFun; x++)if(Funds.get(x).Name.toString() == N){
					omb_library.Error(11, N);
					return false;}
				if(Parsing){
					TVal last = new TVal();
					last.Id = id;
					last.Name = N;
					last.Value = V;
					Funds.add(NFun, last);
					//NFun++;
				}
				else{
					if(! tableExists(database, "Funds"))
						database.execSQL(constants.cs_funds);

					ContentValues cv = new ContentValues();

					//cv.put("id", "NULL");
					cv.put("name", N);
					cv.put("value", V);
					database.insert("Funds", null, cv);
				}
				break;
			*/
			case tvCre:
				for(x = 0; x < NCre; x++)if(Credits.get(x).Name.compareToIgnoreCase(N) == 0){
					Credits.get(x).Value += V;
					changeFundValue(TTypeVal.tvCre, Credits.get(x).Id, Credits.get(x).Value);
					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						temp.ContactIndex = c_index;
						Credits.add(NCre, temp);
						//NCre++;
					}
					else{
						if(! tableExists(database, "Credits"))
							database.execSQL(constants.cs_credits);
						ContentValues cv = new ContentValues();
						cv.put("name", N);
						cv.put("value", V);
						cv.put("contact_index", c_index);
						database.insert("Credits", null, cv);
					}
				}
				break;
			case tvDeb:
				for(x = 0; x < NDeb; x++)if(Debts.get(x).Name.compareToIgnoreCase(N) == 0){
					Debts.get(x).Value += V;
					changeFundValue(TTypeVal.tvDeb, Debts.get(x).Id, Debts.get(x).Value);
					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						temp.ContactIndex = c_index;
						Debts.add(NDeb, temp);
						//NDeb++;
					}
					else{
						if(! tableExists(database, "Debts"))
							database.execSQL(constants.cs_debts);
						ContentValues cv = new ContentValues();
						cv.put("name", N);
						cv.put("value", V);
						cv.put("contact_index", c_index);
						database.insert("Debts", null, cv);
					}
				}
				break;
			default:
				return false;}
		if(! Parsing) parseDatabase();
		return true;}

	public /*boolean*/void updateMatters(TOpType T)
	{
		boolean Ex;
		int i;
		int j;
		MattersBuffer.clear();
		for(i = 0; i < Lines.size(); i++)if(Lines.get(i).Type == T){
			Ex=false;
			for(j = 0; j < MattersBuffer.size(); j++)if(MattersBuffer.get(j).equalsIgnoreCase(Lines.get(i).Reason)){
				Ex=true;
				break;}
			if(!Ex)MattersBuffer.add(Lines.get(i).Reason);}
		//MattersBuffer->Sort(); TODO: to be implemented
		//return /*true*/;
	}

	public void  xmlExport(File F1, File F2){
		boolean FirstDate = false;	// set when first date is inserted
		String App;	// line to append

		// path selection
		String document = Opts.getString("GDDoc", "NULL");
		File file = new File(document);
		String dir = file.getParent() + "/" + Opts.getString("GDPrefix", "OpenMoneyBox") + "_" + String.format("%d", FileData.Year);
		dir = dir + "-" + String.format("%02d", (int) FileData.Month +1);
		file = new File (dir);
		if(! file.exists()) file.mkdir();

		// xsl template copy
		String xsltempl;

		xsltempl = frame.getResources().getString(R.string.lang) + "/ombexport.xsl";
		try {
			omb_library.copyAsset(xsltempl, dir + "/ombexport.xsl");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// xml document creation
		int ci;
		file = new File(dir + "/" + Opts.getString("GDPrefix", "OpenMoneyBox") +".xml");
		FileWriter writer = null;
		try {
			writer = new FileWriter(file);
			writer.append("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>\n");
			GregorianCalendar now = new GregorianCalendar();
			writer.append("<!--File Created By OpenMoneyBox 3.3 on " +
					omb_library.omb_DateToStr(now) + " "
					+ omb_library.omb_TimeToStr(now, false) + "-->\n");
			writer.append("<?xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"?>\n");
			writer.append("<groups><headers>\n");
			String Header = frame.getResources().getString(R.string.xml_title);
			now.set(Calendar.MONTH, (int) FileData.Month /*- 1*/);
			writer.append("<title heading=\"" + Header + " \" month=\""
					+ now.getDisplayName(Calendar.MONTH , Calendar.LONG, Locale.getDefault()) + " "
					+ String.format("%d", FileData.Year) + "\" />\n");
			writer.append("</headers><days>\n");
			// line parsing
			for(int i = 0; i < NLin /*-1*/; i++){
				App = "";
				if(IsDate(i)){
					if(! FirstDate) {
						FirstDate = true;
					}
					else {
						writer.append("</day>\n");
					}
					App += "<day date=\"";
					App += Lines.get(i).Date.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault()) + " ";
					App += omb_library.omb_DateToStr(Lines.get(i).Date);
					App += "\" total=\"";
					//cur = Double.parseDouble(Lines.get(i).Value);
					App += Lines.get(i).Value;
					App += "\">\n";
					writer.append(App);}
				else{
					App += "<item time=\"";
					App += omb_library.omb_TimeToStr(Lines.get(i).Time, false);
					App += "\" type=\"";
					switch(Lines.get(i).Type){
						case toGain:
							App += "1";
							break;
						case toExpe:
							App += "2";
							break;
						case toSetCre:
							App += "3";
							break;
						case toRemCre:
							App += "4";
							break;
						case toConCre:
							App += "5";
							break;
						case toSetDeb:
							App += "6";
							break;
						case toRemDeb:
							App += "7";
							break;
						case toConDeb:
							App += "8";
							break;
						case toGetObj:
							App += "9";
							break;
						case toGivObj:
							App += "10";
							break;
						case toLenObj:
							App += "11";
							break;
						case toBakObj:
							App += "12";
							break;
						case toBorObj:
							App += "13";
							break;
						case toRetObj:
							App += "14";
							break;
						default:
							App += "0";
							break;}
					App += "\" value=\"";
					/*
					if(Lines.get(i).Type < 9){
						Lines[i].Value.ToDouble(&cur);
						App += FormDigits(cur);}
					else */

					if(Lines.get(i).Value.contains("\"")){
						String vals = Lines.get(i).Value;
						vals = vals.replace("\"", "[OMB_ESCAPEQUOTES]");	// intermediate string to be processed by SubstSpecialChars
						App += vals;
					}
					else App += Lines.get(i).Value;

					App += "\" reason=\"";

					if(Lines.get(i).Reason.contains("\"")){
						String reas = Lines.get(i).Reason;
						reas = reas.replace("\"", "[OMB_ESCAPEQUOTES]");	// intermediate string to be processed by SubstSpecialChars
						App += reas;
					}
					else App += Lines.get(i).Reason;

					App +=  "\" category=\"";
					App = SubstSpecialChars(App);
					writer.append(App);
					ci = (int) Lines.get(i).CategoryIndex;
					if(ci >= 0){
						for(int j = 0; j < NCat; j++) if(CategoryDB.get(j).Id == ci){
							writer.append(CategoryDB.get(j).Name);
							break;}

					}
					//writer.append(CategoryDB.get(ci).Name);
					writer.append("\"/>");
				}}

			// file completion
			writer.append("</day></days></groups>\n");
			//file->Write(wxTextFileType_Unix,wxConvUTF8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// file closure
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Logo picture copy
		try {
			omb_library.copyAsset("logo.png", dir + "/logo.png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Chart copy
		if(F1 != null) try {
			omb_library.copyFile(F1, dir + "/chart1.png");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if(F2 != null) try {
			omb_library.copyFile(F2, dir + "/chart2.png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// ::wxEndBusyCursor();
	}

	private boolean IsDate(int R)
	{
		return Lines.get(R).IsDate;
	}

	public int checkShopList(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NSho; i++)
			if(ShopItems.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	public int checkLentObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NLen; i++)
			if(Lent.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	public int checkBorrowedObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NBor; i++)
			if(Borrowed.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	String getKey(boolean archive){
		String pwd ;

		try {
			if (archive) pwd = Opts.getString("key_archive", "");
			else pwd = Opts.getString("key_main", "");
		}
		catch(Exception e){
			return "";
		}

		return pwd;
	}

}
