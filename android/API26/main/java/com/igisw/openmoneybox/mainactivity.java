/* *************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-05-26
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.DigitsKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class mainactivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener,
		DashboardFragment.OnFragmentInteractionListener,
		FundsFragment.OnFragmentInteractionListener,
		CreditsFragment.OnFragmentInteractionListener,
		DebtsFragment.OnFragmentInteractionListener,
		ObjectsFragment.OnFragmentInteractionListener,
		ReportFragment.OnFragmentInteractionListener,
		ShoplistFragment.OnFragmentInteractionListener,
		ChartFragment.OnFragmentInteractionListener,
		MapFragment.OnFragmentInteractionListener{

	private final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;
	private final int ASK_LOCATION_PERMISSION = 103;

	private boolean HasAlarms = false;

	//private float lastX = -1;

	public omb33core Data;
	private String document;
	private String tmp_string;
	public SharedPreferences Opts;
	private Handler timer;

	// added for v3.2.1.1
	public double LastLatitude = constants.ombInvalidLatitude;
	public double LastLongitude = constants.ombInvalidLongitude;
	private boolean LocationPermission = false;

	private int fragmentIndex;
	private boolean viewIsAtHome;
	private FundsFragment FunFragment;
	private CreditsFragment CreFragment;
	private DebtsFragment DebFragment;
	private ObjectsFragment ObjFragment;
	private ShoplistFragment ShoFragment;
	private ChartFragment ChaFragment;
	//________________________

	//added for v2.2.2.3
	private final String dummyId = "some_channel_id";
	private final String alarmId = "some_other_channel_id";
	private NotificationManager notificationManager;
	//________________________

	private LocationManager mLocationManager;
	private final LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(final Location location) {
			LastLatitude = location.getLatitude();
			LastLongitude = location.getLongitude();
		}

		@Override
		public void onProviderDisabled(String provider){

		}

		@Override
		public void onProviderEnabled(String provider){

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras){

		}
	};

	@SuppressLint("DefaultLocale")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// String model = android.os.Build.MODEL;	// Development model: "FRD-L09"
		// float density = getResources().getDisplayMetrics().density;	// http://stackoverflow.com/questions/5099550/how-to-check-an-android-device-is-hdpi-screen-or-mdpi-screen

		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		Opts = PreferenceManager.getDefaultSharedPreferences(this);

		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			navigationView.setBackgroundColor(0xff000000);
			navigationView.setItemIconTintList(getResources().getColorStateList(R.color.state_list));
			navigationView.setItemTextColor(getResources().getColorStateList(R.color.state_list));
		}
		else this.setTheme(R.style.LightTheme);

		new SimpleEula(this).show();

		// Check permissions
		// Write external SD permission
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
				!= PackageManager.PERMISSION_GRANTED) {

			// Request the permission.
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
			// MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
			// app-defined int constant. The callback method gets the
			// result of the request.
			return;
		}
		if(Opts.getBoolean("GSavePositions", false)){
			LocationPermission = (ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION)
				== PackageManager.PERMISSION_GRANTED) ||
				(ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_COARSE_LOCATION)
				== PackageManager.PERMISSION_GRANTED);
			if(! LocationPermission){
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
								Manifest.permission.ACCESS_COARSE_LOCATION},
						ASK_LOCATION_PERMISSION);

			}

		}

		notificationManager =
			(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			createNotificationChannels();
		}

		//boolean load_direct_doc = true;
		if(savedInstanceState == null) displayView(R.id.nav_dashboard);
		else {
			/*
			// Restore temporary document
			if(savedInstanceState.getBoolean("modified")){
				Data = new TData(this.getCacheDir() + "/temp.omb", this, null);
				Data.FileData.Modified = true;
				showF();
				load_direct_doc = false;
			}
			*/

			// Restore active fragment
			displayView(savedInstanceState.getInt("fragindex"));
		}

		document = Opts.getString("GDDoc", "");

		// Load sqlcipher libs
		SQLiteDatabase.loadLibs(this);
		if(document.isEmpty())
		{
			// Browse for file or launch wizard
			startActivityForResult(new Intent(getApplicationContext(), documentselection.class), 100);
		}
		else
		{
			/*if(load_direct_doc)*/ openDocument();

			Bundle bundle = getIntent().getExtras();
		    if(bundle != null){
	
				int kind = bundle.getInt("kind");
				int index = bundle.getInt("index");
				boolean snooze_all = bundle.getBoolean("snooze_all");
				int year = bundle.getInt("year");
				int month = bundle.getInt("month");
				int day = bundle.getInt("day");

				ContentValues cv = new ContentValues();

				//int i;
				GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
				cal.set(year, month, day);
				
				switch(kind){
					case 1:
						Data.Lent.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Loans", cv, "id = " + String.format("%d", Data.Lent.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 1, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
					case 2:
						Data.Borrowed.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Borrows", cv, "id = " + String.format("%d", Data.Borrowed.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 2, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
					case 3:
						Data.ShopItems.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Shoplist", cv, "id = " + String.format("%d", Data.ShopItems.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 3, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
				}
				if(Data.FileData.Modified)displayView(R.id.nav_dashboard);

		    }
		    /*
			// Start timer to check alarms
		    HasAlarms = Data.HasAlarms();
			timer = new Handler();
			if(HasAlarms) new Thread(new Task()).start();
			*/
		}

		if(Opts.getBoolean("GSavePositions", false)) {
			if (LocationPermission) {
				mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, /*LOCATION_REFRESH_TIME*/ 60000,
					/*LOCATION_REFRESH_DISTANCE*/10, mLocationListener);

				Location lloc = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if(lloc != null) {
					LastLatitude = lloc.getLatitude();
					LastLongitude = lloc.getLongitude();
				}
			}
		}

		// Check if launched from shortcut
        switch (getIntent().getAction()) {
            case "EXPENSE":
                expenseClick(null);
                break;
            case "PROFIT":
                profitClick(null);
                break;
            case "GROCERY":
                displayView(R.id.nav_shoplist);
                break;
        }

	}

	@Override
	protected void onStart(){
		super.onStart();

		if(Data != null)
			if(! Data.successfulKey) insertPass(false);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.mainmenu, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		
		Intent browserIntent;
		
	    switch (item.getItemId()) {
	    	case R.id.editcategories:
	    		EditCategoriesClick();
	    		return true;
	    
	        case R.id.options:
	        	startActivity(new Intent(this, mainoptions.class));
	            return true;

	        case R.id.setTotal:
	        	SetTotalClick();
	        	return true;
	            
	        case R.id.defaultFund:
	        	DefaultFundClick();
	        	return true;

 	        case R.id.about:
				about.Show(mainactivity.this);
	            return true;
	            
 	        case R.id.help:
 	        	String guideAddress = "http://igisw-bilancio.sourceforge.net/android/guide/33/" + getResources().getString(R.string.lang) + "/help/openmoneybox-android.html";
 	        	browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(guideAddress));
				startActivity(browserIntent);
				return true;

 	        case R.id.donate:
 	        	String donateAddress = "http://igisw-bilancio.sourceforge.net/donation.html";
 	        	browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(donateAddress));
				//browserIntent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
				startActivity(browserIntent);
				return true;
				
 	        case R.id.bugReport:
 	        	
 	        	String bugAddress = "https://bugs.launchpad.net/bilancio";
 	        	browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bugAddress));
				startActivity(browserIntent);
				return true;

 	        case R.id.exportArchive:
 	        	File fMaster = this.getDatabasePath(constants.archive_name);
 	        	if(fMaster.exists()){
 	        		String dest = Data.FileData.FileName.getParent();
 	        		dest = dest + "/" + constants.archive_name;
 	        		try {
						omb_library.copyFile(fMaster, dest);
					} catch (IOException e) {
						// TODO Catch block automatically generated
						e.printStackTrace();
					}
 	        		Toast.makeText(getApplicationContext(), getResources().getString(R.string.export_archive_complete), Toast.LENGTH_LONG).show();

 	        	}
 	        	return true;

			/*
			case R.id.changePwd:
 	        	modPass();
 	        	return true;
			*/

	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	/*
	public void saveClick (View view){

		if(Data.FileData.Modified) Data.SaveToFile(false, document);
		return;
		
	}
	*/
	
	/*
	@Override
	public boolean onKeyDown (int keyCode, KeyEvent event){
		return super.onKeyDown(keyCode, event);
	}
	*/

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else if (viewIsAtHome) {
			if(Data != null) {
				HasAlarms = Data.HasAlarms();

				if (Data.FileData.Modified) {
					AlertDialog.Builder alert = new AlertDialog.Builder(this);

					alert.setTitle(getResources().getString(R.string.app_name));
					alert.setMessage(getResources().getString(R.string.file_changed));

					alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							//noinspection SyntaxError
							Data.database.execSQL("RELEASE rollback;");
							Data.FileData.Modified = false;
							stopLocation();
							if (HasAlarms) moveTaskToBack(true);
							else {
								Data.database.close();
								finish();
							}
						}
					});

					alert.setNegativeButton(getResources().getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Data.FileData.Modified = false;
							stopLocation();
							if (HasAlarms) moveTaskToBack(true);
							else {
								onBackPressed();
								Data.database.close();
								finish();
							}
						}
					});

					alert.setNeutralButton(getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							//
						}
					});
					alert.show();
				}
				else {
					Data.database.close();
					stopLocation();
					super.onBackPressed();
				}

				// Start timer to check alarms
				if(HasAlarms) {

					if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
						dummyNotification();

					timer = new Handler();
					new Thread(new Task()).start();

					//moveTaskToBack(true);
				}
			}
			else{
				//Data.database.close();
				super.onBackPressed();
			}
		}
		else { //if the current view is not the Dashboard fragment
			displayView(R.id.nav_dashboard); //display the Dashboard fragment
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	   super.onSaveInstanceState(outState);
	   
		if(! document.isEmpty()){

			// Save active fragment
			outState.putInt("fragindex", fragmentIndex);
	   }
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(! isFinishing())
			Data.database.close();	// close the database on orientation change
	}

	private void operation(int Op){
		int i;
		if(Op == 0 || Op > 2) return;

		Bundle bundle = new Bundle();
		Intent intent = new Intent(this, operation.class);
		
		if(Op == 1){
			bundle.putBoolean("change_labels", false);
			Data.updateMatters(omb31core.TOpType.toGain);
		}
		else{
			bundle.putBoolean("change_labels", true);
			Data.updateMatters(omb31core.TOpType.toExpe);
		}
		bundle.putStringArrayList("matters", Data.MattersBuffer);

		// Parse funds
		ArrayList<String> funds = new ArrayList<>();
		for(i = 0; i < Data.NFun; i++)funds.add(Data.Funds.get(i).Name);
		bundle.putStringArrayList("funds", funds);
		
		// Default fund index
		for(i = 0; i < funds.size(); i++)if(funds.get(i).compareToIgnoreCase(Data.FileData.DefFund) == 0){
			bundle.putInt("defaultfundindex", i);
			break;}
		
		// Parse categories
		ArrayList<String> categories = new ArrayList<>();
		categories.add("-");
		for(i = 0; i < Data.NCat; i++)categories.add(Data.CategoryDB.get(i).Name);
		bundle.putStringArrayList("categories", categories);
		
		intent.putExtras(bundle);
		
		if(Op == 1) startActivityForResult(intent, 1);
		else startActivityForResult(intent, 2);

	}

	public void profitClick(View view){
		operation(1);
	}

	public void expenseClick(View view){
		operation(2);
	}

	@Override
    public void onActivityResult(int requestCode,int resultCode, Intent data)
    {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode != RESULT_OK) return;
		Bundle bundle = null;
		if(requestCode != 100) bundle = data.getExtras();
		
		int IDef = -1, IExi = -1, c_id;
		double value, Fon = -1, Def = -1, Val, Tot1, Fou = -1;
		String obj, per;
		GregorianCalendar Alm;
		
		switch(requestCode){
			case 100:	// Browse document
				document = Opts.getString("GDDoc", "NULL");
				if(! document.isEmpty()) openDocument();
				break;
	
			case 1:	// Gain
			case 2:	// Expense
				omb_library.appContext = getApplicationContext();
				
				int i;
				double Tot;
				String Fund = null, OpValue;

				long currencyIndex;
				double currencyRate;
				String currencySymbol;

				String SelFund = bundle.getString("fund");
				Val = bundle.getDouble("value");

				currencyIndex = bundle.getLong("currency", -1);
				currencyRate = bundle.getDouble("rate", 1);
				currencySymbol = bundle.getString("symbol", "");

				if(currencyIndex == 1)
					Val *= currencyRate;

				OpValue = omb_library.FormDigits(Val, true);
				for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(SelFund) == 0){
					Fund = Data.Funds.get(i).Name;
					Fon = Data.Funds.get(i).Value;
					break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				if(Fon == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}

				switch(requestCode){
					case 1:
						if(LocationPermission)
							Data.addOper(-1, Data.Day, new GregorianCalendar(),
									omb31core.TOpType.toGain, OpValue, bundle.getString("matter"),
									(long) bundle.getInt("category"), -1, true,
									LastLatitude, LastLongitude,
									currencyIndex, currencyRate, currencySymbol);
						else
							Data.addOper(-1, Data.Day, new GregorianCalendar(),
									omb31core.TOpType.toGain, OpValue, bundle.getString("matter"),
									(long) bundle.getInt("category"), -1, false,
									-1, -1,
									currencyIndex, currencyRate, currencySymbol);
						for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareTo(Fund) == 0){
							Data.Funds.get(i).Value += Val;
							Data.changeFundValue(omb31core.TTypeVal.tvFou, Data.Funds.get(i).Id, Data.Funds.get(i).Value);
							break;}
						break;
					case 2:
						if(Val > Fon){
							omb_library.Error(13, "");
							return;}
						String MatValue = bundle.getString("matter");
						if(LocationPermission)
							Data.addOper(-1, Data.Day, new GregorianCalendar(),
									omb31core.TOpType.toExpe, OpValue, MatValue,
									(long) bundle.getInt("category"), -1, true,
									LastLatitude, LastLongitude,
									currencyIndex, currencyRate, currencySymbol);
						else
							Data.addOper(-1, Data.Day, new GregorianCalendar(),
									omb31core.TOpType.toExpe, OpValue, MatValue,
									(long) bundle.getInt("category"), -1, false,
									-1, -1,
									currencyIndex, currencyRate, currencySymbol);
						for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareTo(Fund) == 0){
							Data.Funds.get(i).Value -= Val;
							Data.changeFundValue(omb31core.TTypeVal.tvFou, Data.Funds.get(i).Id, Data.Funds.get(i).Value);
							break;}
						//Data.SetTot(TData.TTypeVal.tvFou, Data.GetTot(TData.TTypeVal.tvFou) - Val);
						if((Fon - Val == 0) && (! Fund.equals(Data.FileData.DefFund))){
							String Msg = String.format(getResources().getString(R.string.fund_exhaust), SelFund);
							tmp_string = Fund;
							
					 		AlertDialog.Builder alert = new AlertDialog.Builder(this);

							alert.setTitle(getResources().getString(R.string.app_name));
							alert.setMessage(Msg);

							// Set an EditText view to get user input 
							//final EditText input = new EditText(this);
							//input.setTransformationMethod(PasswordTransformationMethod.getInstance());
							//alert.setView(input);

							alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									for(int i = 0;i < Data.NFun /*-1*/; i++)if(Data.Funds.get(i).Name.compareTo(/*Fund*/tmp_string) == 0){
										Data.delValue(omb31core.TTypeVal.tvFou, i);
										break;}
							  }
							});

							alert.setNegativeButton(getResources().getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
							  public void onClick(DialogInterface dialog, int whichButton) {
							    // Canceled.
								//return false;
							  }
							});
							
							alert.show();

						}
						remove_obtained_shopitem(MatValue.toLowerCase());
				}
				//showF();
				displayView(R.id.nav_dashboard);

				break;
			
			case 3: // Categories edited
				if(bundle.getBoolean("modified")){
					ArrayList<String> CategoryList = bundle.getStringArrayList("categories");

					Data.updateCategories(CategoryList);
					
					Data.FileData.Modified = true;
					//showF();
					displayView(fragmentIndex);
				}
				break;
			
			case 4:  // Add fund
				//int Ch = 0;
				Def = 0;
				String fund = bundle.getString("fund");
				value = bundle.getDouble("value");
				
				//Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				for(i = 0; i < Data.NFun ; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(fund) == 0){
					omb_library.Error(11, fund);
					return;
				}
				else if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0)Def = Data.Funds.get(i).Value;

				if(value > Def){
					omb_library.Error(12, "");
					return;}

				Data.setDefaultFundValue(Def - value);

				if(! Data.addValue(omb31core.TTypeVal.tvFou, -1, fund, value)){
					omb_library.Error(10, "");
					return;}
				displayView(R.id.nav_funds);
				break;
				
			case 5:	// add credit
				String credit = bundle.getString("credit");
				Val = bundle.getDouble("value");
				for(i = 0; i < Data.NFun ; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				Tot1 = Data.GetTot(omb31core.TTypeVal.tvCre);
				if((Def == -1) || (Tot == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(Def < Val){
					omb_library.Error(19, "");
					return;}

				c_id = getContactIndex(bundle);

				Data.addValue(omb31core.TTypeVal.tvCre, -1, credit, Val, c_id);
				Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Val);
				if(LocationPermission)
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toSetCre, omb_library.FormDigits(Val, true),
							credit, -1, c_id, true, LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toSetCre, omb_library.FormDigits(Val, true),
							credit, -1, c_id, false, -1, -1,
							-1, 1, "");
				displayView(R.id.nav_credits);
				break;
			
			case 6:	// remove credit
				for(i = 0; i < Data.NFun /*-1*/; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				for(i = 0; i < Data.NCre /*-1*/; i++)
					if(Data.Credits.get(i).Name.compareToIgnoreCase(Data.Credits.get(CreFragment.credit_pos).Name) == 0){
						IExi = /*i*/ Data.Credits.get(i).Id;
						Fou = Data.Credits.get(i).Value;
						break;}
				Tot1 = Data.GetTot(omb31core.TTypeVal.tvCre);
				if((Def == -1) || (Tot == -1) || (Fou == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemCre, omb_library.FormDigits(Fou, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemCre, omb_library.FormDigits(Fou, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb31core.TTypeVal.tvCre, IExi);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Fou);
				}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Fou){
						omb_library.Error(21, getResources().getString(R.string.credit_term));
						return;}
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLatitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb31core.TTypeVal.tvCre, IExi, Fou - Par);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Par);
				}
				Data.parseDatabase();
				//showF();
				displayView(R.id.nav_credits);
				break;
				
			case 7:	// Condone credit
				int ICre = -1;
				double Cre = -1;
				for(i = 0; i < Data.NCre /*-1*/; i++)
					if(Data.Credits.get(i).Name.compareToIgnoreCase(Data.Credits.get(CreFragment.credit_pos).Name) == 0){
						ICre = /*i*/ Data.Credits.get(i).Id;
						Cre = Data.Credits.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvCre);
				if(Cre == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConCre, omb_library.FormDigits(Cre, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConCre, omb_library.FormDigits(Cre, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb31core.TTypeVal.tvCre, ICre);}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Cre){
						omb_library.Error(21, getResources().getString(R.string.credit_term));
						return;}
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb31core.TTypeVal.tvCre, ICre, Cre - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_credits);
				break;
				
			case 8:	// add debt
				String debt = bundle.getString("debt");
				Val = bundle.getDouble("value");
				for(i = 0; i < Data.NFun; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				Tot1 = Data.GetTot(omb31core.TTypeVal.tvDeb);
				if((Def == -1) || (Tot == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}

				c_id = getContactIndex(bundle);

				Data.addValue(omb31core.TTypeVal.tvDeb, -1, debt, Val, c_id);
				Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Val);
				if(LocationPermission)
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toSetDeb, omb_library.FormDigits(Val, true),
							debt, -1, c_id, true, LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toSetDeb, omb_library.FormDigits(Val, true),
							debt, -1, c_id, false, -1, -1,
							-1, 1, "");
				displayView(R.id.nav_debts);
				break;
			
			case 9:	// remove debt
				for(i = 0; i < Data.NFun; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
				for(i = 0; i < Data.NDeb; i++)
					if(Data.Debts.get(i).Name.compareToIgnoreCase(Data.Debts.get(DebFragment.debt_pos).Name) == 0){
						IExi = /*i*/ Data.Debts.get(i).Id;
						Fou = Data.Debts.get(i).Value;
						break;}
				Tot1 = Data.GetTot(omb31core.TTypeVal.tvDeb);
				if((Def == -1) || (Tot == -1) || (Fou == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(bundle.getBoolean("total")){
				    if(Fou > Def){
						omb_library.Error(24, "");
						return;}
				    if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemDeb, omb_library.FormDigits(Fou, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
				    else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemDeb, omb_library.FormDigits(Fou, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb31core.TTypeVal.tvDeb, IExi);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Fou);
				}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Fou){
						omb_library.Error(21, getResources().getString(R.string.debt_term));
						return;}
					if(Par > Def){
						omb_library.Error(24, "");
						return;}
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toRemDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb31core.TTypeVal.tvDeb, IExi, Fou - Par);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_debts);
				break;
				
			case 10:	// Condone debt
				int IDeb = -1;
				double Deb = -1;
				for(i = 0; i < Data.NDeb; i++)
					if(Data.Debts.get(i).Name.compareToIgnoreCase(Data.Debts.get(DebFragment.debt_pos).Name) == 0){
						IDeb = /*i*/ Data.Debts.get(i).Id;
						Deb = Data.Debts.get(i).Value;
						break;}
				Tot = Data.GetTot(omb31core.TTypeVal.tvDeb);
				if(Deb == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConDeb, omb_library.FormDigits(Deb, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConDeb, omb_library.FormDigits(Deb, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb31core.TTypeVal.tvDeb, IDeb);}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Deb){
						omb_library.Error(21, getResources().getString(R.string.debt_term));
						return;}
					if(LocationPermission)
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.addOper(-1, Data.Day, new GregorianCalendar(),
								omb31core.TOpType.toConDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb31core.TTypeVal.tvDeb, IDeb, Deb - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_debts);
				break;
				
			case 11:	// Get object
            case 12:	// Give object
                omb33core.TOpType objType;
                if(requestCode == 11) objType = omb31core.TOpType.toGetObj;
                else objType = omb31core.TOpType.toGivObj;

				c_id = getContactIndex(bundle);

				obj = bundle.getString("object");
				if(LocationPermission)
					Data.addOper(-1, Data.Day, new GregorianCalendar(), objType, obj,
						bundle.getString("person"), -1, c_id, true, LastLatitude,
						LastLongitude,
							-1, 1, "");
				else
					Data.addOper(-1, Data.Day, new GregorianCalendar(), objType, obj,
							bundle.getString("person"), -1, c_id, false, -1,
							-1,
							-1, 1, "");
				if(requestCode == 11) remove_obtained_shopitem(obj.toLowerCase());
				displayView(R.id.nav_dashboard);
				break;

			case 13:	// Lend object
				c_id = getContactIndex(bundle);
				obj = bundle.getString("object");
				per = bundle.getString("person");
				Alm = (GregorianCalendar) GregorianCalendar.getInstance();
				Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
				Data.addObject(omb33core.TObjType.toPre, -1, per, obj, Alm, c_id);
				if(LocationPermission)
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb33core.TOpType.toLenObj, obj, per, -1, c_id, true,
							LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb33core.TOpType.toLenObj, obj, per, -1, c_id, false,
							-1, -1,
							-1, 1, "");
				displayView(R.id.nav_objects);
				break;

			case 14:	// Borrow object
				c_id = getContactIndex(bundle);
				obj = bundle.getString("object");
				per = bundle.getString("person");
				Alm = (GregorianCalendar) GregorianCalendar.getInstance();
				Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
				Data.addObject(omb31core.TObjType.toInP, -1, per, obj, Alm, c_id);
				if(LocationPermission)
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toBorObj, obj, per, -1, c_id, true,
							LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.addOper(-1, Data.Day, new GregorianCalendar(),
							omb31core.TOpType.toBorObj, obj, per, -1, c_id, false,
							-1, -1,
							-1, 1, "");
				displayView(R.id.nav_objects);
				break;

			case 15:	// Add shop item
				obj = bundle.getString("object");
				boolean item_new = true;
				for(i = 0; i < Data.NSho; i++){
					if(Data.ShopItems.get(i).Name.compareToIgnoreCase(obj) == 0){
						omb_library.Error(48, obj);
						item_new = false;
						break;
					}
				}
				if(item_new){
					Alm = (GregorianCalendar) GregorianCalendar.getInstance();
					Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
					Data.addShopItem(-1, obj, Alm);
					displayView(R.id.nav_shoplist);
				}
				break;
				
			default:
		}

    }

	private void EditCategoriesClick(){
		Bundle bundle = new Bundle();
		
		Intent intent = new Intent(this, editcategories.class);
		
		ArrayList<String> cats = new ArrayList<>();
		cats.clear();
		for(int i = 0; i < Data.NCat; i++) cats.add(Data.CategoryDB.get(i).Name);
		bundle.putStringArrayList("categories", cats);
		
		intent.putExtras(bundle);
		
		startActivityForResult(intent, 3);

	}

	private void FundOperation(int Op){
		if(Op == 0 || Op > 3) return;
		int i;
		double Fon /*= -1*/;
		if(Op > 1 && Data.NFun < 2){
			if(Op == 2) omb_library.Error(8, null);
			else omb_library.Error(9, null);
			return;}

		switch(Op){
		case 1:
			//Bundle bundle = new Bundle();
			Intent intent = new Intent(this, fund_add.class);
			startActivityForResult(intent, 4);
			break;
		case 2:
			Fon = Data.Funds.get(FunFragment.fund_pos).Value;
			int id = Data.Funds.get(FunFragment.fund_pos).Id;
			
			for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
				Data.setDefaultFundValue(Data.Funds.get(i).Value + Fon);
				break;}

			Data.delValue(omb31core.TTypeVal.tvFou, /*fund_pos*/id);
			
			//showF();
			displayView(R.id.nav_funds);
			break;
		case 3:
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle(getResources().getString(R.string.app_name));
			alert.setMessage(String.format(this.getResources().getString(R.string.fund_reset),
					Data.Funds.get(FunFragment.fund_pos).Name));

			// Set an EditText view to get user input 
			final EditText input = new EditText(this);
			input.setKeyListener(new DigitsKeyListener(false, true));
			input.setText(omb_library.FormDigits(Data.Funds.get(FunFragment.fund_pos).Value, true));
			alert.setView(input);

			alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					
					double Fon = Data.Funds.get(FunFragment.fund_pos).Value;
					double NFun = Double.parseDouble(input.getText().toString());
					double Tot = Data.GetTot(omb31core.TTypeVal.tvFou);
					
					for(int i = 0; i < Data.NFun /*- 1*/; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						double Def = Data.Funds.get(i).Value;
						if(NFun < Fon){
							//Data.Funds.get(i).Value = Def + Fon - NFun;
							Data.setDefaultFundValue(Def + Fon - NFun);
						}
						else{
							if(Tot < (NFun - Fon)){
								omb_library.Error(13, "");
								return;}
							if(Def < (NFun - Fon)){
								omb_library.Error(24, "");
								return;}
		                    //Data.Funds.get(i).Value = Def - NFun + Fon;
							Data.setDefaultFundValue(Def - NFun + Fon);
						}}
					else if(i == FunFragment.fund_pos) {
						Data.Funds.get(i).Value = NFun;
						Data.changeFundValue(omb31core.TTypeVal.tvFou, Data.Funds.get(i).Id, Data.Funds.get(i).Value);
					}
					Data.FileData.Modified = true;
					//showF();
					displayView(R.id.nav_funds);
				}
			});

			alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
				//return false;
			  }
			});
			
			alert.show();
			//if(Data.FileData.Modified) ShowF();
			
			break;
		}
		
	}

	public void NewFundClick(View view){
		FundOperation(1);}

	public void RemoveFundClick(View view){
		FundOperation(2);}

	public void ResetFundClick(View view){
		FundOperation(3);}

	private void SetTotalClick(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(getResources().getString(R.string.app_name));
		alert.setMessage(this.getResources().getString(R.string.fund_settotal));

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		input.setKeyListener(new DigitsKeyListener(false, true));
		input.setText(omb_library.FormDigits(Data.GetTot(omb31core.TTypeVal.tvFou), true));
		alert.setView(input);

		alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				
				int i, IP = -1;
				double OldTot, Tot, Pre = -1;	// OldTot: previous total value
				// Tot: new total value
				// Pre: Default fund value
				OldTot = Data.GetTot(omb31core.TTypeVal.tvFou);
				String curstr = input.getText().toString();
				Tot = Double.parseDouble(curstr);
				
				omb_library.appContext = getApplicationContext();
				
				if(OldTot > Tot){
					omb_library.Error(17, "");
					return;}
				for(i = 0; i < Data.NFun /*- 1*/; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
					Pre = Data.Funds.get(i).Value;
					IP = i;
					break;}
				if(IP > -1){
					Data.Funds.get(IP).Value = Pre + Tot - OldTot;
					Data.changeFundValue(omb31core.TTypeVal.tvFou, Data.Funds.get(IP).Id, Data.Funds.get(IP).Value);
				}
				else{
					String def = getResources().getString(R.string.default_name);
					Data.addValue(omb31core.TTypeVal.tvFou, -1, def, Tot);
					//Data.FileData.DefFund = def;
					Data.setDefaultFund(def);
				}
				//Data.SetTot(TData.TTypeVal.tvFou, Tot);
				Data.FileData.Modified = true;
				//showF();
				displayView(fragmentIndex);
			}
		});

		alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
			//return false;
		  }
		});

		alert.show();
		
	}

	private void DefaultFundClick(){
		int i;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setTitle(getResources().getString(R.string.app_name));
        builder.setTitle(this.getResources().getString(R.string.fund_default_choose));
        
        ArrayList<String> list = new ArrayList<>();
        for(i = 0; i < Data.NFun; i++) list.add(Data.Funds.get(i).Name);
        final CharSequence[] items = list.toArray(new CharSequence[list.size()]);
        //items.ch

        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
        		Data.setDefaultFund(items[item].toString());
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

	}

	public void NewCreditClick(View view){
		Intent intent = new Intent(this, credit_add.class);
		startActivityForResult(intent, 5);
	}

	public void RemoveCreditClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 0);
		bundle.putDouble("max_value", Data.Credits.get(CreFragment.credit_pos).Value);
		Intent intent = new Intent(this, remcreddeb.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 6);
	}
	
	public void CondoneCreditClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 1);
		bundle.putDouble("max_value", Data.Credits.get(CreFragment.credit_pos).Value);
		Intent intent = new Intent(this, remcreddeb.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 7);
	}
	
	public void NewDebtClick(View view){

		Intent intent = new Intent(this, debt_add.class);
		startActivityForResult(intent, 8);
	}

	public void RemoveDebtClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 2);
		bundle.putDouble("max_value", Data.Debts.get(DebFragment.debt_pos).Value);
		Intent intent = new Intent(this, remcreddeb.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 9);
	}
	
	public void CondoneDebtClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 3);
		bundle.putDouble("max_value", Data.Debts.get(DebFragment.debt_pos).Value);
		Intent intent = new Intent(this, remcreddeb.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 10);
	}

	public void receivedClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 1);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);
		startActivityForResult(intent, 11);
	}

	public void giftedClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 2);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);
		startActivityForResult(intent, 12);
	}

	public void LendClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 3);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 13);
	}

	public void getBackClick(View view){
		String obj = null;
		String per = null;
		for(int i = 0; i < Data.NLen; i++){
			if(Data.Lent.get(i).Id == ObjFragment.lent_pos){
				obj = Data.Lent.get(i).Object;
				per = Data.Lent.get(i).Name;
				break;
			}
		}
		Data.delObject(omb31core.TObjType.toPre, ObjFragment.lent_pos);
		if(LocationPermission)
			Data.addOper(-1, Data.Day, new GregorianCalendar(), omb31core.TOpType.toBakObj, obj, per,
					-1, -1, true, LastLatitude, LastLongitude,
					-1, 1, "");
		else
			Data.addOper(-1, Data.Day, new GregorianCalendar(), omb31core.TOpType.toBakObj, obj, per,
				-1, -1, false, -1, -1,
					-1, 1, "");
		displayView(R.id.nav_objects);
	}

	public void BorrowClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 4);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 14);
	}

	public void giveBackClick(View view){
		String obj = null;
		String per = null;
		for(int i = 0; i < Data.NBor; i++){
			if(Data.Borrowed.get(i).Id == ObjFragment.borrowed_pos){
				obj = Data.Borrowed.get(i).Object;
				per = Data.Borrowed.get(i).Name;
				break;
			}
		}
		Data.delObject(omb31core.TObjType.toInP, ObjFragment.borrowed_pos);
		if(LocationPermission)
			Data.addOper(-1, Data.Day, new GregorianCalendar(), omb31core.TOpType.toRetObj, obj, per,
					-1, -1, true, LastLatitude, LastLongitude,
					-1, 1, "");
		else
			Data.addOper(-1, Data.Day, new GregorianCalendar(), omb31core.TOpType.toRetObj, obj, per,
					-1, -1, false, -1, -1,
					-1, 1, "");
		displayView(R.id.nav_objects);
	}

	public void addShopItemClick(View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 5);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		startActivityForResult(intent, 15);
	}
	
	public void delShopItemClick(View view){
		int id = Data.ShopItems.get(ShoFragment.shop_pos).Id;
		Data.delShopItem(/*shop_pos*/id);
		//showF();
		displayView(R.id.nav_shoplist);
	}

	private void openDocument(){
		omb_library.appContext = getApplicationContext();

		File f = new File(document);
		if(f.exists()) {
			// Document memory-space creation
			Data = new omb33core(document, this, null); // Progress bar is set after frame creation
			/* TODO: to be implemented
			Data.Progress = frame->Progress;
			frame->UpdateCalendar();
			*/

			Data.frame = this;

		}
		else startActivity(new Intent(this, documentselection.class));

	}
	
	private void remove_obtained_shopitem(String obj){
		for(int i = 0; i < Data.NSho; i++){
			if(obj.compareToIgnoreCase(Data.ShopItems.get(i).Name) == 0){
				String Msg = String.format(getResources().getString(R.string.shoplist_bought), Data.ShopItems.get(i).Name);
				final int tmp_int = /*i*/Data.ShopItems.get(i).Id;
				//shop_pos = i;
				
		 		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		 		alert.setTitle(getResources().getString(R.string.app_name));
				alert.setMessage(Msg);

				// Set an EditText view to get user input 
				//final EditText input = new EditText(this);
				//input.setTransformationMethod(PasswordTransformationMethod.getInstance());
				//alert.setView(input);

				alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Data.delShopItem(tmp_int);
						//showF();
						displayView(fragmentIndex);
				  }
				});

				alert.setNegativeButton(getResources().getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog, int whichButton) {
				    // Canceled.
					//return false;
				  }
				});
				
				alert.show();

			}
		}
		
	}

	private void alarm_notification(int kind, int index, String text)
	{
		Notification.Builder mBuilder =
		        new Notification.Builder(this)
				//.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
		        .setContentTitle(getResources().getString(R.string.alarm))
		        .setContentText(text)
		        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
		        ;
        switch (android.os.Build.VERSION.SDK_INT ) {
			case Build.VERSION_CODES.LOLLIPOP_MR1:
			case Build.VERSION_CODES.M:
				mBuilder.setColor(0xff35ff);
				mBuilder.setSmallIcon(R.drawable.ic_launcher_white);
				break;
			case Build.VERSION_CODES.N:
			case Build.VERSION_CODES.N_MR1:
			case Build.VERSION_CODES.O:
				mBuilder.setColor(0x803f00);
				mBuilder.setSmallIcon(R.drawable.ic_launcher_brown);
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
					mBuilder.setChannelId(alarmId);
				break;
			default:
        		mBuilder.setSmallIcon(R.mipmap.ic_launcher);
		}

		// Creates an explicit intent for an Activity in your app
		Bundle bundle = new Bundle();
		Intent resultIntent = new Intent(this, alarm.class);
		bundle.putInt("kind", kind);
		bundle.putInt("index", index);
		bundle.putString("message", text);
		resultIntent.putExtras(bundle);
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(mainactivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0/*16*/,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		/*
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		*/

		Notification notif = mBuilder.build();
		if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 ) {
			int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

		    if (smallIconViewId != 0) {
		        if (notif.contentIntent != null)
		            notif.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		
		        if (notif.headsUpContentView != null)
		            notif.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		
		        if (notif.bigContentView != null)
		            notif.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		    }
		}

		// mId allows you to update the notification later on.
		notificationManager.notify(10000, notif);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			notificationManager.cancel(1);
	}
	
	class Task implements Runnable {
		@Override
		public void run() {
			if(HasAlarms){
				int mainTimerInt = 600000;
				timer.postDelayed(new Runnable() {
					@Override
					public void run() {
						doChecks(false, -1, null);
					}
				}, mainTimerInt);
			}
		}
	}

	@SuppressLint("DefaultLocale")
	private boolean doChecks(boolean auto, int kind, GregorianCalendar cal)
	{

		//if(isConnected(this)) return false;
		
		int result;
		result = Data.checkLentObjects();
		if(result >= 0){
			if(auto && (kind == 1))
			{
				Data.Lent.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Loans", cv, "id = " + String.format("%d", Data.Lent.get(result).Id), null);
			}
			else alarm_notification(1, result,
					String.format(getResources().getString(R.string.alarm_detail_lent), Data.Lent.get(result).Name)
			);
			return true;
		}

		result = Data.checkBorrowedObjects();
		if(result >= 0){
			if(auto && (kind == 2)) {
				Data.Borrowed.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Borrows", cv, "id = " + String.format("%d", Data.Borrowed.get(result).Id), null);
			}
			else alarm_notification(2, result,
					String.format(getResources().getString(R.string.alarm_detail_borrowed), Data.Borrowed.get(result).Name)
			);
			return true;
		}

		result = Data.checkShopList();
		if(result >= 0){
			if(auto && (kind == 3))
			{
				Data.ShopItems.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Shoplist", cv, "id = " + String.format("%d", Data.ShopItems.get(result).Id), null);
			}
			else alarm_notification(3, result,
					String.format(getResources().getString(R.string.alarm_detail_shoplist), Data.ShopItems.get(result).Name)
			);
			return true;
		}

		return false;
	}

	/*
    private  boolean isConnected(Context context) {
		if(isDebuggable(this)) return false;
        Intent intent = context.registerReceiver(null, new IntentFilter("android.hardware.usb.action.USB_STATE"));
        return intent.getExtras().getBoolean("connected");
    }

	// TODO: comment this routine and calls for release builds
	private boolean isDebuggable(Context ctx)
	{
	    boolean debuggable = false;
	 
	    PackageManager pm = ctx.getPackageManager();
	    try
	    {
	        ApplicationInfo appinfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
	        debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
	    }
	    catch(NameNotFoundException e)
	    {
	       //debuggable variable will remain false
	    }
	     
	    return debuggable;
	}
	*/

	/*
	@Override
	public boolean onTouchEvent(MotionEvent touchevent) {
		switch (touchevent.getAction()) {
			// when user first touches the screen to swap
			case MotionEvent.ACTION_DOWN: {
				lastX = touchevent.getX();
				break;
			}
			case MotionEvent.ACTION_UP: {
				float currentX = touchevent.getX();

				// if left to right swipe on screen
				if (lastX < currentX) {

					switchTabs(true);
				}

				// if right to left swipe on screen
				else if (lastX > currentX) {
					switchTabs(false);
				}

				break;
			}

		}
		return false;
	}
	*/

	/*
	public void switchTabs(boolean direction) {
		if (direction) // true = move left
		{
			if (myTabHost.getCurrentTab() == 0)
				myTabHost.setCurrentTab(myTabHost.getTabWidget().getTabCount() - 1);
			else
				myTabHost.setCurrentTab(myTabHost.getCurrentTab() - 1);
		} else
		// move right
		{
			if (myTabHost.getCurrentTab() != (myTabHost.getTabWidget()
					.getTabCount() - 1))
				myTabHost.setCurrentTab(myTabHost.getCurrentTab() + 1);
			else
				myTabHost.setCurrentTab(0);
		}
	}
	*/

	public void updateObjectRecyclerItem(int item){
		int itemCount = ObjFragment.ov.getChildCount();
		int type = -1;
		recycler_adapter_objects.ObjectViewHolder holder;
		for(int i = 0; i < itemCount; i++){
			holder = (recycler_adapter_objects.ObjectViewHolder) ObjFragment.ov.findViewHolderForAdapterPosition(i);
			if(i == item){
				holder.objectChecked.setChecked(true);
				type = ObjFragment.objects.get(item).type;
			}
			else holder.objectChecked.setChecked(false);
		}

		boolean enabled = (item >= 0);
		if(type == 1){
			ObjFragment.btn_givebackObj.setEnabled(enabled);
			ObjFragment.btn_getbackObj.setEnabled(false);
			if(item >= 0) ObjFragment.borrowed_pos = ObjFragment.objects.get(item).id;
		}
		else{
			ObjFragment.btn_getbackObj.setEnabled(enabled);
			ObjFragment.btn_givebackObj.setEnabled(false);
			if(item >= 0) ObjFragment.lent_pos = ObjFragment.objects.get(item).id;
		}

	}

	public void updateCredDebRecyclerItem(int item){
		RecyclerView view;
		//int tab = myTabHost.getCurrentTab();
		switch(fragmentIndex){
			case R.id.nav_debts:
				view = DebFragment.dv;
				break;
			default:
				view = CreFragment.cv;
		}

		int itemCount = view.getChildCount();
		recycler_adapter_creddeb.ObjectViewHolder holder;
		for(int i = 0; i < itemCount; i++){
			holder = (recycler_adapter_creddeb.ObjectViewHolder) view.findViewHolderForAdapterPosition(i);
			if(i == item) holder.itemChecked.setChecked(true);
			else holder.itemChecked.setChecked(false);
		}

		boolean enabled = (item >= 0);
		switch(fragmentIndex){
			case R.id.nav_debts:
				DebFragment.btn_removeDebt.setEnabled(enabled);
				DebFragment.btn_condoneDebt.setEnabled(enabled);
				if(item >= 0) DebFragment.debt_pos = item;
				break;
			default:
				CreFragment.btn_removeCredit.setEnabled(enabled);
				CreFragment.btn_condoneCredit.setEnabled(enabled);
				if(item >= 0) CreFragment.credit_pos = item;
		}

	}

	private int getContactIndex(Bundle bundle){
		String contact_id = bundle.getString("contact_id");
		int c_id = Integer.parseInt(contact_id);
		String contact_name;
		if(c_id > 0){
			contact_name = bundle.getString("contact_name");
			if(! Data.findContact(contact_id, contact_name)) c_id = -1;
		}
		return c_id;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
										   @NonNull int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
				boolean canUseExternalStorage = false;
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					canUseExternalStorage = true;
				}

				if (!canUseExternalStorage) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_write_needed), Toast.LENGTH_LONG).show();
					finish();
				}
				else
					// user now provided permission
					// perform function for what you want to achieve
					this.recreate();
				break;

			case ASK_LOCATION_PERMISSION:
				if (grantResults.length > 0) {
					boolean gpsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
					boolean coarsePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;

					if ((gpsPermission || coarsePermission)) LocationPermission = true;
					else
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.locations_wont_store_warning), Toast.LENGTH_LONG).show();
				}
				break;

			case constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS:
				boolean canUseContacts = false;
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					canUseContacts = true;
				}

				if (!canUseContacts) {
					Toast.makeText(getApplicationContext(), "Contacts cannot be accessed.", Toast.LENGTH_LONG).show();
					//finish();
				}
				/*
				else {
					// user now provided permission
					// perform function for what you want to achieve
				}
				*/
				break;
		}
	}

	private void displayView(int viewId) {

		// lock orientation if needed to avoid change loss in the database
		// TODO (igor#1#): rework for tablets which have different natural orientation
		if(Data != null) {
			if (Data.FileData.Modified) {
				int orientation; // = this.getRequestedOrientation();
				int rotation = ((WindowManager) this.getSystemService(
						Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
				switch (rotation) {
					case Surface.ROTATION_90:
						orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
						break;
					case Surface.ROTATION_180:
						orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
						break;
					case Surface.ROTATION_270:
						orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
						break;
					case Surface.ROTATION_0:
					default:
						orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
						break;
				}

				this.setRequestedOrientation(orientation);
			} else this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}

		//Fragment fragment = null;
		String title = getString(R.string.app_name);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		switch (viewId) {
			case R.id.nav_dashboard:
				DashboardFragment dashFragment = new DashboardFragment();
				//title  = "_dashboard";

				ft.replace(R.id.content_frame, dashFragment);
				ft.commit();

				viewIsAtHome = true;
				break;
            case R.id.nav_funds:
                FunFragment = new FundsFragment();
                title = getString(R.string.funds);

				ft.replace(R.id.content_frame, FunFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
            case R.id.nav_credits:
                CreFragment = new CreditsFragment();
                title = getString(R.string.credits);

				ft.replace(R.id.content_frame, CreFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
            case R.id.nav_debts:
                DebFragment = new DebtsFragment();
                title = getString(R.string.debts);

				ft.replace(R.id.content_frame, DebFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
            case R.id.nav_objects:
                ObjFragment = new ObjectsFragment();
                title = getString(R.string.objects);

				ft.replace(R.id.content_frame, ObjFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
            case R.id.nav_report:
				ReportFragment repFragment = new ReportFragment();
                title = getString(R.string.report);

				ft.replace(R.id.content_frame, repFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
            case R.id.nav_shoplist:
                ShoFragment = new ShoplistFragment();
                title = getString(R.string.shoplist);

				ft.replace(R.id.content_frame, ShoFragment);
				ft.commit();

				viewIsAtHome = false;
                break;
			case R.id.nav_chart:
				ChaFragment = new ChartFragment();
				title = getString(R.string.charts);

				ft.replace(R.id.content_frame, ChaFragment);
				ft.commit();

				viewIsAtHome = false;
				break;
			case R.id.nav_map:
				com.igisw.openmoneybox.MapFragment mapFragment = new MapFragment();
				title = getString(R.string.navigation_drawer_map);

				ft.replace(R.id.content_frame, mapFragment);
				ft.commit();

				viewIsAtHome = false;
				break;
		}

        /*
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
         */

		fragmentIndex = viewId;

		// set the toolbar title
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(title);
		}

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);

	}

	public void    onFragmentInteraction(Uri uri){
		//We can keep this empty
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		// Handle navigation view item clicks here.
		displayView(item.getItemId());
		return true;
	}

	public void textConvClick(View view){
		//StatusBar->SetStatusText(_("Document conversion i progress..."),0);

		File Ch1 = null, Ch2 = null;

		File outputDir = this.getCacheDir(); // context being the Activity pointer

		try {
			Ch1 = File.createTempFile("prefix", ".omb", outputDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			Ch2 = File.createTempFile("prefix", ".omb", outputDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(ChaFragment != null){
			// TODO implement chart saving when ChaFragment is null
			ChaFragment.saveChart(R.id.fund_graph, Ch1);
			ChaFragment.saveChart(R.id.trend_graph, Ch2);
			Data.xmlExport(Ch1, Ch2);
		}
		else Data.xmlExport(null, null);

		Toast.makeText(getApplicationContext(), getResources().getString(R.string.xml_completed), Toast.LENGTH_LONG).show();
	}

	/*
	public void passwordClick(View view){
		Data.ModPass();
	}
	*/

	@RequiresApi(Build.VERSION_CODES.O)
	private void createNotificationChannels() {
		@SuppressWarnings("unchecked") List<NotificationChannelGroup> notificationChannelGroups = new ArrayList();
		notificationChannelGroups.add(new NotificationChannelGroup("group_one", "Group One"));
		notificationChannelGroups.add(new NotificationChannelGroup("group_two", "Group Two"));
		notificationManager.createNotificationChannelGroups(notificationChannelGroups);

		int importance = NotificationManager.IMPORTANCE_DEFAULT;
		NotificationChannel dummyNotificationChannel = new NotificationChannel(dummyId,
				getResources().getString(R.string.dummy_notification_title), importance);
		NotificationChannel alarmNotificationChannel = new NotificationChannel(alarmId,
				getResources().getString(R.string.notification_channel_alarm), importance);

		/*
		dummyNotificationChannel.enableLights(true);
		dummyNotificationChannel.setLightColor(Color.RED);
		dummyNotificationChannel.enableVibration(true);
		dummyNotificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

		alarmNotificationChannel.enableLights(true);
		alarmNotificationChannel.setLightColor(Color.RED);
		alarmNotificationChannel.enableVibration(true);
		alarmNotificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
		*/

		dummyNotificationChannel.setGroup(notificationChannelGroups.get(0).getId());
		alarmNotificationChannel.setGroup(notificationChannelGroups.get(1).getId());
		notificationManager.createNotificationChannel(dummyNotificationChannel);
		notificationManager.createNotificationChannel(alarmNotificationChannel);
	}

	@RequiresApi(Build.VERSION_CODES.O)
	private void dummyNotification(){
		Notification notification = new Notification.Builder(this)
				.setContentTitle(getResources().getString(R.string.dummy_notification_title))
				.setContentText(getResources().getString(R.string.dummy_notification_text))
				.setSmallIcon(R.drawable.ic_launcher_brown)
				.setChannelId(dummyId)
				.build();

		notificationManager.notify(1, notification);
	}

	private void insertPass(boolean archive)
	{
		final boolean arch = archive;

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(getResources().getString(R.string.password_change));
		//alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);
		String cancel = getResources().getString(R.string.dialog_cancel);

		alert.setPositiveButton(getResources().getString(android.R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String value = input.getText().toString();
						SharedPreferences.Editor editor = Opts.edit();
						if(arch) editor.putString("key_archive", value);
						else editor.putString("key_main", value);
						editor.commit();
						recreate();
					}
				});

		alert.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
				//response.concat("_CANCEL");
				finish();

			}
		});

		alert.show();

		//return response;
	}

	/*
	public void modPass()
	{
		//final boolean arch = archive;

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(getResources().getString(R.string.password_change));
		//alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);
		String cancel = getResources().getString(R.string.dialog_cancel);

		alert.setPositiveButton(getResources().getString(android.R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String oldPass = Data.getKey(false);
						String newPass = input.getText().toString();

						if(oldPass.isEmpty()){
							// database not encrypted
							if(newPass.isEmpty()) return;

							File originalFile = new File(document);
							String crypted = originalFile.getParent() + "/encrypted.db";
							String sql;

                            try {
								sql = String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", crypted, newPass);
								Data.database.rawExecSQL(sql);

								//sql = String.format("SELECT sqlcipher_export('%s');", crypted);
								Data.database.rawExecSQL("SELECT sqlcipher_export('encrypted');");

								Data.database.rawExecSQL("DETACH DATABASE encrypted;");

								//Data.database.close();
							}
							catch(Exception e){
								int i =1;
							}
							File cryptedFile = new File(crypted);

							int i = originalFile.getName().lastIndexOf('.');
							String name = originalFile.getName().substring(0,i);
							File backupFile = new File(originalFile.getParent() + "/" + name + "-un.bak");

							originalFile.renameTo(backupFile);

							//File export = new File("encrypted.db");
							cryptedFile.renameTo(originalFile);
						}
						else{
							// encrypted database
							String sql = String.format("PRAGMA rekey = '%s';", newPass);
							Data.database.execSQL(sql);

						}

						SharedPreferences.Editor editor = Opts.edit();

						// if(arch) editor.putString("key_archive", value);
						//else
							editor.putString("key_main", newPass);
						editor.commit();
						//recreate();
					}
				});

		alert.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.

			}
		});

		alert.show();

	}
	*/

	private void stopLocation(){
		if(Opts.getBoolean("GSavePositions", false)) {
			if (LocationPermission) {
				mLocationManager.removeUpdates(mLocationListener);
				mLocationManager = null;
			}
		}

	}

}

