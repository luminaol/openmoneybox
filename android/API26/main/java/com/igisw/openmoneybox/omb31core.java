/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-03-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.widget.ProgressBar;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.GregorianCalendar;
import java.util.Locale;

// sqlcipher library

class omb31core
{
	// Non-GUI components
	public char dec_sep;
	/*
	// Product Capabilities
	public static int MAX_FUNDS     =   100;
	public static int MAX_CREDITS   =   100;
	public static int MAX_DEBTS     =   100;
	public static int MAX_LENT      =  1000;
	public static int MAX_BORROWED  =  1000;
	public static int MAX_SHOPS     =  1000;
	public static int MAX_LINES     = 10000;
	//______________________________
	*/
	SharedPreferences Opts;
	public mainactivity frame;

	public SQLiteDatabase database;

	// Credits table create statement
	private final static String cs_credits_v31 = "CREATE TABLE Credits(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Debts table create statement
	private final static String cs_debts_v31 = "CREATE TABLE Debts(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Loans table create statement
	private final static String cs_loans_v31 = "CREATE TABLE Loans(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER);";

	// Borrows table create statement
	private final static String cs_borrows_v31 = "CREATE TABLE Borrows(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER);";

	// Transactions list table create statement
	private final static String cs_transactions_v31 = "create TABLE Transactions(" +
			"id INTEGER PRIMARY KEY," +
			"isdate INTEGER," +
			"date INTEGER," +
			"time INTEGER," +
			"type INTEGER," +
			"value TEXT," +
			"reason TEXT," +
			"cat_index INTEGER);";

	// Credits table create statement (master)
	private final static String cs_credits_master_v31 = "CREATE TABLE Credits%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Debts table create statement (master)
	private final static String cs_debts_master_v31 = "CREATE TABLE Debts%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Loans table create statement (master)
	private final static String cs_loans_master_v31 = "CREATE TABLE Loans%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER);";

	// Borrows table create statement (master)
	private final static String cs_borrows_master_v31 = "CREATE TABLE Borrows%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER);";

	// Routines
	
	/*
	private boolean SortValues(TTypeVal T)
	{
		int Min,x,y;
		//String N;
		//double V;
		TVal temp;
		switch(T){
			case tvFou:
				for(x = 0; x < NFun; x++){
					Min = x;
					for(y = x + 1; y < NFun; y++)if(Funds.get(Min).Name.compareTo(Funds.get(y).Name) > 0)Min = y;
					if(Min != x){
						temp = Funds.get(x);
						//N = temp.Name;
						//V = temp.Value;
						Funds.set(x, Funds.get(Min));
						Funds.set(Min, temp);
						}}
				break;
		    case tvCre:
		    	for(x = 0; x < NCre ; x++){
		    		Min = x;
		    		for(y = x + 1; y < NCre; y++)if(Credits.get(Min).Name.compareTo(Credits.get(y).Name) > 0)Min = y;
		    		if(Min != x){
		    			temp = Credits.get(x);
		    			//N = Credits[x].Name;
		    			//V = Credits[x].Value;
		    			Credits.set(x, Credits.get(Min));
		    			Credits.set(Min, temp);
		    		}}
		    	break;
		    case tvDeb:
		    	for(x = 0; x < NDeb; x++){
		    		Min = x;
		    		for(y = x + 1; y < NDeb; y++)if(Debts.get(Min).Name.compareTo(Debts.get(y).Name) > 0)Min = y;
		    		if(Min != x){
		    			temp = Debts.get(x);
		    			//N = Debts[x].Name;
		    			//V = Debts[x].Value;
		    			Debts.set(x, Debts.get(Min));
		    			Debts.set(Min, temp);
		    		}}
		    	break;
		    	default:
		    		return false;}
		  return true;}
	*/

	// Non-GUI components
	double Tot_Funds;
	double Tot_Credits;
	double Tot_Debts;

	/*
	// Routines
	protected void UpdateProgress(int i)
	{
		if( Progress != null){
			boolean nonzero = false;
			if(Progress.getProgress() == 0) nonzero = true;
			Progress.setProgress(i);
			if(nonzero){
				if(i == 0)Progress.setVisibility(View.INVISIBLE);}
			else if(i > 0)Progress.setVisibility(View.VISIBLE);
			//Progress->Update();
			//wxTheApp->Yield(false);
		}
	}
	*/

	/*
	protected void Sort()
	{
		if(Lines.size() < 3)return;
		int Min;
		for(int i = 0; i < Lines.size() - 1; i++){
			Min = i;
			for(int j = i + 1; j < Lines.size(); j++){
				if(IsDate(Min)){
					if(Lines.get(Min).Date.compareTo(Lines.get(j).Date) > 0) Min = j;}
				else{
					if(Lines.get(Min).Date.compareTo(Lines.get(j).Date) > 0) Min = j;
					else if(Lines.get(Min).Date.compareTo(Lines.get(j).Date) == 0){
						if(IsDate(j)) Min = j;
						else{
							// Workaround necessary due to use of wxTimePickerCtrl
							Lines.get(Min).Time.set(1900, Day.get(GregorianCalendar.MONTH), 1);
							Lines.get(j).Time.set(1900, Day.get(GregorianCalendar.MONTH), 1);
							if(Lines.get(Min).Time.compareTo(Lines.get(j).Time) > 0) Min = j;}

						}}}
			if(Min > i){
				TLine Tmp = Lines.get(i);
				Lines.set(i, Lines.get(Min));
				Lines.set(Min, Tmp);
			}}}
	*/

	// Non-GUI components
	public enum TTypeVal {tvFou, tvCre, tvDeb}
	public TTypeVal typeVal;
	
	public class TVal_v31{
		public int Id;
		public String Name;
		public double Value;
		public TVal_v31(){
			Name = "";
			Value = 0;
		}
	}

	public enum TOpType{
		  toNULL,
		  toGain, toExpe,
		  toSetCre, toRemCre, toConCre,
		  toSetDeb, toRemDeb, toConDeb,
		  toGetObj, toGivObj,
		  toLenObj, toBakObj,
		  toBorObj, toRetObj
	}
	public TOpType opType;

	public enum TObjType {toPre, toInP}

	public TObjType objType;
	
	public class TObj_v31{
		public int Id;
		public String Name, Object;
		public GregorianCalendar Alarm;
		public TObj_v31(){
			Id = -1;
			Name = "";
			Object = "";
			Alarm = new GregorianCalendar();
			Alarm.set(1900, 1, 1);
		}
	}

	public class TShopItem{
		public int Id;
		public String Name;
		public GregorianCalendar Alarm;
		TShopItem(){
			Id = -1;
			Name = "";
			Alarm = new GregorianCalendar();
			Alarm.set(1900, 1, 1);
		}
	}

	public class TLine_v31{
		int Id;
		public boolean IsDate;
		public GregorianCalendar Date, Time;
		public TOpType Type;
		public String Value, Reason;
		public long CategoryIndex;
		public TLine_v31(){
			Id = -1;
			IsDate = false;
			Date = new GregorianCalendar();
			Date.set(1900, 1, 1);
			Time = new GregorianCalendar();
			Time.set(1900, 1, 1);
			Type = TOpType.toNULL;
			Value = "";
			Reason = "";
		  	CategoryIndex = -1;
		}
	}

	public class TCategory{
		int Id;
		String Name;
		boolean test;
		TCategory(){
			Id = -1;
			Name = "";
			test = false;
		}
	}

	//bool NotEnoughMemory;
	//private boolean Opening; // Set to true during file loading
	boolean Parsing;	// Set to true during database parsing
	public GregorianCalendar Day;
	//public GregorianCalendar Hour;
	public int NFun, NCre, NDeb, NLen, NBor, NSho, NLin, NCat;
	public class ACC{
		public boolean Modified,ReadOnly;
		int Year;
		//public GregorianCalendar DateStamp;
		public long Month;
		public File FileName;
		public String DefFund;}

	public final ACC FileData;

	public final ArrayList<TVal_v31> Funds;
	private final ArrayList<TVal_v31> Credits, Debts;
	private final ArrayList<TObj_v31> Lent, Borrowed;
	public final ArrayList<TShopItem> ShopItems;
	private final ArrayList<TLine_v31> Lines;
	public final ArrayList<String> MattersBuffer;
	public final ArrayList<TCategory> CategoryDB;

	// Routines
	omb31core(String document, mainactivity mainframe, ProgressBar ProgressBar)
	{
		if(mainframe != null){
			Opts = PreferenceManager.getDefaultSharedPreferences(mainframe);
		}
		// Data creation
		Funds = new ArrayList<>();
		Credits = new ArrayList<>();
		Debts = new ArrayList<>();
		Lent = new ArrayList<>();
		Borrowed = new ArrayList<>();
		ShopItems = new ArrayList<>();
		Lines = new ArrayList<>();
		//__________________
		//NotEnoughMemory=false;
		MattersBuffer = new ArrayList<>();
		MattersBuffer.add(null);

		CategoryDB = new ArrayList<>();
		// CategoryDB.add(null);

		FileData = new ACC();

		//android.widget.ProgressBar progress = ProgressBar;

		Initialize(document, true);

		openDatabase(document);
		
		//Opening=false;
	
		//mainframe = frame;
	}

	private void Initialize(String document, boolean Creating)
	{
		NFun = NCre = NDeb = NLen = NBor = NSho = NLin = NCat = 0;
		
		//TODO: removing following dummy initializations
		// Funds initialization
		TVal_v31 temp = new TVal_v31();
		Funds.add(temp);
		// Credits initialization
		Credits.add(temp);
		// Debts initialization
		Debts.add(temp);
		// Lent objects initialization
		//TObj temp_obj = new TObj();
		//Lent.add(temp_obj);
		// Borrowed objects initialization
		//Borrowed.add(temp_obj);
		// Shopping list initialisation
		//TShopItem temp_shop = new TShopItem();
		//ShopItems.add(temp_shop);
		/*
		// Report initialisation
		TLine temp_line = new TLine();
		Lines.add(temp_line);
	  	*/
	  	// Totals zeroing
		Tot_Funds = Tot_Credits = Tot_Debts = 0;
		// File data initialisation
		FileData.FileName = new File(document);
		//FileData.FileView = null;
		//FileData.Access = null;	
		FileData.DefFund = null;
		FileData.Modified=false;
		FileData.ReadOnly=false;
		if(Creating){
		    //Word A,M,G;
			Day = (GregorianCalendar) GregorianCalendar.getInstance();
			FileData.Year = Day.get(GregorianCalendar.YEAR);
			//FileData.Month=/*today*/Day.GetMonth(wxDateTime::Local);
			// Following code is a workaround for GetMonth() bug in Linux
			// http://forums.wxwidgets.org/viewtopic.php?t=25154&highlight=getmonth
			//String temp;
			FileData.Month = Day.get(GregorianCalendar.MONTH);
			//FileData.DateStamp.set(1900, 1, 1);
		}
	}

	private boolean addDate_v31(int id, GregorianCalendar D, String T)
	{
		//if(!D.IsValid())return false;
		if(! Parsing){
		    int M, Y;
			Y = Day.get(GregorianCalendar.YEAR);
			M = Day.get(GregorianCalendar.MONTH);
			if((Y != FileData.Year) || (M != FileData.Month)){
				if(Opts.getBoolean("GConv", false)) frame.textConvClick(null);
				
				String Path = FileData.FileName.getParent();
		        String month = String.format("%02d", FileData.Month + 1);
				String File = Opts.getString("GDPrefix", "OpenMoneyBox") + "_";
				File += String.format("%d", FileData.Year);
				File += "-" + month + ".omb";
				FileData.Modified = true;

				File fPath, fMaster, fExport;
				SQLiteDatabase db, exp;
				fMaster = frame.getDatabasePath(constants.archive_name);
				fPath = new File(fMaster.getParent());
				fPath.mkdirs();
				String master = fMaster.toString();

				String monthstring = String.format("%d_%02d", FileData.Year, FileData.Month + 1);
				String trailname = "_" + monthstring;
				String export = Path + "/" + trailname;
				fExport = new File(export);

				boolean master_exist = fMaster.exists();
				boolean export_exist = fExport.exists();

				db = SQLiteDatabase.openOrCreateDatabase(master, "", null);
				exp = SQLiteDatabase.openOrCreateDatabase(export, "", null);
				
				if(! master_exist) db.setVersion(constants.dbVersion);
				if(! export_exist) exp.setVersion(constants.dbVersion);

				// Write file metadata
				String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
						+ android.os.Build.VERSION.RELEASE;

				ContentValues cv = new ContentValues();
				
				if(! tableExists(db, "Information")){
					db.execSQL(constants.cs_information);
					
					cv.put("id", String.format("%d", constants.dbMeta_application_info));
					cv.put("data", metadata);
					db.insert("Information", null, cv);
					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_default_fund));
					cv.put("data", "default");
					db.insert("Information", null, cv);
				}
				else{
					cv.put("data", metadata);
					db.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
				}

				if(! tableExists(exp, "Information")){
					exp.execSQL(constants.cs_information);
					
					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_application_info));
					cv.put("data", metadata);
					exp.insert("Information", null, cv);
					cv.clear();
					cv.put("id", String.format("%d", constants.dbMeta_default_fund));
					cv.put("data", "default");
					exp.insert("Information", null, cv);
				}
				else{
					cv.put("data", metadata);
					exp.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
				}

				// Transaction table init
				if(! tableExists(db, "Transactions")) db.execSQL(cs_transactions_v31);
				if(! tableExists(exp, "Transactions")) exp.execSQL(cs_transactions_v31);

				// Category table init
				boolean master_had_categories = tableExists(db, "Categories");
				if(! master_had_categories) db.execSQL(constants.cs_categories);
				boolean export_had_categories = tableExists(exp, "Categories");
				if(! export_had_categories) exp.execSQL(constants.cs_categories);

				//String trailname = "_" + monthstring;
				if(! tableExists(db, "Funds" + trailname)) db.execSQL(String.format(constants.cs_funds_master, trailname));
				if(! tableExists(db, "Credits" + trailname)) db.execSQL(String.format(cs_credits_master_v31, trailname));
				if(! tableExists(db, "Debts" + trailname)) db.execSQL(String.format(cs_debts_master_v31, trailname));
				if(! tableExists(db, "Loans" + trailname)) db.execSQL(String.format(cs_loans_master_v31, trailname));
				if(! tableExists(db, "Borrows" + trailname)) db.execSQL(String.format(cs_borrows_master_v31, trailname));
				if(! tableExists(exp, "Funds" + trailname)) exp.execSQL(String.format(constants.cs_funds_master, trailname));
				if(! tableExists(exp, "Credits" + trailname)) exp.execSQL(String.format(cs_credits_master_v31, trailname));
				if(! tableExists(exp, "Debts" + trailname)) exp.execSQL(String.format(cs_debts_master_v31, trailname));
				if(! tableExists(exp, "Loans" + trailname)) exp.execSQL(String.format(cs_loans_master_v31, trailname));
				if(! tableExists(exp, "Borrows" + trailname)) exp.execSQL(String.format(cs_borrows_master_v31, trailname));
				
				db.close();
				exp.close();

				database.execSQL("RELEASE rollback;");
				// backup database
				try {
					omb_library.copyFile(FileData.FileName, Path + "/" + File);
				} catch (IOException e) {
					//e.printStackTrace();
					return false;
				}

				database.execSQL(String.format("ATTACH DATABASE '%s' AS master;", master));
				database.execSQL(String.format("ATTACH DATABASE '%s' AS export;", export));
				//List<Pair<String, String>> debug = database.getAttachedDbs();
				
				// Archive data in master database
				database.execSQL("INSERT INTO master.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM Transactions;");
				database.execSQL(String.format("INSERT INTO master.Funds%s SELECT * FROM Funds;", trailname));
				database.execSQL(String.format("INSERT INTO master.Credits%s SELECT * FROM Credits;", trailname));
				database.execSQL(String.format("INSERT INTO master.Debts%s SELECT * FROM Debts;", trailname));
				database.execSQL(String.format("INSERT INTO master.Loans%s SELECT * FROM Loans;", trailname));
				database.execSQL(String.format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", trailname));
				if(! master_had_categories) database.execSQL("INSERT INTO master.Categories SELECT * FROM Categories;");

				// Archive data in export database
				database.execSQL("INSERT INTO export.Transactions (isdate, date, time, type, value, reason, cat_index) SELECT isdate, date, time, type, value, reason, cat_index FROM Transactions;");
				database.execSQL(String.format("INSERT INTO export.Funds%s SELECT * FROM Funds;", trailname));
				database.execSQL(String.format("INSERT INTO export.Credits%s SELECT * FROM Credits;", trailname));
				database.execSQL(String.format("INSERT INTO export.Debts%s SELECT * FROM Debts;", trailname));
				database.execSQL(String.format("INSERT INTO export.Loans%s SELECT * FROM Loans;", trailname));
				database.execSQL(String.format("INSERT INTO export.Borrows%s SELECT * FROM Borrows;", trailname));
				if(! master_had_categories) database.execSQL("INSERT INTO export.Categories SELECT * FROM Categories;");

				cv.clear();
				cv.put("data", monthstring);
				database.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_mobile_export), null);

				database.execSQL("DELETE FROM Transactions");
				// ________________________________

				database.execSQL("DETACH DATABASE 'master';");
				database.execSQL("DETACH DATABASE 'export';");
				database.execSQL("VACUUM");
				database.execSQL("SAVEPOINT rollback;");

				/*
				for(int i = 0; i <= CategoryDB.size() - 1;i++)
					CategoryDB.set(i, omb_library.iUpperCase(CategoryDB.get(i)));
				Collections.sort(CategoryDB);
				*/
				
				Lines.clear();
				NLin = 0;
				FileData.Month = M;
				FileData.Year = Y;
			}}

		else	// Robustness code to avoid creation of duplicate date entries
			for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate)
				if(Lines.get(i).Date == D) return true;

		if(Parsing){
			TLine_v31 temp = new TLine_v31();
			temp.Id = id;
			temp.IsDate = true;
			temp.Date = D;
			//temp.Time.set(1900, 1, 1);
			temp.Type = TOpType.toNULL;
			temp.Value = T;
			temp.Reason = null;
			temp.CategoryIndex = -1;
			Lines.add(NLin, temp);
			//NLin++;
		}
		else{
			ContentValues cv = new ContentValues();
			
			//cv.put("id", "NULL");
			cv.put("isdate", true);
			cv.put("date", D.getTimeInMillis() / 1000);
			cv.put("time", -1);
			cv.put("type", 0);
			cv.put("value", T);
			cv.put("reason", "");
			cv.put("cat_index", -1);
			database.insert("Transactions", null, cv);
			
			//Sort();
		}
		FileData.Modified=true;
		return true;
	}

	public boolean addValue(TTypeVal T, int id, String N, double V)
	{
		if(! Parsing) if(N.isEmpty() || V == 0) return false;
		boolean E = false;
		int x;
		TVal_v31 temp = new TVal_v31();
		switch(T){
			case tvFou:
				for(x = 0; x < NFun; x++)if(Funds.get(x).Name.equals(N)){
					omb_library.Error(11, N);
					return false;}
				if(Parsing){
					TVal_v31 last = new TVal_v31();
					last.Id = id;
					last.Name = N;
					last.Value = V;
					Funds.add(NFun, last);
				}
				else{
					if(! tableExists(database, "Funds"))
						database.execSQL(constants.cs_funds);
						
					ContentValues cv = new ContentValues();
					
					//cv.put("id", "NULL");
					cv.put("name", N);
					cv.put("value", V);
					database.insert("Funds", null, cv);
				}
				/*
				if(!SortValues(TTypeVal.tvFou)){
					omb_library.Error(37, null);
					//Application->Terminate();
					//wxTheApp->Exit();	// TODO: to be implemented
					return false;}
				*/
				break;
			case tvCre:
				for(x = 0; x < NCre; x++)if(Credits.get(x).Name.compareToIgnoreCase(N) == 0){
					Credits.get(x).Value += V;

					/*
					ContentValues cv = new ContentValues();
					cv.put("value", Credits.get(x).Value);
					database.update("Credits", cv, "id = " + String.format("%d", Credits.get(x).Id), null);
					*/
					changeFundValue(TTypeVal.tvCre, Credits.get(x).Id, Credits.get(x).Value);

					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						Credits.add(NCre, temp);
						//NCre++;
					}
					else{
						if(! tableExists(database, "Credits"))
							database.execSQL(cs_credits_v31);
							ContentValues cv = new ContentValues();
							cv.put("name", N);
							cv.put("value", V);
							database.insert("Credits", null, cv);
					}
				}
				//Tot_Credits += V;
				/*
				if(!SortValues(TTypeVal.tvCre)){
					omb_library.Error(37, null);
					//Application->Terminate();
					//wxTheApp->Exit();	// TODO: to be implemented
					return false;}
				*/
				break;
			case tvDeb:
				for(x = 0; x < NDeb; x++)if(Debts.get(x).Name.compareToIgnoreCase(N) == 0){
					Debts.get(x).Value += V;

					/*
					ContentValues cv = new ContentValues();
					cv.put("value", Debts.get(x).Value);
					database.update("Debts", cv, "id = " + String.format("%d", Debts.get(x).Id), null);
					*/
					changeFundValue(TTypeVal.tvDeb, Debts.get(x).Id, Debts.get(x).Value);

					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						Debts.add(NDeb, temp);
					//NDeb++;
					}
					else{
						if(! tableExists(database, "Debts"))
							database.execSQL(cs_debts_v31);
							ContentValues cv = new ContentValues();
							cv.put("name", N);
							cv.put("value", V);
							database.insert("Debts", null, cv);
					}
				}
				//Tot_Debts += V;
				/*
				if(!SortValues(TTypeVal.tvDeb)){
					omb_library.Error(37, null);
					//Application->Terminate();
					//wxTheApp->Exit();	// TODO: to be implemented
					return false;}
				*/
				break;
			default:
				return false;}
		if(! Parsing) parseDatabase_v31();
		return true;}

	public /*boolean*/void delValue(TTypeVal T,int I)
	{
		if(I < 0)return /*false*/;
		
		/*
		//int x;
		double cur;
		switch(T){
			case tvFou:
			if(I > Funds.size() - 1)return false;
			break;
			case tvCre:
			if(I > Credits.size() - 1)return false;
			break;
			case tvDeb:
			if(I > Debts.size() - 1)return false;
			break;
			default:
			return false;}
		*/
		
		switch(T){
			case tvFou:
				/*
				cur = Funds.get(I).Value;
				Funds.remove(I);
				Tot_Funds-=cur;
				NFun--;
				*/
				
				database.delete("Funds", "id=?", new String[]{Integer.toString(I)});
				break;
			case tvCre:
				/*
				cur = Credits.get(I).Value;
				Credits.remove(I);
				Tot_Credits -= cur;
				NCre--;
				*/
				
				database.delete("Credits", "id=?", new String[]{Integer.toString(I)});
				break;
			case tvDeb:
				/*
				cur = Debts.get(I).Value;
				Debts.remove(I);
				Tot_Debts -= cur;
				NDeb--;
				*/
				database.delete("Debts", "id=?", new String[]{Integer.toString(I)});
		}
		parseDatabase_v31();
		FileData.Modified=true;
		//return /*true*/;
	}

	private boolean addObject(TObjType T, int id, String N, String O, GregorianCalendar D)
	{
		if(N.isEmpty() || O.isEmpty())return false;
		//if (D == invalidDate) return false;	// TODO: to be implemented
		switch(T){
			case toPre:
				if(Parsing){
					TObj_v31 temp = new TObj_v31();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					Lent.add(temp);
					//NLen++;
				}
				else{
					if(! tableExists(database, "Loans"))
						database.execSQL(cs_loans_v31);
					ContentValues cv = new ContentValues();
					cv.put("name", N);
					cv.put("item",  O);
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);
					else
						cv.put("alarm", -1);
					database.insert("Loans", null, cv);
					parseDatabase_v31();
				}
				break;
			case toInP:
				if(Parsing){
					TObj_v31 temp = new TObj_v31();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					Borrowed.add(temp);}
				else{
					if(! tableExists(database, "Borrows"))
						database.execSQL(cs_borrows_v31);
					ContentValues cv = new ContentValues();
					cv.put("name", N);
					cv.put("item",  O);
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);
					else
						cv.put("alarm", -1);
					database.insert("Borrows", null, cv);
					//NBor++;
					parseDatabase_v31();
				}
		}
		FileData.Modified=true;
	  return true;}

	public /*boolean*/void delObject(TObjType T, int I)
	{
		if(I<0)return /*false*/;
		//int x;
		switch(T){
			case toPre:
				//if(I > Lent.size() - 1)return false;
				database.delete("Loans", "id=?", new String[]{Integer.toString(I)});
				break;
			case toInP:
				database.delete("Borrows", "id=?", new String[]{Integer.toString(I)});
				//if(I > Borrowed.size() - 1)return false;
				break;
			default:
				return /*false*/;}
		parseDatabase_v31();
		FileData.Modified=true;
		//return /*true*/;
	}

	public boolean addShopItem(int id, String N, GregorianCalendar A)
	{
		if(N.isEmpty())return false;
		//if(A == invalidDate) return false; TODO: to be implemented
		if(Parsing){
			TShopItem temp = new TShopItem();
			temp.Id = id;
			temp.Name = N;
			temp.Alarm = A;
			ShopItems.add(temp);
			//NSho++;
		}
		else{
			if(! tableExists(database, "Shoplist"))
				database.execSQL(constants.cs_shoplist);
			ContentValues cv = new ContentValues();
			cv.put("item",  N);
			
			if(omb_library.checkAlarm(A))
				cv.put("alarm", A.getTimeInMillis() / 1000);
			else
				cv.put("alarm", -1);
			database.insert("Shoplist", null, cv);
			parseDatabase_v31();
		}
		FileData.Modified=true;
		return true;}

	public /*boolean*/void delShopItem(int I)
	{
		if(I < 0 /*|| I >= ShopItems.size()*/)return /*false*/;
		
		/*
		ShopItems.remove(I);
		NSho--;
		*/
		
		database.delete("Shoplist", "id=?", new String[]{Integer.toString(I)});
		parseDatabase_v31();
		
		FileData.Modified=true;
		//return /*true*/;
	}

	private boolean addOper(int id, GregorianCalendar D, GregorianCalendar O, TOpType T, String V, String M, long N)
	{
		if(T == TOpType.toNULL || V.isEmpty() || M.isEmpty())return false;
		String Tot = omb_library.FormDigits(Tot_Funds, true);
		if(Parsing){
			TLine_v31 temp = new TLine_v31();
			temp.Id = id;
			temp.IsDate = false;
			temp.Date = D;
			temp.Time = O;
			temp.Type = T;
			temp.Value = V;
			temp.Reason = M;
			temp.CategoryIndex = N;
			Lines.add(NLin, temp);
			//NLin++;
		}
		else{
			// Last date check
			boolean found = false;
			if(NLin < 1){
				addDate_v31(-1, D, Tot);
				found = true;
			}
			else for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate){
				GregorianCalendar cal1 = Lines.get(i).Date;
				boolean sameday = cal1.get(Calendar.YEAR) == D.get(Calendar.YEAR) &&
					cal1.get(Calendar.DAY_OF_YEAR) == D.get(Calendar.DAY_OF_YEAR);
				if(sameday){
					found = true;
					break;}}
			if (! found) addDate_v31(-1, D, Tot);

			ContentValues cv = new ContentValues();
			
			cv.put("isdate", false);
			cv.put("date", D.getTimeInMillis() / 1000);
			cv.put("time", O.getTimeInMillis() / 1000);

			int value = 0;
			switch (T) {
				case toGain:
					value = 1;
					break;
				case toExpe:
					value = 2;
					break;
				case toSetCre:
					value = 3;
					break;
				case toRemCre:
					value = 4;
					break;
				case toConCre:
					value = 5;
					break;
				case toSetDeb:
					value = 6;
					break;
				case toRemDeb:
					value = 7;
					break;
				case toConDeb:
					value = 8;
					break;
				case toGetObj:
					value = 9;
					break;
				case toGivObj:
					value = 10;
					break;
				case toLenObj:
					value = 11;
					break;
				case toBakObj:
					value = 12;
					break;
				case toBorObj:
					value = 13;
					break;
				case toRetObj:
					value = 14;
					break;
			}

			cv.put("type", value);
			cv.put("value", V);
			cv.put("reason", M);
			cv.put("cat_index", N);
			database.insert("Transactions", null, cv);

			FileData.Modified=true;
			parseDatabase_v31();
			//Sort();
		}
		return true;
	}

/*
	public String LoadFromFile(boolean Auto, String AName, String F, boolean App)
	{
		Opening=true;
		String Name = null;
		if(Auto)Name=AName;
		if(F==Name){
			omb_library.Error(1, Name);
			Opening=false;
			return null;}
		File f = new File(Name); 
		if(! f.exists()){
			Opening=false;
			return null;}
		switch(omb_library.CheckFileFormat(Name)){
		case bilFFORMAT_UNKNOWN:	// Unknown Format
			omb_library.Error(2, Name);
			Opening=false;
			return null;
		case bilFFORMAT_31:
			break;
		case bilFFORMAT_302:
		case bilFFORMAT_301:	// v3
			// TODO (igor#1#): IMPLEMENT CONVERSION PROMPT

			break;
		default:
			Opening=false;
		return null;}
		
		// boolean didCat = false;	// set to true when first real Category is set in CategoryDB
		
		int b,BR = 0; // FH: opened file handler
		long L, CI = 0;	// Category index
		int OP;									//  L: file or string lenght
	                    //  b: bugger index
	                    // BR: buffer lenght
	                    // OP: read operation identifier
		byte Buffer[];
		String S,V,Al,Dat;  // N: file name
														// S: operation name
														// V: value
														// Al: alarm
														// Dat: last read date
		double cur = 0;  // cur: long pointer for string convertions
		GregorianCalendar aldate;             // aldate: wxdatetime pointer for string convertions

	    //Dat=::wxDateTime::Today().Format(L"%d/%m/%Y",wxDateTime::Local);  // Initialisation
	    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    Dat = df.format(Calendar.getInstance().getTime());

		// file copy into the buffer ---------------//
	    FileInputStream file = null;
		try {
			file = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	//
		L = f.length();							//
		//file->Seek(0);								//
		Buffer = new byte[(int) L+1];						//
		try {
			BR = file.read(Buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //-----------------//
		b=6;

		// Check if file has metadata
		if(Buffer[b] == '#'){
			do b++;
			while (Buffer[b] != '#');
			b++;}

		S = "";
		int ch_int;
		char ch;
		do{
			if(Buffer[b] < 0) {
				ch_int = Buffer[b] + 256;
				ch = (char)ch_int;
			}
			else ch = (char)Buffer[b];
			S += ch;
			b++;
			if(Buffer[b] < 0) {
				ch_int = Buffer[b] + 256;
				ch = (char)ch_int;
			}
			else ch = (char)Buffer[b];
		}
		while((ch != (char)255) && b < BR);
		S=Crip(S);
		

		Initialize(AName, false);
		FileData.Access=S; // Password storage
		//FileData.FileName=Name;
		//::wxBeginBusyCursor(wxHOURGLASS_CURSOR); TODO: to be implemented
		b++;
		while(b<BR){
			UpdateProgress(b*100/BR);
			S = "";
			do {
				if(Buffer[b] < 0) {
					ch_int = Buffer[b] + 256;
					ch = (char)ch_int;
				}
				else ch = (char)Buffer[b];
				S += ch;
				b++;
				if(Buffer[b] < 0) {
					ch_int = Buffer[b] + 256;
					ch = (char)ch_int;
				}
				else ch = (char)Buffer[b];
			}
			while(ch != (char)255 && b < BR);
			OP = Integer.parseInt(S.substring(0,2));
			L = S.length();
			S = S.substring(2, (int)L);
			S=Crip(S);
			b++;
			if(OP<4){ // the operation is a value
				V = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					V += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				//V=V.Trim();
				V=Crip(V);
				
				cur = Double.parseDouble(V);
				
				switch(OP){
					case 1:
						if(AddValue(TTypeVal.tvFou, S, cur)) Tot_Funds += cur;
						break;
					case 2:
						AddValue(TTypeVal.tvCre, S, cur);
						break;
					case 3:
						AddValue(TTypeVal.tvDeb, S, cur);}}
			else if(OP==4||OP==5){ // the operation is an object
				V = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					V += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				V=Crip(V);
				b++;
				Al = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					Al += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				Al=Crip(Al);
				aldate = omb_library.omb_StrToDate(Al);
				if(OP == 4)AddObject(TObjType.toPre, S, V, aldate);
				else AddObject(TObjType.toInP, S, V, aldate);}
			else if(OP==6){ // the operation is a shopping list item
				Al = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					Al += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);

				Al=Crip(Al);
				AddShopItem(S, omb_library.omb_StrToDate(Al));}
			else if(OP==7){ // the operation is a date
				V = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					V += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				V = Crip(V);
				
				Dat=S;
				addDate(omb_library.omb_StrToDate(S), V);}
			else if(OP==9)FileData.DefFund=S; // default fund storage

			else if(OP == 10){
				CategoryDB.add(S);
				b--;}

			else{	// the operation is a transaction
				String Val, Mot, Not = "";
				GregorianCalendar Tim = omb_library.omb_StrToTime(S);
				TOpType Type = TOpType.toNULL;
				S = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					S += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				if(S.compareTo("A") == 0) Type=TOpType.toGain;
				else if(S.compareTo("B") == 0) Type=TOpType.toExpe;
				else if(S.compareTo("C") == 0) Type=TOpType.toSetCre;
				else if(S.compareTo("D") == 0) Type=TOpType.toRemCre;
				else if(S.compareTo("E") == 0) Type=TOpType.toConCre;
				else if(S.compareTo("F") == 0) Type=TOpType.toSetDeb;
				else if(S.compareTo("G") == 0) Type=TOpType.toRemDeb;
				else if(S.compareTo("H") == 0) Type=TOpType.toConDeb;
				else if(S.compareTo("I") == 0) Type=TOpType.toGetObj;
				else if(S.compareTo("J") == 0) Type=TOpType.toGivObj;
				else if(S.compareTo("K") == 0) Type=TOpType.toLenObj;
				else if(S.compareTo("L") == 0) Type=TOpType.toBakObj;
				else if(S.compareTo("M") == 0) Type=TOpType.toBorObj;
				else if(S.compareTo("N") == 0) Type=TOpType.toRetObj;
				b++;
				S = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					S += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				Val=Crip(S);
				b++;
				S = "";
				do{
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
					S += ch;
					b++;
					if(Buffer[b] < 0) {
						ch_int = Buffer[b] + 256;
						ch = (char)ch_int;
					}
					else ch = (char)Buffer[b];
				}
				while(ch != (char)255);
				Mot=Crip(S);
				b++;
				if(Buffer[b] < 0) {
					ch_int = Buffer[b] + 256;
					ch = (char)ch_int;
				}
				else ch = (char)Buffer[b];
				if(ch != (char)255){
					S = "";
					do{
						if(Buffer[b] < 0) {
							ch_int = Buffer[b] + 256;
							ch = (char)ch_int;
						}
						else ch = (char)Buffer[b];
						S += ch;
						b++;
						if(Buffer[b] < 0) {
							ch_int = Buffer[b] + 256;
							ch = (char)ch_int;
						}
						else ch = (char)Buffer[b];
					}
					while(ch != (char)255);
					Not=Crip(S);
					CI = -1;
					CI = Long.parseLong(Not);
				}
				AddOper(omb_library.omb_StrToDate(Dat), Tim, Type, Val, Mot, CI);
				}
			b++;}
		GregorianCalendar LastDate = omb_library.omb_StrToDate(Dat);
		//if(!LastDate.IsValid())LastDate=wxDateTime::Today();	// TODO: to be implemented
		FileData.Year = LastDate.get(Calendar.YEAR);
		FileData.Month = LastDate.get(Calendar.MONTH);
		
		// File attributes
		//wxFileName *fattrs=new wxFileName(Name);
		//FileData.DateStamp.setTime(new Date(f.lastModified()));
		if(! f.canWrite()) FileData.ReadOnly = true;

		try {
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		FileData.Modified=false;
		//delete Dialog;
		Opening=false;
		//delete[] Buffer;
		UpdateProgress(100);
		UpdateProgress(0);
		//::wxEndBusyCursor();	// TODO: to be implemented
		return Name;}
*/
	
	public double GetTot(TTypeVal T)
	{
		  double Ret;
		  switch(T){
		    case tvFou:
				Ret=Tot_Funds;
		    break;
		    case tvCre:
		    Ret=Tot_Credits;
		    break;
		    case tvDeb:
		    Ret=Tot_Debts;
		    break;
		    default:Ret=-1;}
		  return Ret;}
	
	/*
	public boolean SetTot(TTypeVal T,double Tot)
	{
		  switch(T){
		    case tvFou:
				Tot_Funds=Tot;
		    break;
		    case tvCre:
		    Tot_Credits=Tot;
		    break;
		    case tvDeb:
		    Tot_Debts=Tot;
		    break;
		    default:
		    return false;}
		  return true;
	}
	*/

	public boolean IsDate_v31(int R)
	{
		return Lines.get(R).IsDate;
	}
	
	/*
	public boolean HasAlarms(){
		boolean result=false;
		int i;
		GregorianCalendar checkdate = new GregorianCalendar();
		checkdate.add(Calendar.YEAR, 90);
		for(i = 0; i < NSho; i++)if(ShopItems.get(i).Alarm.compareTo(checkdate) == -1){
				result = true;
				break;}
		if(result) return true;
		for(i = 0; i < NLen; i++)if(Lent.get(i).Alarm.compareTo(checkdate) == -1){
				result = true;
				break;}
		if(result) return true;
		for(i = 0; i < NBor; i++)if(Borrowed.get(i).Alarm.compareTo(checkdate) == -1){
				result = true;
				break;}
		return result;}
	*/

	/*
	public int checkShopList(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NSho; i++)
			if(ShopItems.get(i).Alarm.compareTo(Now) == -1) return i;
		return -1;
	}

	public int checkLentObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NLen; i++)
			if(Lent.get(i).Alarm.compareTo(Now) == -1) return i;
		return -1;
	}

	public int checkBorrowedObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NBor; i++)
			if(Borrowed.get(i).Alarm.compareTo(Now) == -1) return i;
		return -1;
	}
	*/

	private void 	openDatabase(String File){
		boolean file_exist = false;

		File f = new File(File);
		if(f.exists()) file_exist = true;

	    if(file_exist){
	     	switch(omb_library.checkFileFormat(File, this.getKey(false))){
		        case bilFFORMAT_UNKNOWN:	// Unknown Format
		          omb_library.Error(2, File);
		          Parsing = false;
		          return /*false*/;
		        case ombFFORMAT_31:
		          break;

				// Comment following line to compile with bilFFORMAT_302 support
				/*
		        case bilFFORMAT_302:
					omb30core olddoc = new omb30core(File);
	        		int i;
	        		//double cur;

	        		database = SQLiteDatabase.openOrCreateDatabase(File, null);

	        		// Write Funds
	        		if(! tableExists(database, "Funds")) database.execSQL(constants.cs_funds);
	        		for(i = 0; i < olddoc.NFon; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.Funds.get(i).Name);
	        			cv.put("value", olddoc.Funds.get(i).Value);
	        			database.insert("Funds", null, cv);
	        		}
	        		// Write default fund
	        		if(! tableExists(database, "Information")){
	        			database.execSQL(constants.cs_information);

	        			ContentValues cv = new ContentValues();

	        			String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
	        					+ android.os.Build.VERSION.RELEASE;
	        			cv.put("id", String.format("%d", constants.dbMeta_application_info));
	        			cv.put("data", metadata);
	        			database.insert("Information", null, cv);

	        			cv.clear();
	        			cv.put("id", String.format("%d", constants.dbMeta_default_fund));
	        			cv.put("data", olddoc.FileData.DefFund);
	        			database.insert("Information", null, cv);
	        		}

	        		// Write Credits
	        		if(! tableExists(database, "Credits")) database.execSQL(cs_credits_v31);
	        		for(i = 0; i < olddoc.NCre; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.Credits.get(i).Name);
	        			cv.put("value", olddoc.Credits.get(i).Value);
	        			database.insert("Credits", null, cv);
	        		}

	        		// Write Debts
	        		if(! tableExists(database, "Debts")) database.execSQL(cs_debts_v31);
	        		for(i = 0; i < olddoc.NDeb; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.Debts.get(i).Name);
	        			cv.put("value", olddoc.Debts.get(i).Value);
	        			database.insert("Debts", null, cv);
	        		}

	        		// Write Loans
	        		if(! tableExists(database, "Loans")) database.execSQL(cs_loans_v31);
	        		for(i = 0; i < olddoc.NLen; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.Lent.get(i).Name);
	        			cv.put("item", olddoc.Lent.get(i).Object);
	        			cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
	        			database.insert("Loans", null, cv);
	        		}

	        		// Write Borrows
	        		if(! tableExists(database, "Borrows")) database.execSQL(cs_borrows_v31);
	        		for(i = 0; i < olddoc.NBor; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.Lent.get(i).Name);
	        			cv.put("item", olddoc.Lent.get(i).Object);
	        			cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
	        			database.insert("Borrows", null, cv);
	        		}

	        		// Write Categories
	        		if(! tableExists(database, "Categories")) database.execSQL(constants.cs_categories);
	        		for(i = 0; i < olddoc.CategoryDB.size(); i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("name", olddoc.CategoryDB.get(i));
	        			cv.put("active", 1);
	        			database.insert("Categories", null, cv);
	        		}

	        		// Write Transactions
	        		if(! tableExists(database, "Transactions")) database.execSQL(cs_transactions_v31);
	        		for(i = 0; i < olddoc.NLin; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("isdate", olddoc.Lines.get(i).IsDate);
	        			cv.put("date", olddoc.Lines.get(i).Date.getTimeInMillis() / 1000);
	        			cv.put("time", olddoc.Lines.get(i).Time.getTimeInMillis() / 1000);

	        			int value = 0;
						omb30core.TOpType T = olddoc.Lines.get(i).Type;
	        			if(T == omb30core.TOpType.toGain)value = 1;
	        			else if(T == omb30core.TOpType.toExpe)value = 2;
	        			else if(T == omb30core.TOpType.toSetCre)value = 3;
	        			else if(T == omb30core.TOpType.toRemCre)value = 4;
	        			else if(T == omb30core.TOpType.toConCre)value = 5;
	        			else if(T == omb30core.TOpType.toSetDeb)value = 6;
	        			else if(T == omb30core.TOpType.toRemDeb)value = 7;
	        			else if(T == omb30core.TOpType.toConDeb)value = 8;
	        			else if(T == omb30core.TOpType.toGetObj)value = 9;
	        			else if(T == omb30core.TOpType.toGivObj)value = 10;
	        			else if(T == omb30core.TOpType.toLenObj)value = 11;
	        			else if(T == omb30core.TOpType.toBakObj)value = 12;
	        			else if(T == omb30core.TOpType.toBorObj)value = 13;
	        			else if(T == omb30core.TOpType.toRetObj)value = 14;

	        			cv.put("type", value);
	        			String cnv = olddoc.Lines.get(i).Value.replace(",", ".");
	        			cv.put("value", cnv);
	        			cv.put("reason", olddoc.Lines.get(i).Reason);
	        			cv.put("cat_index", olddoc.Lines.get(i).CategoryIndex + 1);
	        			database.insert("Transactions", null, cv);

	        		}

	        		// Write shopping list
	        		if(! tableExists(database, "Shoplist")) database.execSQL(constants.cs_shoplist);
	        		for(i = 0; i < olddoc.NSho; i++){
	        			ContentValues cv = new ContentValues();
	        			cv.put("item", olddoc.ShopItems.get(i).Name);
	        			cv.put("alarm", olddoc.ShopItems.get(i).Alarm.getTimeInMillis() / 1000);
	        			database.insert("Shoplist", null, cv);

	        		}

	        		database.close();

		          break;
				*/ // bilFFORMAT_302 ---
		        default:
		          Parsing = false;
		          return /*false*/;}
	    }

		SQLiteDatabaseHook hook = null;
	    if(! file_exist) {
			hook = new SQLiteDatabaseHook() {
				public void preKey(SQLiteDatabase database) {
					database.execSQL(constants.cipher_compat_sql);
				}

				public void postKey(SQLiteDatabase database) {
				}
			};
		}

		database = SQLiteDatabase.openOrCreateDatabase(File, "", null, hook);
		//database.rawQuery("pragma journal_mode=memory;", null);

		if (!file_exist) database.setVersion(31);

		// Set restore savepoint
		database.execSQL("SAVEPOINT rollback;");

		// Write file metadata
		String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
				+ android.os.Build.VERSION.RELEASE;

		ContentValues cv = new ContentValues();

		if (!tableExists(database, "Information")) {
			database.execSQL(constants.cs_information);

			cv.put("id", String.format("%d", constants.dbMeta_application_info));
			cv.put("data", metadata);
			database.insert("Information", null, cv);
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_default_fund));
			cv.put("data", "default");
			database.insert("Information", null, cv);
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_mobile_export));
			cv.put("data", "");
			database.insert("Information", null, cv);
		} else {
			cv.put("data", metadata);
			database.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_application_info), null);
		}

		// Set currency when if not present (added later in dbVersion 31)
		Cursor currencyTable = database.query("Information", new String[] { "data" }, "id = " + String.format("%d", constants.dbMeta_currency), null, null, null, null);
		if(currencyTable.getCount() == 0){
			String curr = Currency.getInstance(Locale.getDefault()).getSymbol();
			cv.clear();
			cv.put("id", String.format("%d", constants.dbMeta_currency));
			cv.put("data", curr);
			database.insert("Information", null, cv);
		}

		// Transaction table init
		if (!tableExists(database, "Transactions"))
			database.execSQL(cs_transactions_v31);

		// Category table init
		if (!tableExists(database, "Categories"))
			database.execSQL(constants.cs_categories);

		parseDatabase_v31();
		FileData.Modified = false;

		currencyTable.close();

		//return /*true*/;
	}

	boolean tableExists(SQLiteDatabase db, String tableName){
	    Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ tableName + "'", null);
	    if(cursor != null) {
	        if(cursor.getCount() > 0) {
	        	cursor.close();
	            return true;
	        }
	        cursor.close();
	    }
	    return false;
	}

	private void parseDatabase_v31(){
		Parsing = true;

		boolean is_date;
		int Rows, id, i;	// Rows: number of rows in the query table
							// id: id of table item
		long ind;			// ind: category index of transaction
		double val;
		TOpType type;
		String str, reason;
		GregorianCalendar date, time, lastdate = new GregorianCalendar();
		Cursor Table;

		// Read Funds
		if(tableExists(database, "Funds")){
			//Table = database->GetTable("select * from Funds order by 1;");
			Table = database.query("Funds", null, null, null, null,null, "name");
			Funds.clear();
			NFun = 0;
			Tot_Funds = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				val = Table.getDouble(2);
				if(addValue(TTypeVal.tvFou, id, str, val)){
					NFun++;
					Tot_Funds += val;}}}

		// Read default fund
		Table = database.query("Information", new String[] { "data" }, "id = " + String.format("%d", constants.dbMeta_default_fund), null, null, null, null);
		Table.moveToPosition(0);
		FileData.DefFund = Table.getString(0);

		// Read Credits
		if(tableExists(database, "Credits")){
			Table = database.query("Credits", null, null, null, null,null, "name");
			Credits.clear();
			NCre = 0;
			Tot_Credits = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				val = Table.getDouble(2);
				if(addValue(TTypeVal.tvCre, id, str, val)){
					NCre++;
					Tot_Credits += val;}}}

		// Read Debts
		if(tableExists(database, "Debts")){
			Table = database.query("Debts", null, null, null, null,null, "name");
			Debts.clear();
			NDeb = 0;
			Tot_Debts = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				val = Table.getDouble(2);
				if(addValue(TTypeVal.tvDeb, id, str, val)){
					NDeb++;
					Tot_Debts += val;}}}

		// Read Loans
		if(tableExists(database, "Loans")){
			Table = database.query("Loans", null, null, null, null,null, "name");
			Lent.clear();
			NLen = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				reason = Table.getString(2);
				ind = Table.getInt(3);
				date = new GregorianCalendar();
				if(ind == -1) date.add(Calendar.YEAR, 100);
				else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				if(addObject(TObjType.toPre, id, str, reason, date)) NLen++;}}

		// Read Borrows
		if(tableExists(database, "Borrows")){
			Table = database.query("Borrows", null, null, null, null,null, "name");
			Borrowed.clear();
			NBor = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				reason = Table.getString(2);
				ind = Table.getInt(3);
				date = new GregorianCalendar();
				if(ind == -1) date.add(Calendar.YEAR, 100);
				else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				if(addObject(TObjType.toInP, id, str, reason, date)) NBor++;}}

		// Read Categories
		Table = database.query("Categories", null, null, null, null,null, "name");
		CategoryDB.clear();
		NCat = 0;
		Rows = Table.getCount();
		for (i = 0; i < Rows; i++){
			Table.moveToPosition(i)	;
			id = Table.getInt(0);
			str = Table.getString(1);
			is_date = (Table.getInt(2) != 0);
			if(is_date) addCategory(id, str);
		}

		// Read Transactions
		Table = database.query("Transactions", null, null, null, null,null, "date");
		Lines.clear();
		NLin = 0;
		Rows = Table.getCount();
		for (i = 0; i < Rows; i++){
			Table.moveToPosition(i);
			id = Table.getInt(0);
			is_date = (Table.getInt(1) != 0);
			date = new GregorianCalendar();
			ind = Table.getInt(2);
			date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
			ind = Table.getInt(3);
			time = new GregorianCalendar();
			time.setTimeInMillis(ind * 1000);	// dates are stored in seconds

			str = Table.getString(5);
			reason = Table.getString(6);
			ind = Table.getInt(7);
			if(is_date){
				//str.ToCDouble(&val);
				if(addDate_v31(id, date, /*val*/str)) NLin++;
				lastdate = date;}
			else{
				switch(Table.getInt(4)){
					default:
					case 0:
						type = TOpType.toNULL;
						break;
					case 1:
						type = TOpType.toGain;
						break;
					case 2:
						type = TOpType.toExpe;
						break;
					case 3:
						type = TOpType.toSetCre;
						break;
					case 4:
						type = TOpType.toRemCre;
						break;
					case 5:
						type = TOpType.toConCre;
						break;
					case 6:
						type = TOpType.toSetDeb;
						break;
					case 7:
						type = TOpType.toRemDeb;
						break;
					case 8:
						type = TOpType.toConDeb;
						break;
					case 9:
						type = TOpType.toGetObj;
						break;
					case 10:
						type = TOpType.toGivObj;
						break;
					case 11:
						type = TOpType.toLenObj;
						break;
					case 12:
						type = TOpType.toBakObj;
						break;
					case 13:
						type = TOpType.toBorObj;
						break;
					case 14:
						type = TOpType.toBorObj;
				}

				if(addOper(id, date, time, type, str, reason, ind)) NLin++;}
			}

		// Read shopping list
			if(tableExists(database, "Shoplist")){
			Table = database.query("Shoplist", null, null, null, null,null, "item collate nocase");
			ShopItems.clear();
			NSho = 0;
			Rows = Table.getCount();
			for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				id = Table.getInt(0);
				str = Table.getString(1);
				ind = Table.getInt(2);
				date = new GregorianCalendar();
				if(ind == -1) date.add(Calendar.YEAR, 100);
				else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				if(addShopItem(id, str, date)) NSho++;}}
		
		FileData.Year = lastdate.get(Calendar.YEAR);
		FileData.Month = lastdate.get(Calendar.MONTH);
		
		Parsing = false;

		Table.close();
	}

	public void setDefaultFund(String def){
		ContentValues cv = new ContentValues();
		cv.put("data", def);
		database.update("Information", cv, "id = " + String.format("%d", constants.dbMeta_default_fund), null);

		FileData.DefFund = def;
		FileData.Modified = true;
		parseDatabase_v31();
	}

	public void setDefaultFundValue(double value){
		for(int i = 0; i < NFun; i++)
			if(Funds.get(i).Name.compareToIgnoreCase(FileData.DefFund) == 0){
				/*
				ContentValues cv = new ContentValues();
				cv.put("value", value);
				database.update("Funds", cv, "id = " + String.format("%d", Funds.get(i).Id), null);
				*/
				changeFundValue(TTypeVal.tvFou, Funds.get(i).Id, value);

				parseDatabase_v31();
				break;}
	}

	void addCategory(int id, String name){
		TCategory temp = new TCategory();
		temp.Id = id;
		temp.Name = name;
		temp.test = false;
		CategoryDB.add(temp);
		NCat++;}

	void updateCategories(ArrayList<String> lbox){
		boolean exists;
		int i, x;	// x: pointer in Data->Lines
							// E: pointer to existing value
		String value;
		ContentValues cv = new ContentValues();
		
		boolean master_exist = false;
		SQLiteDatabase db = null;

		File fMaster = frame.getDatabasePath(constants.archive_name);
		if(fMaster.exists()){
			String master = fMaster.toString();
			db = SQLiteDatabase.openOrCreateDatabase(master, "", null);
			if(tableExists(db, "Categories")) master_exist = true;
			else db.close();
		}
		
		// Check removed items
		for(i = NCat - 1; i >= 0; i--){
			//exists = false;
			value = CategoryDB.get(i).Name;
			for (x = 0; x < lbox.size(); x++)if(value.compareToIgnoreCase(lbox.get(x)) == 0){
				CategoryDB.get(i).test = true;
				break;}}
		for(i = NCat - 1; i >= 0; i--) if(! CategoryDB.get(i).test){
			cv.clear();
			cv.put("cat_index", -1);
			database.update("Transactions", cv, "cat_index=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});

			cv.clear();
			cv.put("active", false);
			database.update("Categories", cv, "id=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});

			if(master_exist){
				cv.clear();
				cv.put("cat_index", -1);
				db.update("Transactions", cv, "cat_index=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});

				cv.clear();
				cv.put("active", false);
				db.update("Categories", cv, "id=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});
				
			}
		}

		// Check added items
		for(i = 0; i < lbox.size(); i++){
			exists = false;
			value = lbox.get(i);
			for(x = 0; x < NCat; x++) if(value.compareToIgnoreCase(CategoryDB.get(x).Name) == 0){
				exists = true;
				break;}
			if(! exists){
				cv.clear();
				cv.put("name", value);
				cv.put("active", true);
				database.insert("Categories", null, cv);
				
				if(master_exist) db.insert("Categories", null, cv);
			}
		}

		if(master_exist) db.close();
		
		FileData.Modified = true;
		parseDatabase_v31();
	}

	/*boolean*/void changeFundValue(TTypeVal type, int id, double V){

		String table;
		switch(type){
			case tvFou:
				table = "Funds";
				break;
			case tvCre:
				table = "Credits";
				break;
			case tvDeb:
				table = "Debts";
				break;
			default:
				table = "";}

		//String curstr = ::wxString::FromCDouble(V);
		ContentValues cv = new ContentValues();
		cv.clear();
		cv.put("value", V);
		database.update(table, cv, "id = " + String.format("%d", id), null);

		parseDatabase_v31();
		FileData.Modified=true;
		//return /*true*/;
	}

	String SubstSpecialChars(String S){
		// source: http://www.mrwebmaster.it/xml/xml-lettere-accentate-perche-errore_8390.html
		S = S.replace("&", "&amp;");
		S = S.replace("à", "&#224;");
		S = S.replace("á", "&#225;");
		S = S.replace("è", "&#232;");
		S = S.replace("é", "&#233;");
		S = S.replace("ì", "&#236;");
		S = S.replace("í", "&#237;");
		S = S.replace("ò", "&#242;");
		S = S.replace("ó", "&#243;");
		S = S.replace("ù", "&#249;");
		S = S.replace("ú", "&#250;");
		S = S.replace("Ì", "&#204;");
		S = S.replace("[OMB_ESCAPEQUOTES]", "&quot;");
		return S;}

	String getKey(boolean archive){
		String pwd ;

		try {
			if (archive) pwd = Opts.getString("key_archive", "");
			else pwd = Opts.getString("key_main", "");
		}
		catch(Exception e){
			return "";
		}

		return pwd;
	}

}
