/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-11-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class fund_add extends Activity {

	private EditText nameText, valueText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.funds_add);
		
		nameText = findViewById(R.id.Name);
		valueText = findViewById(R.id.Value);

		ImageButton findButton = findViewById(R.id.searchButton);
		findButton.setVisibility(View.GONE);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
            LinearLayout ll = findViewById(R.id.ll_funds);
            ll.setBackgroundColor(0xff000000);
			TextView caption = findViewById(R.id.Caption);
			caption.setBackgroundColor(0xff000000);
			caption.setTextColor(0xffffffff);
			nameText.setHintTextColor(0xff333333);
			nameText.setTextColor(0xffffffff);
			valueText.setHintTextColor(0xff333333);
			valueText.setTextColor(0xffffffff);
		}
	}
	
	public void okBtnClick(View view){
		double cur;
		int ReturnCode /*= RESULT_CANCELED*/;
		
		omb_library.appContext = getApplicationContext();

		//TextView value = (TextView) findViewById(R.id.opValue);
		String name = nameText.getText().toString();
		if(name.isEmpty()){
			omb_library.Error(23, "");
			nameText.requestFocus();
			return;}
		
		if(valueText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			valueText.requestFocus();
			return;}
		cur = Double.parseDouble(valueText.getText().toString());
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		name = omb_library.iUpperCase(name);
		name = name.trim();
		bundle.putString("fund", name);
		bundle.putDouble("value", cur);
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
}
