/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-02-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;

import java.io.File;
import java.util.GregorianCalendar;

public class documentselection extends Activity {

	private static final int PICKFILE_RESULT_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.documentselection);
	}
	
	public void BrowseClick(View view) {
		// TODO Implement embedded file browsing
		/*
		String extState = Environment.getExternalStorageState();
		//you may also want to add (...|| Environment.MEDIA_MOUNTED_READ_ONLY)
		//if you are only interested in reading the filesystem
		if(!extState.equals(Environment.MEDIA_MOUNTED)) {
		    //handle error here
		}
		else {
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					File sel = new File(dir, filename);
					// Filters based on whether the file is hidden or not
					boolean accepted = filename.toLowerCase().endsWith(".omb");
					return (accepted || sel.isDirectory()) && !sel.isHidden();
				}
			};

			//there is also getRootDirectory(), getDataDirectory(), etc. in the docs
			File sd = Environment.getExternalStorageDirectory();
			//This will return an array with all the Files (directories and files)
			//in the external storage folder
			String[] fList = sd.list(filter);

		}
		*/

		// ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
		// browser.
        //Intent fileintent = new Intent(Intent.ACTION_GET_CONTENT);
		Intent fileintent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

		// Filter to only show results that can be "opened", such as a
		// file (as opposed to a list of contacts or timezones)
		fileintent.addCategory(Intent.CATEGORY_OPENABLE);

        fileintent.setType("*/*");
        try {
        	startActivityForResult(fileintent, PICKFILE_RESULT_CODE);
        	//Uri uri = fileintent.getData();
        	//FilePath = FileUtils.getPath(this, uri);
            
        } catch (ActivityNotFoundException e) {
            //Log.e("tag", "No activity can handle picking a file. Showing alternatives.");

		}
	}
	
	public void WizardClick(View view) {
		Intent intent = new Intent(this, wizard.class);
       	startActivityForResult(intent, 2);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Fix no activity available
        if (data != null) {
			switch (requestCode) {
				case PICKFILE_RESULT_CODE:
					if (resultCode == RESULT_OK) {
						String filePath ;
						Uri uri = data.getData();

						filePath = RealPathUtil.getRealPath(getApplicationContext(), uri);

						//FilePath is your file as a string
						if (!filePath.isEmpty()) {
							SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
							Editor edit = Opts.edit();
							edit.putString("GDDoc", filePath);
							edit.apply();
						}

						setResult(resultCode);
						finish();
					}
					break;
				case 2:
					Bundle bundle = data.getExtras();
					double saved = bundle.getDouble("saved");
					double cash = bundle.getDouble("cash");

					String extState = Environment.getExternalStorageState();
					//you may also want to add (...|| Environment.MEDIA_MOUNTED_READ_ONLY)
					//if you are only interested in reading the filesystem

					if (extState.equals(Environment.MEDIA_MOUNTED)) {
						File sd = Environment.getExternalStorageDirectory();
						String filename = sd.getAbsolutePath();
						filename = filename + "/openmoneybox.omb";

						//boolean Status;
						omb33core Data_Auto = new omb33core(filename, null, null);
						String cash_fund = getResources().getString(R.string.wizard_cash_fund);
						Data_Auto.addValue(omb31core.TTypeVal.tvFou, -1, getResources().getString(R.string.wizard_saved_fund), saved);
						Data_Auto.addValue(omb31core.TTypeVal.tvFou, -1, cash_fund, cash);
						Data_Auto.setDefaultFund(cash_fund);
						Data_Auto.addDate(-1, new GregorianCalendar(), omb_library.FormDigits(saved + cash, true));
						Data_Auto.database.execSQL("RELEASE rollback;");
						Data_Auto.database.close();

						SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
						Editor edit = Opts.edit();
						edit.putString("GDDoc", filename);
						edit.apply();
						//}
					}
					// TODO implement error code
					/*
					else{
						//handle error here
					}
					*/
					break;
				default:
			}
		}
	}
	
	/*
	@Override
	protected void onResume(){
		super.onResume();
		if(FilePath != ""){
	 		AlertDialog.Builder pass_alert = new AlertDialog.Builder(this);
	
	 		pass_alert.setTitle(getResources().getString(R.string.app_name));
	 		pass_alert.setMessage(getResources().getString(R.string.password_insert));
	
			// Set an EditText view to get user input 
			final EditText input = new EditText(this);
			input.setTransformationMethod(PasswordTransformationMethod.getInstance());
			pass_alert.setView(input);
	
			pass_alert.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					Editor editor = Opts.edit(); 
					editor.putString("GPassword", input.getText().toString());
					editor.commit();
			  }
			});
	
			pass_alert.show();
		}
	}
	*/

	@Override
	public void onBackPressed() {
		// Do nothing
	}

}
