;	Constants
;	Last modify 20/05/2019

;___________________________________________________________________________________
;Version info
!define MAJ_VER 3	;Major version
!define MIN_VER 3	;Minor version
!define RELEASE 1	;Release
!define BUILD 3	;Build Number

!define MAJ_VER_str "03"
!define MIN_VER_str "03"
!define RELEASE_str "01"
!define BUILD_str "003"

;	!define IGILOGO_NEW "1"	; Set to 1 if a new igiLogo.dll version is included
;___________________________________________________________________________________

;Development info
;	!define COMPILER "mingw32-g++"	;Mingw32
;	!define DEBUG "1"	;defined if it is a debug version
;	!define ALPHA "1"	;defined if it is an alpha version
;	!define BETA "1"		;defined if it is a beta version
;	!define RC "1"		;defined if it is a Release Candidate
;___________________________________________________________________________________

;Path info
;___________________________________________________________________________________
!define Files "..\_win\install"
;	!define igiLogoScriptPath "E:\Utenti\igor\Documenti\Programmazione\igiLogo_DLL"
;___________________________________________________________________________________

;Registry Keys
;___________________________________________________________________________________
!Define SW_Key "SOFTWARE\igiSW\OpenMoneyBox\3.3"	;HKLM\Software
;___________________________________________________________________________________

;Features
;___________________________________________________________________________________

;Update info
;___________________________________________________________________________________
!define REG_VER	1	;Registry check value for installer updates
;___________________________________________________________________________________
